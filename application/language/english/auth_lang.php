<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Name:  Auth Lang - English
*
* Author: Ben Edmunds
* 		  ben.edmunds@gmail.com
*         @benedmunds
*
* Author: Daniel Davis
*         @ourmaninjapan
*
* Location: http://github.com/benedmunds/ion_auth/
*
* Created:  03.09.2013
*
* Description:  English language file for Ion Auth example views
*
*/

// Errors
$lang['error_csrf'] = 'This form post did not pass our security checks.';

// Login
$lang['login_heading']         = 'Login';
$lang['login_subheading']      = 'Please login with your email/username and password below.';
$lang['login_identity_label']  = 'Email/Username:';
$lang['login_password_label']  = 'Password:';
$lang['login_remember_label']  = 'Remember Me:';
$lang['login_submit_btn']      = 'Login';
$lang['login_forgot_password'] = 'Forgot your password?';
$lang['login_username_placeholder'] = 'Enter username';
$lang['login_password_placeholder'] = 'Enter password';

// Index
$lang['index_heading']           = 'Users';
$lang['index_subheading']        = 'Below is a list of the users.';
$lang['index_fname_th']          = 'First Name';
$lang['index_lname_th']          = 'Last Name';
$lang['index_email_th']          = 'Email';
$lang['index_groups_th']         = 'Groups';
$lang['index_status_th']         = 'Status';
$lang['index_action_th']         = 'Action';
$lang['index_active_link']       = 'Active';
$lang['index_inactive_link']     = 'Inactive';
$lang['index_create_user_link']  = 'Create a new user';
$lang['index_create_group_link'] = 'Create a new group';

// Deactivate User
$lang['deactivate_heading']                  = 'Deactivate User';
$lang['deactivate_subheading']               = 'Are you sure you want to deactivate the user \'%s\'';
$lang['deactivate_confirm_y_label']          = 'Yes:';
$lang['deactivate_confirm_n_label']          = 'No:';
$lang['deactivate_submit_btn']               = 'Submit';
$lang['deactivate_validation_confirm_label'] = 'confirmation';
$lang['deactivate_validation_user_id_label'] = 'user ID';

// Create User
$lang['create_user_heading']                           = 'Create User';
$lang['create_user_subheading']                        = 'Please enter the user\'s information below.';
$lang['create_user_fname_label']                       = 'First Name:';
$lang['create_user_lname_label']                       = 'Last Name:';
$lang['create_user_company_label']                     = 'Company Name:';
$lang['create_user_identity_label']                    = 'Identity:';
$lang['create_user_email_label']                       = 'Email:';
$lang['create_user_phone_label']                       = 'Phone:';
$lang['create_user_password_label']                    = 'Password:';
$lang['create_user_password_confirm_label']            = 'Confirm Password:';
$lang['create_user_submit_btn']                        = 'Create User';
$lang['create_user_validation_fname_label']            = 'First Name';
$lang['create_user_validation_lname_label']            = 'Last Name';
$lang['create_user_validation_identity_label']         = 'Identity';
$lang['create_user_validation_email_label']            = 'Email Address';
$lang['create_user_validation_phone_label']            = 'Phone';
$lang['create_user_validation_company_label']          = 'Company Name';
$lang['create_user_validation_password_label']         = 'Password';
$lang['create_user_validation_password_confirm_label'] = 'Password Confirmation';
$lang['create_first_name_placeholder']                 = 'Enter first name';
$lang['create_last_name_placeholder']                  = 'Enter last name';
$lang['create_indentify_placeholder']                  = 'Enter username';
$lang['create_company_placeholder']                    = 'Enter company name';
$lang['create_email_placeholder']                      = 'Enter email';
$lang['create_phone_placeholder']                      = 'Enter phone number';
$lang['create_password_placeholder']                   = 'Enter password';
$lang['create_password_confirm_placeholder']           = 'Enter password confirm';

// Edit User
$lang['edit_user_heading']                           = 'Edit User';
$lang['edit_user_subheading']                        = 'Please enter the user\'s information below.';
$lang['edit_user_fname_label']                       = 'First Name:';
$lang['edit_user_lname_label']                       = 'Last Name:';
$lang['edit_user_company_label']                     = 'Company Name:';
$lang['edit_user_email_label']                       = 'Email:';
$lang['edit_user_phone_label']                       = 'Phone:';
$lang['edit_user_password_label']                    = 'Password: (if changing password)';
$lang['edit_user_password_confirm_label']            = 'Confirm Password: (if changing password)';
$lang['edit_user_groups_heading']                    = 'Member of groups';
$lang['edit_user_submit_btn']                        = 'Save User';
$lang['edit_user_validation_fname_label']            = 'First Name';
$lang['edit_user_validation_lname_label']            = 'Last Name';
$lang['edit_user_validation_email_label']            = 'Email Address';
$lang['edit_user_validation_phone_label']            = 'Phone';
$lang['edit_user_validation_company_label']          = 'Company Name';
$lang['edit_user_validation_groups_label']           = 'Groups';
$lang['edit_user_validation_password_label']         = 'Password';
$lang['edit_user_validation_password_confirm_label'] = 'Password Confirmation';

// Create Group
$lang['create_group_title']                  = 'Create Group';
$lang['create_group_heading']                = 'Create Group';
$lang['create_group_subheading']             = 'Please enter the group information below.';
$lang['create_group_name_label']             = 'Group Name:';
$lang['create_group_desc_label']             = 'Description:';
$lang['create_group_submit_btn']             = 'Create Group';
$lang['create_group_validation_name_label']  = 'Group Name';
$lang['create_group_validation_desc_label']  = 'Description';
$lang['create_group_placeholder_group_name'] = 'Enter group name';
$lang['create_group_placeholder_group_description'] = 'Enter group description';

// Edit Group
$lang['edit_group_title']                  = 'Edit Group';
$lang['edit_group_saved']                  = 'Group Saved';
$lang['edit_group_heading']                = 'Edit Group';
$lang['edit_group_subheading']             = 'Please enter the group information below.';
$lang['edit_group_name_label']             = 'Group Name:';
$lang['edit_group_desc_label']             = 'Description:';
$lang['edit_group_submit_btn']             = 'Save Group';
$lang['edit_group_validation_name_label']  = 'Group Name';
$lang['edit_group_validation_desc_label']  = 'Description';

// Change Password
$lang['change_password_heading']                               = 'Change Password';
$lang['change_password_old_password_label']                    = 'Old Password:';
$lang['change_password_new_password_label']                    = 'New Password (at least %s characters long):';
$lang['change_password_new_password_confirm_label']            = 'Confirm New Password:';
$lang['change_password_submit_btn']                            = 'Change';
$lang['change_password_validation_old_password_label']         = 'Old Password';
$lang['change_password_validation_new_password_label']         = 'New Password';
$lang['change_password_validation_new_password_confirm_label'] = 'Confirm New Password';

// Forgot Password
$lang['forgot_password_heading']                 = 'Forgot Password';
$lang['forgot_password_subheading']              = 'Please enter your %s so we can send you an email to reset your password.';
$lang['forgot_password_email_label']             = '%s:';
$lang['forgot_password_submit_btn']              = 'Submit';
$lang['forgot_password_validation_email_label']  = 'Email Address';
$lang['forgot_password_identity_label'] = 'Identity';
$lang['forgot_password_email_identity_label']    = 'Email';
$lang['forgot_password_email_not_found']         = 'No record of that email address.';
$lang['forgot_password_identity_not_found']         = 'No record of that username.';

// Reset Password
$lang['reset_password_heading']                               = 'Change Password';
$lang['reset_password_new_password_label']                    = 'New Password (at least %s characters long):';
$lang['reset_password_new_password_confirm_label']            = 'Confirm New Password:';
$lang['reset_password_submit_btn']                            = 'Change';
$lang['reset_password_validation_new_password_label']         = 'New Password';
$lang['reset_password_validation_new_password_confirm_label'] = 'Confirm New Password';

// Article Home
$lang['article_home_heading']                           = 'Article';
$lang['article_home_subheading']                        = 'Below is a list of the article.';
$lang['article_home_new_article_btn']                   = 'Add New';
$lang['article_home_identifier_1']                      = 'Home';
$lang['article_home_identifier_2']                      = 'List of Article';
$lang['article_home_title_lbl']                         = 'Title';
$lang['article_home_last_update_lbl']                   = 'Last Update';
$lang['article_home_category_lbl']                      = 'Category';
$lang['article_home_actions_lbl']                       = 'Actions';
$lang['article_home_edit_article_btn']                  = 'Edit';
$lang['article_home_delete_article_btn']                = 'Delete Article';
$lang['article_home_delete_alert'] 						= 'Are you sure want to delete this Article?';
$lang['article_home_detail_article_btn']                = 'Show Detail';

// New Article
$lang['article_add_identifier_1']                      = 'Home';
$lang['article_add_identifier_2']                      = 'Add Article';
$lang['article_add_form_title']                        = 'Create New Article';
$lang['article_add_title_lbl']                         = 'Title of Article:';
$lang['article_add_title_placeholder']                 = 'Write the Title';
$lang['article_add_upload_lbl']                        = 'Upload Image:';
$lang['article_add_content_lbl']                       = 'Content of Article';
$lang['article_add_information_title']                 = 'information';
$lang['article_add_select_category']                   = 'Select Category:';
$lang['article_add_post_article_btn']                  = 'Publish Article';

//Edit Article
$lang['article_edit_identifier_1']                      = 'Home';
$lang['article_edit_identifier_2']                      = 'Edit Article';
$lang['article_edit_form_title']                        = 'Edit Article';
$lang['article_edit_title_lbl']                         = 'Title of Article:';
$lang['article_edit_title_placeholder']                 = 'Write the Title';
$lang['article_edit_content_lbl']                       = 'Content of Article';
$lang['article_edit_information_title']                 = 'information';
$lang['article_edit_select_category']                   = 'Select Category:';
$lang['article_edit_upload_lbl']                        = 'Edit Image:';
$lang['article_edit_post_article_btn']                  = 'Update Article';

// Detail Article
$lang['article_detail_heading']                         = 'Article Detail';
$lang['article_detail_identifier_1']                    = 'Home';
$lang['article_detail_identifier_2']                    = 'Article Detail';
$lang['article_detail_form_title']                      = 'Article';
$lang['article_detail_date_post']                       = 'Date of Create :';
$lang['article_detail_title_lbl']              	        = 'Title of Article';
$lang['article_detail_last_update_lbl']					= 'Last Update';
$lang['article_detail_category_lbl']			        = 'Category';
$lang['article_detail_content_lbl']		                = 'Content';

// Category Article Home
$lang['cat_article_home_heading']                           = 'Article Category';
$lang['cat_article_home_new_cat_article_btn']               = 'Add New';
$lang['cat_article_home_identifier_1']                      = 'Home';
$lang['cat_article_home_identifier_2']                      = 'List of Article Category';
$lang['cat_article_home_title_lbl']                         = 'Name';
$lang['cat_article_home_actions_lbl']                       = 'Actions';
$lang['cat_article_home_edit_cat_article_btn']              = 'Edit';
$lang['cat_article_home_delete_cat_article_btn']            = 'Delete Article Category';
$lang['cat_article_home_delete_alert'] 						= 'Are you sure want to delete this Article Cate?';

// New Category Article
$lang['cat_article_add_identifier_1']                      = 'Home';
$lang['cat_article_add_identifier_2']                      = 'Add Article Category';
$lang['cat_article_add_form_title']                        = 'Add Article Category';
$lang['cat_article_add_title_lbl']                         = 'Name of Category:';
$lang['cat_article_add_title_placeholder']                 = 'Write the Name Article Category';
$lang['cat_article_add_information_title']                 = 'information';
$lang['cat_article_add_post_cat_article_btn']              = 'Add Category Article';

//Edit Category Article
$lang['article_edit_identifier_1']                      = 'Home';
$lang['article_edit_identifier_2']                      = 'Edit Article Category';
$lang['article_edit_form_title']                        = 'Edit Article Category';
$lang['article_edit_title_lbl']                         = 'Name of Category:';
$lang['article_edit_title_placeholder']                 = 'Write the Name Article Category';
$lang['article_edit_information_title']                 = 'information';
$lang['article_edit_post_cat_article_btn']              = 'Update';

// Gallery Home
$lang['gallery_home_heading']                           = 'Gallery';
$lang['gallery_home_subheading']                        = 'Below is a list of the photos.';
$lang['gallery_home_new_gallery_btn']                   = 'Add New';
$lang['gallery_home_identifier_1']                      = 'Home';
$lang['gallery_home_identifier_2']                      = 'List of Gallery';
$lang['gallery_home_delete_galley_btn']                 = 'Delete Image';
$lang['gallery_home_delete_alert'] 						= 'Are you sure want to delete this Image?';

// Gallery Article
$lang['gallery_add_identifier_1']                      = 'Home';
$lang['gallery_add_identifier_2']                      = 'Add Image';
$lang['gallery_add_form_title']                        = 'Add New Image';
$lang['gallery_add_title_lbl']                         = 'Upload Image:';
$lang['gallery_add_information_title']                 = 'information';
$lang['gallery_add_post_gallery_btn']                  = 'Upload Image';

// Set Profile
$lang['profile_heading']                            = 'Profile';
$lang['profile_identifier_1']                       = 'Home';
$lang['profile_identifier_2']                       = 'Profile';
$lang['profile_form_title']                         = 'Set Profile';
$lang['profile_vision_lbl']                         = 'Vision:';
$lang['profile_mission_lbl']                        = 'Mision:';
$lang['profile_history_lbl']                        = 'A Brief History:';
$lang['profile_edit_btn']                           = 'Update Profile';

// Courses Home
$lang['courses_home_heading']                           = 'Courses Program';
$lang['courses_home_new_courses_btn']                   = 'Add New';
$lang['courses_home_identifier_1']                      = 'Home';
$lang['courses_home_identifier_2']                      = 'Courses Program';
$lang['courses_home_title_lbl']                         = 'Name of Courses';
$lang['courses_home_price_lbl']		                    = 'Price';
$lang['courses_home_actions_lbl']                       = 'Actions';
$lang['courses_home_edit_courses_btn']                  = 'Edit';
$lang['courses_home_delete_alert'] 						= 'Are you sure want to delete this Courses Program?';
$lang['courses_home_delete_courses_btn']                = 'Delete Courses';

// New Courses
$lang['courses_add_identifier_1']                      = 'Home';
$lang['courses_add_identifier_2']                      = 'Add Courses Program';
$lang['courses_add_form_title']                        = 'Add New Courses';
$lang['courses_add_title_lbl']                         = 'Name of Courses:';
$lang['courses_add_title_placeholder']                 = 'Write the Name Courses Program';
$lang['courses_add_desc_lbl']                          = 'Description:';
$lang['courses_add_price_lbl']                         = 'Price:';
$lang['courses_add_price_placeholder']                 = 'Write the Price Courses Program';
$lang['courses_add_information_title']                 = 'Information';
$lang['courses_add_post_courses_btn']                  = 'Add Courses';

//Edit Courses
$lang['courses_edit_identifier_1']                      = 'Home';
$lang['courses_edit_identifier_2']                      = 'Edit Courses';
$lang['courses_edit_form_title']                        = 'Edit Courses Program';
$lang['courses_edit_title_lbl']                         = 'Name of Courses:';
$lang['courses_edit_title_placeholder']                 = 'Write the Name Courses Program';
$lang['courses_edit_desc_lbl']                          = 'Description:';
$lang['courses_edit_price_lbl']                         = 'Price:';
$lang['courses_edit_price_placeholder']                 = 'Write the Price Courses Program';
$lang['courses_edit_information_title']                 = 'Information';
$lang['courses_edit_post_courses_btn']                  = 'Update Courses';

// Program Home
$lang['program_home_heading']                           = 'Program';
$lang['program_home_new_courses_btn']                   = 'Add New';
$lang['program_home_identifier_1']                      = 'Home';
$lang['program_home_identifier_2']                      = 'Program';
$lang['program_home_title_lbl']                         = 'Name of Program';
$lang['program_home_category_lbl']	                    = 'Category';
$lang['program_home_actions_lbl']                       = 'Actions';
$lang['program_home_edit_program_btn']                  = 'Edit';
$lang['program_home_delete_alert'] 						= 'Are you sure want to delete this Program?';
$lang['program_home_delete_program_btn']                = 'Delete Program';

// New Program
$lang['program_add_identifier_1']                      = 'Home';
$lang['program_add_identifier_2']                      = 'Add Program';
$lang['program_add_form_title']                        = 'Add New Program';
$lang['program_add_title_lbl']                         = 'Name of Program:';
$lang['program_add_title_placeholder']                 = 'Write the Name Program';
$lang['program_add_desc_lbl']                          = 'Description Program:';
$lang['program_add_information_title']                 = 'Information';
$lang['program_add_select_category_lbl']               = 'Select Category:';
$lang['program_add_post_program_btn']                  = 'Add Program';

// Edit Program
$lang['program_edit_identifier_1']                      = 'Home';
$lang['program_edit_identifier_2']                      = 'Edit Program';
$lang['program_edit_form_title']                        = 'Edit Program';
$lang['program_edit_title_lbl']                         = 'Name of Program:';
$lang['program_edit_title_placeholder']                 = 'Write the Name Program';
$lang['program_edit_desc_lbl']                          = 'Description Program:';
$lang['program_edit_information_title']                 = 'Information';
$lang['program_edit_select_category_lbl']               = 'Select Category:';
$lang['program_edit_program_btn']                       = 'Edit Program';

// Set Contact
$lang['contact_heading']                            = 'Contact';
$lang['contact_identifier_1']                       = 'Home';
$lang['contact_identifier_2']                       = 'Contact';
$lang['contact_form_title']                         = 'Set Contact';
$lang['contact_edit_email_lbl']                     = 'Email:';
$lang['contact_edit_phone_lbl']                     = 'Phone:';
$lang['contact_edit_fax_lbl']                   	= 'Fax:';
$lang['contact_edit_address_lbl']                   = 'Address:';
$lang['contact_edit_city_lbl']                     	= 'City:';
$lang['contact_edit_postal_code_lbl']               = 'Postal Code:';
$lang['contact_edit_province_lbl']                  = 'Province:';
$lang['contact_edit_country_lbl']                   = 'Country:';
$lang['contact_edit_btn']                           = 'Update Contact';