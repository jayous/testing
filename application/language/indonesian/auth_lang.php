<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Name:  Auth Lang - Indonesia
*
* Author: 	Daeng Muhammad Feisal
*     http://daengdoang.wordpress.com
*			daengdoang@gmail.com
*			@daengdoang
*
*
*
* Location: http://github.com/benedmunds/ion_auth/
*
* Created:  21.06.2013
* Last-Edit: 21.06.2017
*
* Description:  Indonesia language file for Ion Auth example views
*
*/

// Errors
$lang['error_csrf'] = 'Form yang dikirim tidak lulus pemeriksaan keamanan kami.';

// Login
$lang['login_heading']         = 'Masuk';
$lang['login_subheading']      = 'Silakan login dengan email/username dan password anda.';
$lang['login_identity_label']  = 'Email / Nama Pengguna:';
$lang['login_password_label']  = 'Kata Sandi:';
$lang['login_remember_label']  = 'Ingatkan Saya:';
$lang['login_submit_btn']      = 'Login';
$lang['login_forgot_password'] = 'Lupa Kata Sandi?';
$lang['login_username_placeholder'] = 'Masukan nama pengguna';
$lang['login_password_placeholder'] = 'Masukan kata sandi';

// Index
$lang['index_heading']           = 'Pengguna';
$lang['index_subheading']        = 'Di bawah ini list dari para Pengguna.';
$lang['index_fname_th']          = 'Nama Awal';
$lang['index_lname_th']          = 'Nama Akhir';
$lang['index_email_th']          = 'Email';
$lang['index_groups_th']         = 'Grup';
$lang['index_status_th']         = 'Status';
$lang['index_action_th']         = 'Aksi';
$lang['index_active_link']       = 'Aktif';
$lang['index_inactive_link']     = 'Tidak Aktif';
$lang['index_create_user_link']  = 'Buat Pengguna baru';
$lang['index_create_group_link'] = 'Buat grup baru';

// Deactivate User
$lang['deactivate_heading']                  = 'Deaktivasi Pengguna';
$lang['deactivate_subheading']               = 'Anda yakin akan melakukan deaktivasi akun Pengguna \'%s\'';
$lang['deactivate_confirm_y_label']          = 'Ya:';
$lang['deactivate_confirm_n_label']          = 'Tidak:';
$lang['deactivate_submit_btn']               = 'Kirim';
$lang['deactivate_validation_confirm_label'] = 'konfirmasi';
$lang['deactivate_validation_user_id_label'] = 'ID Pengguna';

// Create User
$lang['create_user_heading']                           = 'Buat Pengguna';
$lang['create_user_subheading']                        = 'Silakan masukan informasi Pengguna di bawah ini.';
$lang['create_user_fname_label']                       = 'Nama Awal:';
$lang['create_user_lname_label']                       = 'Nama Akhir:';
$lang['create_user_company_label']                     = 'Nama Perusahaan:';
$lang['create_user_identity_label']                    = 'Identitas:';
$lang['create_user_email_label']                       = 'Email:';
$lang['create_user_phone_label']                       = 'Telepon:';
$lang['create_user_password_label']                    = 'Kata Sandi:';
$lang['create_user_password_confirm_label']            = 'Konfirmasi Kata Sandi:';
$lang['create_user_submit_btn']                        = 'Buat Pengguna';
$lang['create_user_validation_fname_label']            = 'Nama Awal';
$lang['create_user_validation_lname_label']            = 'Nama Akhir';
$lang['create_user_validation_identity_label']         = 'Identitas';
$lang['create_user_validation_email_label']            = 'Alamat Email';
$lang['create_user_validation_phone_label']            = 'Telepon';
$lang['create_user_validation_company_label']          = 'Nama Perusahaan';
$lang['create_user_validation_password_label']         = 'Kata Sandi';
$lang['create_user_validation_password_confirm_label'] = 'Konfirmasi Kata Sandi';
$lang['create_first_name_placeholder']                 = 'Masukan nama pertama pengguna';
$lang['create_last_name_placeholder']                  = 'Masukan nama akhir pengguna';
$lang['create_indentify_placeholder']                  = 'Masukan nama pengguna';
$lang['create_company_placeholder']                    = 'Masukan nama perusahaan';
$lang['create_email_placeholder']                      = 'Masukan email';
$lang['create_phone_placeholder']                      = 'Masukan nomor telepon / hp';
$lang['create_password_placeholder']                   = 'Masukan kata sandi';
$lang['create_password_confirm_placeholder']           = 'Masukan konfirmasi kata sandi';


// Edit User
$lang['edit_user_heading']                           = 'Ubah Pengguna';
$lang['edit_user_subheading']                        = 'Silakan masukan informasi Pengguna di bawah ini.';
$lang['edit_user_fname_label']                       = 'Nama Awal:';
$lang['edit_user_lname_label']                       = 'Nama Akhir:';
$lang['edit_user_company_label']                     = 'Nama Perusahaan:';
$lang['edit_user_email_label']                       = 'Email:';
$lang['edit_user_phone_label']                       = 'Telepon:';
$lang['edit_user_password_label']                    = 'Kata Sandi: (jika mengubah sandi)';
$lang['edit_user_password_confirm_label']            = 'Konfirmasi Kata Sandi: (jika mengubah sandi)';
$lang['edit_user_groups_heading']                    = 'Anggota dari Grup';
$lang['edit_user_submit_btn']                        = 'Simpan Pengguna';
$lang['edit_user_validation_fname_label']            = 'Nama Awal';
$lang['edit_user_validation_lname_label']            = 'Nama Akhir';
$lang['edit_user_validation_email_label']            = 'Alamat Email';
$lang['edit_user_validation_phone_label']            = 'Telepon';
$lang['edit_user_validation_company_label']          = 'Nama Perusahaan';
$lang['edit_user_validation_groups_label']           = 'Nama Grup';
$lang['edit_user_validation_password_label']         = 'Kata Sandi';
$lang['edit_user_validation_password_confirm_label'] = 'Konfirmasi Kata Sandi';

// Create Group
$lang['create_group_title']                  = 'Buat Grup';
$lang['create_group_heading']                = 'Buat Grupp';
$lang['create_group_subheading']             = 'Silakan masukan detail Grup di bawah ini.';
$lang['create_group_name_label']             = 'Nama Grup:';
$lang['create_group_desc_label']             = 'Deskripsi:';
$lang['create_group_submit_btn']             = 'Buat Grup';
$lang['create_group_validation_name_label']  = 'Nama Grup';
$lang['create_group_validation_desc_label']  = 'Deskripsi';
$lang['create_group_placeholder_group_name'] = 'Masukan nama grup';
$lang['create_group_placeholder_group_description'] = 'Masukan deskripsi grup';

// Edit Group
$lang['edit_group_title']                    = 'Ubah Grup';
$lang['edit_group_saved']                    = 'Grup Tersimpan';
$lang['edit_group_heading']                  = 'Ubah Grup';
$lang['edit_group_subheading']               = 'Silakan masukan detail Grup di bawah ini.';
$lang['edit_group_name_label']               = 'Nama Grup:';
$lang['edit_group_desc_label']               = 'Deskripsi:';
$lang['edit_group_submit_btn']               = 'Simpan Grup';
$lang['edit_group_validation_name_label']    = 'Nama Grup';
$lang['edit_group_validation_desc_label']    = 'Deskripsi';

// Change Password
$lang['change_password_heading']                               = 'Ganti Kata Sandi';
$lang['change_password_old_password_label']                    = 'Kata Santi Lama:';
$lang['change_password_new_password_label']                    = 'Kata Sandi Baru (paling sedikit %s karakter):';
$lang['change_password_new_password_confirm_label']            = 'Konfirmasi Kata Sandi:';
$lang['change_password_submit_btn']                            = 'Ubah';
$lang['change_password_validation_old_password_label']         = 'Kata Sandi Lama';
$lang['change_password_validation_new_password_label']         = 'Kata Sandi Baru';
$lang['change_password_validation_new_password_confirm_label'] = 'Konfirmasi Kata Sandi Baru';

// Forgot Password
$lang['forgot_password_heading']                 = 'Lupa Kata Sandi';
$lang['forgot_password_subheading']              = 'Silakan masukkan %s anda, agar kami dapat mengirim email untuk mereset Kata Sandi Anda.';
$lang['forgot_password_email_label']             = '%s:';
$lang['forgot_password_submit_btn']              = 'Kirim';
$lang['forgot_password_validation_email_label']  = 'Alamat Email';
$lang['forgot_password_username_identity_label'] = 'Nama Pengguna';
$lang['forgot_password_email_identity_label']    = 'Email';
$lang['forgot_password_email_not_found']         = 'Tidak ada data dari email tersebut.';
$lang['forgot_password_identity_not_found']      = 'Tidak ada data dari nama pengguna tersebut.';

// Reset Password
$lang['reset_password_heading']                               = 'Ganti Kata Sandi';
$lang['reset_password_new_password_label']                    = 'Kata Sandi Baru (paling sedikit %s karakter):';
$lang['reset_password_new_password_confirm_label']            = 'Konfirmasi Kata Sandi:';
$lang['reset_password_submit_btn']                            = 'Ubah';
$lang['reset_password_validation_new_password_label']         = 'Kata Sandi Baru';
$lang['reset_password_validation_new_password_confirm_label'] = 'Konfirmasi Kata Sandi Baru';

// Article Home
$lang['article_home_heading']                           = 'Artikel';
$lang['article_home_subheading']                        = 'Di bawah ini adalah daftar artikel.';
$lang['article_home_new_article_btn']                   = 'Tambah Baru';
$lang['article_home_identifier_1']                      = 'Beranda';
$lang['article_home_identifier_2']                      = 'Daftar Artikel';
$lang['article_home_title_lbl']                         = 'Judul';
$lang['article_home_last_update_lbl']                   = 'Terakhir Disunting';
$lang['article_home_category_lbl']                      = 'Kategori';
$lang['article_home_actions_lbl']                       = 'Aksi';
$lang['article_home_edit_article_btn']                  = 'Sunting';
$lang['article_home_delete_article_btn']                = 'Hapus Artikel';
$lang['article_home_delete_alert'] 						= 'Apa anda yakin ingin menghapus Artikel ini?';
$lang['article_home_detail_article_btn']                = 'Tampilkan';

// New Article
$lang['article_add_identifier_1']                      = 'Beranda';
$lang['article_add_identifier_2']                      = 'Tambah Artikel';
$lang['article_add_form_title']                        = 'Buat Artikel Baru';
$lang['article_add_title_lbl']                         = 'Judul Artikel:';
$lang['article_add_title_placeholder']                 = 'Tulis Judul';
$lang['article_add_upload_lbl']                        = 'Unggah Gambar:';
$lang['article_add_content_lbl']                       = 'Konten Artikel';
$lang['article_add_information_title']                 = 'Informasi';
$lang['article_add_select_category']                   = 'Pilih Katagori:';
$lang['article_add_post_article_btn']                  = 'Terbitkan Artikel';

//Edit Article
$lang['article_edit_identifier_1']                      = 'Beranda';
$lang['article_edit_identifier_2']                      = 'Sunting Artikel';
$lang['article_edit_form_title']                        = 'Sunting Artikel';
$lang['article_edit_title_lbl']                         = 'Judul Artikel:';
$lang['article_edit_title_placeholder']                 = 'Tulis Judul';
$lang['article_edit_content_lbl']                       = 'Konten Artikel';
$lang['article_edit_information_title']                 = 'Informasi';
$lang['article_edit_select_category']                   = 'Pilih Katagori:';
$lang['article_edit_upload_lbl']                        = 'Sunting Gambar:';
$lang['article_edit_post_article_btn']                  = 'Perbarui Artikel';

// Detail Article
$lang['article_detail_heading']                         = 'Detail Artikel';
$lang['article_detail_identifier_1']                    = 'Beranda';
$lang['article_detail_identifier_2']                    = 'Detail Artikel';
$lang['article_detail_form_title']                      = 'Artikel';
$lang['article_detail_date_post']                       = 'Tanggal Diterbitkan :';
$lang['article_detail_title_lbl']              	        = 'Judul Artikel';
$lang['article_detail_last_update_lbl']					= 'Terakhir Disunting';
$lang['article_detail_category_lbl']			        = 'Kategori';
$lang['article_detail_content_lbl']		                = 'Konten';

// Category Article Home
$lang['cat_article_home_heading']                           = 'Kategori Artikel';
$lang['cat_article_home_new_cat_article_btn']               = 'Tambah Kategori Artikel';
$lang['cat_article_home_identifier_1']                      = 'Beranda';
$lang['cat_article_home_identifier_2']                      = 'Daftar Kategori Artikel';
$lang['cat_article_home_title_lbl']                         = 'Judul';
$lang['cat_article_home_actions_lbl']                       = 'Aksi';
$lang['cat_article_home_edit_cat_article_btn']              = 'Sunting';
$lang['cat_article_home_delete_cat_article_btn']            = 'Hapus Kategori Artikel';
$lang['cat_article_home_delete_alert'] 						= 'Apa anda yakin ingin menghapus Kategori Artikel ini?';

// New Category Article
$lang['cat_article_add_identifier_1']                      = 'Beranda';
$lang['cat_article_add_identifier_2']                      = 'Tambah Kategori Artikel';
$lang['cat_article_add_form_title']                        = 'Tambah Kategori Artikel';
$lang['cat_article_add_title_lbl']                         = 'Nama Kategori:';
$lang['cat_article_add_title_placeholder']                 = 'Tulis Nama Kategori';
$lang['cat_article_add_information_title']                 = 'Informasi';
$lang['cat_article_add_post_cat_article_btn']              = 'Tambahkan';

//Edit Category Article
$lang['article_edit_identifier_1']                      = 'Beranda';
$lang['article_edit_identifier_2']                      = 'Sunting Kategori Artikel';
$lang['article_edit_form_title']                        = 'Sunting Kategori Artikel';
$lang['article_edit_title_lbl']                         = 'Nama Kategori:';
$lang['article_edit_title_placeholder']                 = 'Tulis Nama Kategori';
$lang['article_edit_information_title']                 = 'Informasi';
$lang['article_edit_post_cat_article_btn']              = 'Perbarui';

// Gallery Home
$lang['gallery_home_heading']                           = 'Galeri';
$lang['gallery_home_subheading']                        = 'Di Bawah Ini Adalah Daftar foto';
$lang['gallery_home_new_gallery_btn']                   = 'Tambah Baru';
$lang['gallery_home_identifier_1']                      = 'Beranda';
$lang['gallery_home_identifier_2']                      = 'Daftar Galeri';
$lang['gallery_home_delete_galley_btn']                 = 'Hapus Gambar';
$lang['gallery_home_delete_alert'] 						= 'Apa anda yakin ingin menghapus Gambar ini?';

// Gallery Article
$lang['gallery_add_identifier_1']                      = 'Beranda';
$lang['gallery_add_identifier_2']                      = 'Tambah Gambar';
$lang['gallery_add_form_title']                        = 'Tambah Gambar Baru';
$lang['gallery_add_title_lbl']                         = 'Unggah Gambar:';
$lang['gallery_add_information_title']                 = 'Informasi';
$lang['gallery_add_post_gallery_btn']                  = 'Unggah Gambar';

// Profile Article
$lang['profile_heading']                            = 'Profil';
$lang['profile_identifier_1']                       = 'Beranda';
$lang['profile_identifier_2']                       = 'Profil';
$lang['profile_form_title']                         = 'Set Profil';
$lang['profile_vision_lbl']                         = 'Visi:';
$lang['profile_mission_lbl']                        = 'Misi:';
$lang['profile_history_lbl']                        = 'Sejarah Singkat:';
$lang['profile_edit_btn']                           = 'Perbarui Profil';

// Courses Home
$lang['courses_home_heading']                           = 'Program Kursus';
$lang['courses_home_new_courses_btn']                   = 'Program Kursus Baru';
$lang['courses_home_identifier_1']                      = 'Beranda';
$lang['courses_home_identifier_2']                      = 'Program Kursus';
$lang['courses_home_title_lbl']                         = 'Nama Program Kursus';
$lang['courses_home_price_lbl']		                    = 'Harga';
$lang['courses_home_actions_lbl']                       = 'Aksi';
$lang['courses_home_edit_courses_btn']                  = 'Sunting';
$lang['courses_home_delete_alert'] 						= 'Apa anda yakin ingin menghapus Program Kursus ini?';
$lang['courses_home_delete_courses_btn']                = 'Hapus Kursus';

// New Courses
$lang['courses_add_identifier_1']                      = 'Beranda';
$lang['courses_add_identifier_2']                      = 'Tambah Program Kursus';
$lang['courses_add_form_title']                        = 'Tambah Kursus Baru';
$lang['courses_add_title_lbl']                         = 'Nama Kursus:';
$lang['courses_add_title_placeholder']                 = 'Tulis Nama Program Kursus';
$lang['courses_add_desc_lbl']                          = 'Deskripsi:';
$lang['courses_add_price_lbl']                         = 'Harga:';
$lang['courses_add_price_placeholder']                 = 'Tulis Harga Program Kursus';
$lang['courses_add_information_title']                 = 'Informasi';
$lang['courses_add_post_courses_btn']                  = 'Simpan Program Kursus';

//Edit Courses
$lang['courses_edit_identifier_1']                      = 'Beranda';
$lang['courses_edit_identifier_2']                      = 'Sunting Kursus';
$lang['courses_edit_form_title']                        = 'Sunting Program Kursus';
$lang['courses_edit_title_lbl']                         = 'Nama Program Kursus:';
$lang['courses_edit_title_placeholder']                 = 'Tulis Nama Program Kursus';
$lang['courses_edit_desc_lbl']                          = 'Deskripsi:';
$lang['courses_edit_price_lbl']                         = 'Harga:';
$lang['courses_edit_price_placeholder']                 = 'Tulis Harga Program Kursus';
$lang['courses_edit_information_title']                 = 'Informasi';
$lang['courses_edit_post_courses_btn']                  = 'Perbarui Program Kursus';

// Program Home
$lang['program_home_heading']                           = 'Program';
$lang['program_home_new_courses_btn']                   = 'Program Baru';
$lang['program_home_identifier_1']                      = 'Beranda';
$lang['program_home_identifier_2']                      = 'Program';
$lang['program_home_title_lbl']                         = 'Nama Program';
$lang['program_home_category_lbl']	                    = 'Kategori';
$lang['program_home_actions_lbl']                       = 'Aksi';
$lang['program_home_edit_program_btn']                  = 'Sunting';
$lang['program_home_delete_alert'] 						= 'Apa anda yakin ingin menghapus Program ini?';
$lang['program_home_delete_program_btn']                = 'Hapus Program';

// New Program
$lang['program_add_identifier_1']                      = 'Beranda';
$lang['program_add_identifier_2']                      = 'Tambah Program';
$lang['program_add_form_title']                        = 'Tambah Program Baru';
$lang['program_add_title_lbl']                         = 'Nama Program:';
$lang['program_add_title_placeholder']                 = 'Tulis Nama Program';
$lang['program_add_desc_lbl']                          = 'Deskripsi Program:';
$lang['program_add_information_title']                 = 'Informasi';
$lang['program_add_select_category_lbl']               = 'Pilih Kategori:';
$lang['program_add_post_program_btn']                  = 'Simpan Program';

// Edit Program
$lang['program_edit_identifier_1']                      = 'Beranda';
$lang['program_edit_identifier_2']                      = 'Sunting Program';
$lang['program_edit_form_title']                        = 'Sunting Program';
$lang['program_edit_title_lbl']                         = 'Nama Program:';
$lang['program_edit_title_placeholder']                 = 'Tulis Nama Program';
$lang['program_edit_desc_lbl']                          = 'Deskripsi Program:';
$lang['program_edit_information_title']                 = 'Informasi';
$lang['program_edit_select_category_lbl']               = 'Pilih Kategori:';
$lang['program_edit_program_btn']                       = 'Sunting Program';

// Set Contact
$lang['contact_heading']                            = 'Kontak';
$lang['contact_identifier_1']                       = 'Home';
$lang['contact_identifier_2']                       = 'Kontak';
$lang['contact_form_title']                         = 'Set Kontak';
$lang['contact_edit_email_lbl']                     = 'Email:';
$lang['contact_edit_phone_lbl']                     = 'Telepon:';
$lang['contact_edit_fax_lbl']                   	= 'Fax:';
$lang['contact_edit_address_lbl']                   = 'Alamat:';
$lang['contact_edit_city_lbl']                     	= 'Kota:';
$lang['contact_edit_postal_code_lbl']               = 'Kode Pos:';
$lang['contact_edit_province_lbl']                  = 'Provinsi:';
$lang['contact_edit_country_lbl']                   = 'Negara:';
$lang['contact_edit_btn']                           = 'Update Contact';