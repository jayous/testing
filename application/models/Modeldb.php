<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Modeldb extends CI_Model {
	var $tabel;

    public function setTabel($tabel = null) {
        $this->tabel = $tabel;
    }

    public function read() {
		$result = $this->db->get($this->tabel);
		return $result;
    }

    public function read_id($param = null, $value = null) {
        try {
            $result = $this->db->get_where($this->tabel, array($param => $value));
            return $result;
        } catch(Exception $e) {
            return $e;
        }
    }

    public function read_last() {
        $this->db->order_by('id', 'DESC');
        $this->db->limit(1);
        return $result = $this->db->get($this->tabel);
    }

    public function create($data) {
        try {
			$this->db->insert($this->tabel, $data);
			return true;
        } catch(Exception $e) {
			return $e;
		}
    }

    public function update($data,$param = null, $value = null) {
        try {
            $this->db->where($param,$value)->limit(1)->update($this->tabel, $data);
            return true;
        } catch (Exception $e) {
            return $e;
        }
    }
    
    public function delete($param = null, $id = null) {
        try {
            $this->db->where($param ,$id)->delete($this->tabel);
            return true;
        } catch (Exception $e) {
            return $e;
        }
    }
    public function get_galery_data($number = NULL, $offset = NULL){
        $this->db->select('*');
        $this->db->from('file_galeri');
        $this->db->limit($number, $offset);
        $this->db->order_by('id', 'DESC');
        return $this->db->get();
    }
    public function get_artikel_detail($param = null, $value = null){
        $this->db->select('users.first_name, users.last_name, artikel.id, artikel.tanggal_post, artikel.konten ,artikel.judul, artikel.diubah, artikel.kategori_artikel_id, artikel.slug, kategori_artikel.nama, file.file_loc');
        $this->db->from('artikel');
        $this->db->join('kategori_artikel', 'artikel.kategori_artikel_id = kategori_artikel.id');
        $this->db->join('file','artikel.id = file.artikel_id');
        $this->db->join('users','users.id = artikel.users_id');
        $this->db->where($param, $value);
        return $this->db->get();
    }
    public function get_artikel_kategori($where = NULL ,$number = NULL, $offset = NULL){
        $this->db->select('users.first_name, users.last_name, artikel.id, artikel.tanggal_post, artikel.konten ,artikel.judul, artikel.diubah, artikel.kategori_artikel_id, artikel.slug, kategori_artikel.nama, file.file_loc');
        $this->db->from('artikel');
        $this->db->join('kategori_artikel', 'artikel.kategori_artikel_id = kategori_artikel.id');
        $this->db->join('file','artikel.id = file.artikel_id');
        $this->db->join('users','users.id = artikel.users_id');
        $this->db->where($where);
        $this->db->limit($number, $offset);
        $this->db->order_by('id', 'DESC');
        return $this->db->get();
    }
    public function get_artikel_data($number = NULL, $offset = NULL){
        $this->db->select('users.first_name, users.last_name, artikel.id, artikel.tanggal_post, artikel.konten ,artikel.judul, artikel.diubah, artikel.kategori_artikel_id, artikel.slug, kategori_artikel.nama, file.file_loc');
        $this->db->from('artikel');
        $this->db->join('kategori_artikel', 'artikel.kategori_artikel_id = kategori_artikel.id');
        $this->db->join('file','artikel.id = file.artikel_id');
        $this->db->join('users','users.id = artikel.users_id');
        $this->db->limit($number, $offset);
        $this->db->order_by('id', 'DESC');
        return $this->db->get();
    }
    public function get_artikel(){
        $this->db->select('users.first_name, users.last_name, artikel.id, artikel.tanggal_post, artikel.konten ,artikel.judul, artikel.diubah, artikel.kategori_artikel_id, artikel.slug, kategori_artikel.nama, file.file_loc');
        $this->db->from('artikel');
        $this->db->join('kategori_artikel', 'artikel.kategori_artikel_id = kategori_artikel.id');
        $this->db->join('file','artikel.id = file.artikel_id');
        $this->db->join('users','users.id = artikel.users_id');
        $this->db->order_by('id', 'DESC');
        return $this->db->get();
    }
    public function get_latest_artikel($data = null){
        $this->db->select('users.first_name, users.last_name, artikel.id, artikel.tanggal_post, artikel.konten ,artikel.judul, artikel.diubah, artikel.kategori_artikel_id, artikel.slug, kategori_artikel.nama, file.file_loc');
        $this->db->from('artikel');
        $this->db->join('kategori_artikel', 'artikel.kategori_artikel_id = kategori_artikel.id');
        $this->db->join('file','artikel.id = file.artikel_id');
        $this->db->join('users','users.id = artikel.users_id');
        $this->db->limit($data);
        $this->db->order_by('id', 'DESC');
        return $this->db->get();
    }
    public function get_kategori_artikel($id){
        $this->db->select('*');
        $this->db->from('artikel');
        $this->db->join('kategori_artikel', 'artikel.kategori_artikel_id = kategori_artikel.id');
        $this->db->where('artikel.id', $id);
        return $this->db->get();
    }

    public function get_program(){
        $this->db->select('program.id, program.judul, kategori_program.nama, program.slug');
        $this->db->from('program');
        $this->db->join('kategori_program', 'program.kategori_program_id = kategori_program.id');
        return $this->db->get();
    }

    
}