<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ModelComments extends CI_Model {
    private $_table = "comments";

    public $users_name;
    public $users_email;
    public $users_ip;
    public $comment_content;
    public $artikel_id;

    public function read($id = NULL){
        $this->db->select('*');
        $this->db->from($this->_table);
        $this->db->where('artikel_id', $id);
        $this->db->order_by('id', 'DESC');
        return $this->db->get();
    }

    public function save(){

        $this->users_name = $this->input->post('name');
        $this->users_email = $this->input->post('email');
        $this->users_ip = $this->input->ip_address();
        $this->comment_content = $this->input->post('message');
        $this->artikel_id = $this->input->post('artikel_id');

        $this->db->insert($this->_table, $this);
    }
}