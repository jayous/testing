<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ModelPersons extends CI_Model
{
    private $_table      = "persons";
    private $_fk_table_1 = "file_persons";

    public $id;
    public $full_name;
    public $place_of_birth;
    public $date_of_birth;
    public $gender;
    public $school;
    public $home_address;
    public $mobile_number;
    public $class;

    public function get()
    {
        $this->db->select('p.*, fp.file_loc');
        $this->db->from($this->_table.' AS p');
        $this->db->join($this->_fk_table_1.' AS fp', 'p.id = fp.person_id');
        $this->db->order_by('p.id', 'DESC');
        return $this->db->get();
    }

    public function get_where($where = NULL)
    {
        $this->db->select('p.*, fp.file_loc');
        $this->db->from($this->_table.' AS p');
        $this->db->join($this->_fk_table_1.' AS fp', 'p.id = fp.person_id');
        $this->db->where($where);
        $this->db->order_by('p.id', 'DESC');
        return $this->db->get();
    }

    public function add()
    {
        $input                  = $this->input->post();

        $this->full_name        = $input['full_name'];
        $this->place_of_birth   = $input['place_of_birth'];
        $this->date_of_birth    = $input['date_of_birth'];
        $this->gender           = $input['gender'];
        $this->school           = $input['school'];
        $this->home_address     = $input['home_address'];
        $this->mobile_number    = $input['mobile_number'];
        $this->class            = $input['class'];
        
        $this->db->insert($this->_table, $this);
        $this->id                   = $this->db->insert_id();
    }

    public function update()
    {
        $input                  = $this->input->post();

        $this->id               = $input['id'];
        $this->full_name        = $input['full_name'];
        $this->place_of_birth   = $input['place_of_birth'];
        $this->date_of_birth    = $input['date_of_birth'];
        $this->gender           = $input['gender'];
        $this->school           = $input['school'];
        $this->home_address     = $input['home_address'];
        $this->mobile_number    = $input['mobile_number'];
        $this->class            = $input['class'];

        $this->db->where('id', $this->id);
        $this->db->update($this->_table, $this);
    }

    public function delete()
    {
        $this->db->delete($this->_table, array('id' => $this->id));
    }

    public function rules()
    {
        $rules = array(
            [
                'field' => 'full_name',
                'rules' => 'trim|required',
                'errors' => array(
                    'required' => '<span class="text-danger">Nama Tidak Boleh Kosong</span>',
                )
            ],
            [
                'field' => 'place_of_birth',
                'rules' => 'trim|required',
                'errors' => array(
                    'required' => '<span class="text-danger">Tempat Lahir Tidak Boleh Kosong</span>',
                )
            ],
            [
                'field' => 'date_of_birth',
                'rules' => 'trim|required',
                'errors' => array(
                    'required' => '<span class="text-danger">Tanggal Lahir Tidak Boleh Kosong</span>',
                )
            ],
            [
                'field' => 'gender',
                'rules' => 'trim|required|callback_pilih',
                'errors' => array(
                    'required'  => '<span class="text-danger">Jenis Kelamin Tidak Boleh Kosong</span>',
                    'pilih'    => '<span class="text-danger">Jenis Kelamin Tidak Boleh Kosong</span>'
                )
            ],
            [
                'field' => 'school',
                'rules' => 'trim|required',
                'errors' => array(
                    'required' => '<span class="text-danger">SekolahTidak Boleh Kosong</span>',
                )
            ],
            [
                'field' => 'home_address',
                'rules' => 'trim|required',
                'errors' => array(
                    'required' => '<span class="text-danger">Alamat Rumah Tidak Boleh Kosong</span>',
                )
            ],
            [
                'field' => 'mobile_number',
                'rules' => 'trim|required',
                'errors' => array(
                    'required' => '<span class="text-danger">No Tlp Tidak Boleh Kosong</span>',
                )
            ],
            [
                'field' => 'class',
                'rules' => 'trim|required|callback_pilih',
                'errors' => array(
                    'required' => '<span class="text-danger">Kelas Tidak Boleh Kosong</span>',
                    'pilih' => '<span class="text-danger">Kelas Tidak Boleh Kosong</span>',
                )
            ],
        );

        return $rules;
    }
    public function _id($data = NULL)
    {
        return array(
            'name'          => 'id',
            'type'          => 'hidden',
            'value'         => $data == NULL ? $this->form_validation->set_value('id') : $data,
            'class'         => 'form-control',
            'placeholder'   => 'Masukan Nama Lengkap',
        );
    }

    public function _full_name($data = NULL)
    {
        return array(
            'name'          => 'full_name',
            'type'          => 'text',
            'value'         => $data == NULL ? $this->form_validation->set_value('full_name') : $data,
            'class'         => 'form-control',
            'placeholder'   => 'Masukan Nama Lengkap',
        );
    }

    public function _place_of_birth($data = NULL)
    {
        return array(
            'name'          => 'place_of_birth',
            'type'          => 'text',
            'value'         => $data == NULL ? $this->form_validation->set_value('place_of_birth') : $data,
            'class'         => 'form-control',
            'placeholder'   => 'Masukan Tempat Lahir',
        );
    }

    public function _date_of_birth($data = NULL)
    {
        return array(
            'name'          => 'date_of_birth',
            'type'          => 'date',
            'value'         => $data == NULL ? $this->form_validation->set_value('date_of_birth') : $data,
            'class'         => 'form-control',
            'placeholder'   => 'Masukan Tanggal Lahir',
        );
    }

   public function _gender($data = NULL)
   {
       return array(
            'S'     => $data == NULL ? 'Pilih' : $data,
            'P'     => 'Perempuan',
            'L'     => 'Laki-Laki',
        );
   }

   public function _gender_sunting($data = NULL)
   {
       return array(
            $data     => $data == NULL ? 'Pilih' : $data,
            'P'     => 'Perempuan',
            'L'     => 'Laki-Laki',
        );
   }

    public function _school($data = NULL)
    {
        return array(
            'name'          => 'school',
            'type'          => 'text',
            'value'         => $data == NULL ? $this->form_validation->set_value('school') : $data,
            'class'         => 'form-control',
            'placeholder'   => 'Masukan Nama Sekolah',
        );
    }

    public function _home_address($data = NULL)
    {
        return array(
            'name'          => 'home_address',
            'type'          => 'textarea',
            'value'         => $data == NULL ? $this->form_validation->set_value('home_address') : $data,
            'class'         => 'form-control',
            'placeholder'   => 'Masukan Alamat Rumah',
        );
    }

    public function _mobile_number($data = NULL)
    {
        return array(
            'name'          => 'mobile_number',
            'type'          => 'number',
            'value'         => $data == NULL ? $this->form_validation->set_value('mobile_number') : $data,
            'class'         => 'form-control',
            'placeholder'   => 'Masukan No Tlp',
        );
    }

    public function _class($data = NULL)
    {
        return array(
            'S'     => $data == NULL ? 'Pilih' : $data,
            'N1'     => 'N1',
        );
    }

    public function _class_sunting($data = NULL)
    {
        return array(
            $data     => $data == NULL ? 'Pilih' : $data,
            'N1'     => 'N1',
        );
    }
}