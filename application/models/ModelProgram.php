<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ModelProgram extends CI_Model 
{
    private $_table = "program";
    private $_fk_table_1 = "kategori_program";
    private $_fk_table_2 = "file_program";

    public $id;
    public $judul;
    public $slug;
    public $konten;
    public $tanggal;
    public $kategori_program_id;

    public function rules()
    {
        $rules = array(
            [
                'field' => 'judul',
                'label' => 'Judul',
                'rules' => 'trim|required|callback_judul_exist',
                'errors' => array(
                    'required' => '<span class="text-danger">Nama Tidak Boleh Kosong</span>',
                    'judul_exist' => '<span class="text-danger">Mohon Gunakan Nama yang Lain</span>'
                )
            ],
            [
                'field' => 'konten',
                'label' => 'Isi Konten',
                'rules' => 'trim|required',
                'errors' => array(
                    'required' => '<span class="text-danger">Konten Tidak Boleh Kosong</span>'
                )
            ]
        );

        return $rules;
    }

    public function rules_ubah()
    {
        $rules = array(
            [
                'field' => 'judul',
                'label' => 'Judul',
                'rules' => 'trim|required|callback_ubah_judul_exist',
                'errors' => array(
                    'required' => '<span class="text-danger">Nama Tidak Boleh Kosong</span>',
                    'judul_exist' => '<span class="text-danger">Mohon Gunakan Nama yang Lain</span>'
                )
            ],
            [
                'field' => 'konten',
                'label' => 'Isi Konten',
                'rules' => 'trim|required',
                'errors' => array(
                    'required' => '<span class="text-danger">Konten Tidak Boleh Kosong</span>'
                )
            ]
        );

        return $rules;
    }

    public function read(){
        $this->db->select('p.*, kp.nama, fp.file_loc');
        $this->db->from($this->_table.' AS p');
        $this->db->join($this->_fk_table_1.' AS kp', 'p.kategori_program_id = kp.id');
        $this->db->join($this->_fk_table_2.' AS fp', 'p.id = fp.program_id');
        $this->db->order_by('p.id', 'DESC');
        return $this->db->get();
    }

    public function read_where($where = NULL){
        $this->db->select('p.*, kp.nama, fp.file_loc');
        $this->db->from($this->_table.' AS p');
        $this->db->join($this->_fk_table_1.' AS kp', 'p.kategori_program_id = kp.id');
        $this->db->join($this->_fk_table_2.' AS fp', 'p.id = fp.program_id');
        $this->db->where($where);
        $this->db->order_by('p.id', 'DESC');
        return $this->db->get();
    }
    public function get_page_data($number = NULL, $offset = NULL, $where = NULL){
        $this->db->select('p.*, kp.nama, fp.file_loc');
        $this->db->from($this->_table.' AS p');
        $this->db->join($this->_fk_table_1.' AS kp', 'p.kategori_program_id = kp.id');
        $this->db->join($this->_fk_table_2.' AS fp', 'p.id = fp.program_id');
        $this->db->where($where);
        $this->db->limit($number, $offset);
        $this->db->order_by('p.id', 'DESC');
        return $this->db->get();
    }
    public function add()
    {
        $input = $this->input->post();

        $this->judul                = $input['judul'];
        $this->slug                 = strtolower(url_title($input['judul']));
        $this->konten               = $input['konten'];
        $this->kategori_program_id  = $input['kategori'];
        $this->tanggal              = time();

        $this->db->insert($this->_table, $this);
        $this->id                   = $this->db->insert_id();
    }

    public function update()
    {
        $input = $this->input->post();

        $this->id                   = $input['id'];
        $this->judul                = $input['judul'];
        $this->slug                 = strtolower(url_title($input['judul']));
        $this->konten               = $input['konten'];
        $this->kategori_program_id  = $input['kategori'];
        $this->tanggal              = time();

        $this->db->where('id', $this->id);
        $this->db->update($this->_table, $this);
    }

    public function delete()
    {
        $this->db->delete($this->_table, array('slug' => $this->slug));
    }
}