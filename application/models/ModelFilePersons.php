<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ModelFilePersons extends CI_Model 
{
    private $_table = "file_persons";

    public $file_name;
    public $file_size;
    public $file_type;
    public $file_loc;
    public $person_id;

    public function data($f_name, 
                         $f_size,
                         $f_type,
                         $f_loc,
                         $person_id )
    {
        $this->file_name  = $f_name;
        $this->file_size  = $f_size;
        $this->file_type  = $f_type;
        $this->file_loc   = $f_loc;
        $this->person_id  = $person_id;
    }

    public function get_where($where = NULL)
    {
        $this->db->select('*');
        $this->db->from($this->_table);
        $this->db->where($where);
        return $this->db->get();
    }

    public function add()
    {
        $this->db->insert($this->_table, $this);
    }

    public function update()
    {
        $this->db->where('person_id', $this->program_id);
        $this->db->update($this->_table, $this);
    }

    public function delete()
    {
        $this->db->delete($this->_table, array('person_id' => $this->program_id));
    }

    public function upload_config()
    {
        $config = [
            'upload_path' => './assets/images/persons/temp',
            'allowed_types' => 'jpg|png|jpeg',
            'file_name' => $this->file_name,
            'max_size' => 2048,
            'max_width' => 384,
            'max_height' => 502,
            'overwrite' => FALSE,
            'file_ext_tolower' => TRUE,
            'max_filename' => 100
        ];

        return $config;
    }

    public function resize_config()
    {
        $config = [
            'image_library' => 'gd2',
            'source_image' => './assets/images/persons/temp/'.$this->file_name,
            'create_thumb' => FALSE,
            'maintain_ratio' => TRUE,
            'width' => 354,
            'quality' => '80%',
            'maintain_ratio' => true,
            'new_image' => "./assets/images/persons/".date('Y-m-d').'/'.$this->file_name,
        ];

        return $config;
    }
}