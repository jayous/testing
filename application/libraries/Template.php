<?php

if ( ! defined('BASEPATH'))  exit('No direct script access allowed');

class Template {
	var $template_data = array();

	function view_frontend($content = '', $data = array()){
		$CI 				=& get_instance();
		$dir_template		= "frontend/template";
		$data['content']	= "frontend/".$content;
		$CI->load->view($dir_template,$data);	
	}

	function view_backend($content = '', $data = array()){
		$CI 				=& get_instance();
		$dir_template		= "backend/template";
		$data['content']	= "backend/".$content;
		$CI->load->view($dir_template,$data);	
	}
	

    function set($nama, $value)
    {
        $this->template_data[$nama] = $value;
    }

    function load_frontend($template = '', $view = '', $view_data = array(), $return = FALSE)
    {
        $this->ci = get_instance();
        $this->set('contents', $this->ci->load->view($view, $view_data, TRUE) );
        return $this->ci->load->view("frontend/".$template, $this->template_data, $return);
	}
	function load_backend($template = '', $view = '', $view_data = array(), $return = FALSE)
    {
        $this->ci = get_instance();
        $this->set('contents', $this->ci->load->view($view, $view_data, TRUE) );
        return $this->ci->load->view("backend/".$template, $this->template_data, $return);
    }
}
?>