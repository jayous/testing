<?php
class LanguageLoader
{
   function initialize() {
 
       $ci =& get_instance();
 
       $ci->load->helper('language');
 
       $siteLang = $ci->session->userdata('site_lang');
 
       if ($siteLang) {
 
           $ci->lang->load('auth',$siteLang);
           $ci->lang->load('form_validation',$siteLang);
           $ci->lang->load('ion_auth',$siteLang);
 
       } else {
 
           $ci->lang->load('auth','indonesian');
           $ci->lang->load('form_validation','indonesian');
           $ci->lang->load('ion_auth','indonesian');
 
       }
 
   }
}