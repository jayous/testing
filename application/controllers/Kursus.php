<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kursus extends CI_Controller {

	function __construct() {
		parent:: __construct();
		$this->load->model('Modeldb');
		$this->load->library('form_validation');
		$this->load->helper(array('language'));
        $this->lang->load('auth');
	}

	public function index() {
		$this->Modeldb->setTabel('kursus');
		$data['program'] = $this->Modeldb->read();
		$this->template->load_backend('template', 'backend/contents/kursus-home', $data);
	}

	public function baru() {
		if (ISSET($_POST['tambah'])) {
			$rules = array(
				[
					'field' => 'nama',
					'label' => 'nama',
					'rules' => 'trim|required|callback_nama_exist',
					'errors' => array(
						'required' => '<span class="text-danger">Nama Program Tidak Boleh Kosong</span>',
                        'nama_exist' => '<span class="text-danger">Mohon Gunakan Nama yang Lain</span>'
					)
				],
				[
					'field' => 'deskripsi',
					'label' => 'deskripsi',
					'rules' => 'trim|required',
					'errors' => array(
						'required' => '<span class="text-danger">Deskripsi Tidak Boleh Kosong</span>'
					)
				],
				[
					'field' => 'harga',
					'label' => 'harga',
					'rules' => 'trim|required|integer',
					'errors' => array(
						'required' => '<span class="text-danger">Harga Tidak Boleh Kosong</span>',
						'integer' => '<span class="text-danger">Harga Harus Angka</span>'
					)
				]
			);
			$this->form_validation->set_rules($rules);
			if ($this->form_validation->run() == FALSE) {
				$this->template->load_backend('template', 'backend/contents/kursus-baru');
			} else {
				$this->Modeldb->setTabel('kursus');
				$data_array = array(
					'nama' => $this->input->post('nama'),
					'slug' => strtolower(url_title($this->input->post('nama'))),
					'deskripsi' => $this->input->post('deskripsi'),
					'harga' => $this->input->post('harga')
				);
					$this->Modeldb->setTabel('kursus');
					$this->Modeldb->create($data_array);

				$this->session->set_flashdata('program_sukses', '<div class="alert alert-success fade-in"><a href="#" class="close" style="text-decoration: none;" data-dismiss="alert" aria-label="close">&times;</a><strong>Program Kursus berhasil di Tambahkan</strong></div>');
				redirect('kursus');
			}
		} else {
			$this->template->load_backend('template', 'backend/contents/kursus-baru');
		}
	}

	public function sunting($slug = null) {
		if (ISSET($_POST['sunting'])) {
			$rules = array(
				[
					'field' => 'nama',
					'label' => 'nama',
					'rules' => 'trim|required|callback_ubah_nama_exist',
					'errors' => array(
						'required' => '<span class="text-danger">Nama Program Tidak Boleh Kosong</span>',
                        'ubah_nama_exist' => '<span class="text-danger">Mohon Gunakan Nama yang Lain</span>'
					)
				],
				[
					'field' => 'harga',
					'label' => 'harga',
					'rules' => 'trim|required|integer',
					'errors' => array(
						'required' => '<span class="text-danger">Harga Tidak Boleh Kosong</span>',
						'integer' => '<span class="text-danger">Harga Harus Angka</span>'
					)
				]
			);
			$this->form_validation->set_rules($rules);
			if ($this->form_validation->run() == FALSE) {
				$this->Modeldb->setTabel('kursus');
                $data['slug'] = $this->Modeldb->read_id('slug', $slug);
                $slugg = $data['slug']->row();
                $this->session->set_userdata('slug', $slugg->slug);

                $data['program'] = $this->Modeldb->read_id('slug', $slug)->row();
				$this->template->load_backend('template', 'backend/contents/kursus-sunting', $data);
			} else {
				$this->Modeldb->setTabel('kursus');
				$data_array = array(
					'nama' => $this->input->post('nama'),
					'slug' => strtolower(url_title($this->input->post('nama'))),
					'deskripsi' => $this->input->post('deskripsi'),
					'harga' => $this->input->post('harga')
				);
				$this->session->set_flashdata('program_sukses', '<div class="alert alert-success fade-in"><a href="#" class="close" style="text-decoration: none;" data-dismiss="alert" aria-label="close">&times;</a><strong>Program Kursus berhasil di Perbarui</strong></div>');
				$this->Modeldb->setTabel('kursus');
				$this->Modeldb->update($data_array, 'id', $this->input->post('idlama'));
                redirect('kursus');
			}
		} else {
			$this->Modeldb->setTabel('kursus');
			$data['program'] = $this->Modeldb->read_id('slug', $slug)->row();
			$this->template->load_backend('template', 'backend/contents/kursus-sunting', $data);
		}
	}

	public function hapus($slug = null) {
		$this->Modeldb->setTabel('kursus');
		if ($this->Modeldb->delete('slug', $slug)) {
			$this->session->set_flashdata('program_sukses', '<div class="alert alert-success fade-in"><a href="#" class="close" style="text-decoration: none;" data-dismiss="alert" aria-label="close">&times;</a><strong>Program Kursus berhasil di Hapus</strong></div>');
			redirect('kursus');
		}
	}

	function nama_exist($nama){
        $this->Modeldb->setTabel('kursus');
        $data = $this->Modeldb->read_id('slug', strtolower(url_title($nama)))->row();
        if ($data != null) {
        	return false;
        } else {
        	return true;
        }
    }

    function ubah_nama_exist($nama){
            $this->Modeldb->setTabel('kursus');
            $data = $this->Modeldb->read_id('slug', strtolower(url_title($nama)))->row();
            if (strtolower(url_title($nama)) == $this->session->userdata('slug')) {
                return true;
            } else if ($data != null) {
                return false;
            } else {
                return true;
            }
        }
}