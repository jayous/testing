<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kontak extends CI_Controller {
	function __construct() {
		parent:: __construct();
		$this->load->model('Modeldb');
		$this->load->library('form_validation');
        $this->load->helper(array('language'));
        $this->lang->load('auth');
	}

	public function index() {
		if (ISSET($_POST['set_kontak'])) {
			$array_data = array(
                'email' => $this->input->post('email'),
                'telepon' => $this->input->post('telepon'),
                'fax' => $this->input->post('fax'),
                'alamat' => $this->input->post('alamat'),
                'kota' => $this->input->post('kota'),
                'kodepos' => $this->input->post('kodepos'),
                'provinsi' => $this->input->post('provinsi'),
                'negara' => $this->input->post('negara')
            );
			$this->Modeldb->setTabel('kontak');
			$this->Modeldb->update($array_data, 'id', '1');
			if (!$this->Modeldb->update($array_data, 'id', '1')) {
				$this->session->set_flashdata('set_gagal', '<div class="alert alert-danger fade-in"><a href="#" class="close" style="text-decoration: none;" data-dismiss="alert" aria-label="close">&times;</a><strong>Kontak Gagal di perbarui</strong></div>');
				redirect('kontak');
			} else {
				$this->session->set_flashdata('set_sukses', '<div class="alert alert-success fade-in"><a href="#" class="close" style="text-decoration: none;" data-dismiss="alert" aria-label="close">&times;</a><strong>Kontak Perbarui di Perbarui</strong></div>');
				redirect('kontak');
			}
		} else {
			$this->Modeldb->setTabel('kontak');
			if (empty($this->Modeldb->read()->result())) {
                $data_array = array(
                    'email' => 'hikari@gmail.com',
                    'telepon' => '0857-1234-5678',
                    'fax' => '+1 (123) 456 7891 ',
                    'alamat' => 'Jl. Bayangkara No.10 Blok.5',
                    'kota' => 'Sukabumi',
                    'kodepos' => '43154',
                    'provinsi' => 'Jawa Barat',
                    'negara' => 'Indonesia' 
                );
                $this->Modeldb->create($data_array);
                $this->index();
        	} else {
				$data['kontak'] = $this->Modeldb->read()->row();
				$this->template->load_backend('template', 'backend/contents/setting-kontak', $data);
			}
		}
	}
}