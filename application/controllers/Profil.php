<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profil extends CI_Controller {

	function __construct() {
		parent:: __construct();
		$this->load->model('Modeldb');
		$this->load->library('form_validation');
        $this->load->helper(array('language'));
        $this->lang->load('auth');
	}

	public function index() {
		if (ISSET($_POST['set_profil'])) {
			$array_data = array(
				'visi' => $this->input->post('visi'),
				'misi' => $this->input->post('misi'),
				'sejarah' => $this->input->post('sejarah')
			);
			$this->Modeldb->setTabel('profil');
			$this->Modeldb->update($array_data, 'id', '1');
			if (!$this->Modeldb->update($array_data, 'id', '1')) {
				$this->session->set_flashdata('set_gagal', '<div class="alert alert-danger fade-in"><a href="#" class="close" style="text-decoration: none;" data-dismiss="alert" aria-label="close">&times;</a><strong>Profil Gagal di perbarui</strong></div>');
				redirect('profil');
			} else {
				$this->session->set_flashdata('set_sukses', '<div class="alert alert-success fade-in"><a href="#" class="close" style="text-decoration: none;" data-dismiss="alert" aria-label="close">&times;</a><strong>Profil Perbarui di Perbarui</strong></div>');
				redirect('profil');
			}
		} else {
			$this->Modeldb->setTabel('profil');
			if (empty($this->Modeldb->read()->result())) {
				$data_array = array(
					'id' => '1',
					'visi' => 'Isi Visi',
					'misi' => 'Isi Misi',
					'sejarah' => 'Isi Sejarah'
				);
				$this->Modeldb->create($data_array);
				redirect('profil');
			} else {
				$data['profil'] = $this->Modeldb->read()->row();
				$this->template->load_backend('template', 'backend/contents/setting-profil', $data);
			}
		}
	}
}