<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class News extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('Modeldb');
		$this->load->library('pagination');
		$this->lang->load('auth');
	}
	public function index()
	{
        //get total row
        $this->Modeldb->setTabel('artikel');
        $total_row = $this->Modeldb->read()->num_rows();

        //pagging setting
        $config = array();
        $config['base_url'] = base_url("news/index");
        $config['total_rows'] = $total_row;
        $config['per_page'] = 6;

        //pagging design
        $config['full_tag_open'] = "<div class='blog-pagination'><ul class='pagination'>";
        $config['full_tag_close'] = '</ul></div>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="">';
        $config['cur_tag_close'] = '</a></li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';



        $config['prev_link'] = 'Previous Page';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';


        $config['next_link'] = 'Next Page';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        // end

        $from = $this->uri->segment(3);
        $this->pagination->initialize($config);

        $data = array(
            'blog_title' => 'Hikari - Berita',
            'blog_heading' => 'Berita',
        );
        
        $data['blog']           = $this->Modeldb->get_artikel_data($config['per_page'], $from)->result_array();
        $this->Modeldb->setTabel('kategori_artikel');
        $data['kategori']       = $this->Modeldb->read()->result_array();
        $data['blog_latest']    = $this->Modeldb->get_latest_artikel(5)->result_array();

        $this->Modeldb->setTabel('comments');
        $data['komen']          = $this->Modeldb->read()->result_array();
        $this->parser->parse('frontend/header/header', $data);
        $this->load->view('frontend/content/berita', $data);
        $this->parser->parse('frontend/footer/footer', $data);
    }
    public function page($slug = NULL){
        $data = array(
            'blog_title' => 'Hikari - Berita',
            'blog_heading' => 'Berita',
        );
        $this->Modeldb->setTabel('artikel');
        $data['blog_detail'] = $this->Modeldb->get_artikel_detail('artikel.slug', $slug)->result_array();
        $this->Modeldb->setTabel('kategori_artikel');
        $data['kategori'] = $this->Modeldb->read()->result_array();
        $data['blog_latest']    = $this->Modeldb->get_latest_artikel(5)->result_array();
        $this->parser->parse('frontend/header/header', $data);
        $this->load->view('frontend/content/blog_detail', $data);
        $this->parser->parse('frontend/footer/footer', $data);
    }
    public function categories($slug = null){
        if ($slug == null) {
            redirec('news');
        }

        //get categories
        $this->Modeldb->setTabel('kategori_artikel');
        $categories = $this->Modeldb->read_id('slug', $slug);

        if ($categories->result() == NULL) {
            redirect('news');
        }
        
        //get categories'artikel
            //get total row categories'artikel
            $this->Modeldb->setTabel('artikel');
            $total_row = $this->Modeldb->read_id('kategori_artikel_id', $categories->row()->id)->num_rows();

        //pagging setting
        $config = array();
        $config['base_url'] = base_url("news/categories/".$categories->row()->slug."/index/");
        $config['total_rows'] = $total_row;
        $config['per_page'] = 6;

        //pagging design
        $config['full_tag_open'] = "<div class='blog-pagination'><ul class='pagination'>";
        $config['full_tag_close'] = '</ul></div>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="">';
        $config['cur_tag_close'] = '</a></li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';



        $config['prev_link'] = 'Previous Page';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';


        $config['next_link'] = 'Next Page';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        // end

        $from = $this->uri->segment(5);
        $this->pagination->initialize($config);

        $data = array(
            'blog_title' => 'Hikari - Berita',
            'blog_heading' => 'Berita',
        );
        
        $array_data = array(
            'kategori_artikel_id' =>  $categories->row()->id,
        );
        $this->Modeldb->setTabel('artikel');
        $data['blog']           = $this->Modeldb->get_artikel_kategori($array_data ,$config['per_page'], $from)->result_array();
        $this->Modeldb->setTabel('kategori_artikel');
        $data['kategori']       = $this->Modeldb->read()->result_array();
        $data['blog_latest']    = $this->Modeldb->get_latest_artikel(5)->result_array();
        $data['detail_kategori'] = $categories->row()->nama;

        
        $this->Modeldb->setTabel('comments');
        $data['komen']          = $this->Modeldb->read()->result_array();
        $this->parser->parse('frontend/header/header', $data);
        $this->load->view('frontend/content/berita', $data);
        $this->parser->parse('frontend/footer/footer', $data);
    }
}
