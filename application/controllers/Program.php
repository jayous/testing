<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Program extends CI_Controller 
{

    function __construct() 
    {
		parent:: __construct();
        $this->load->model('Modeldb');
        $this->load->model('ModelProgram');
        $this->load->model('ModelFileProgram');
		$this->load->library('form_validation');
        $this->load->helper(array('language'));
        $this->lang->load('auth');
	}

    public function index() 
    {
        $MP = $this->ModelProgram;
        $data['program'] = $MP->read();
        $this->template->load_backend('template', 'backend/contents/program-home', $data);
    }

    public function tampil_baru()
    {
        $this->Modeldb->setTabel('kategori_program');
        $data['kategori'] = $this->Modeldb->read();
        $this->template->load_backend('template', 'backend/contents/program-baru', $data);
    }

    public function tampil_sunting($slug = NULL)
    {
        $MP         = $this->ModelProgram;
        $data['program'] = $MP->read_where(['p.slug' => $slug])->row();

        $this->Modeldb->setTabel('kategori_program');
        $data['kategori'] = $this->Modeldb->read();

        if ($data['program'] == NULL) 
        {
            redirect('program');
        }
        else
        {
            $this->template->load_backend('template', 'backend/contents/program-sunting', $data);
        }
    }

    public function baru() 
    {
        $MP         = $this->ModelProgram;
        $MFP        = $this->ModelFileProgram;
        $validation = $this->form_validation;
        $upload     = $this->upload;
        $image      = $this->image_lib;

        // Jika tombol Terbit diklik
        if (ISSET($_POST['tambah']))
        {
            $rules = $MP->rules();
            $validation->set_rules($rules);

            if($validation->run() == false)
            {
               $this->tampil_baru();
            } 
            else 
            {
                if (empty($_FILES['userfile']['name']))
                {
                    $MP->add(); // save to program
                    $MFP->data(NULL, NULL, NULL, NULL, $MP->id);
                    $MFP->add();
                    //set message and redirect
                    $this->session->set_flashdata('program_sukses', '<div class="alert alert-success fade-in"><a href="#" class="close" style="text-decoration: none;" data-dismiss="alert" aria-label="close">&times;</a><strong>Program berhasil di Tambahkan</strong></div>');
                    redirect('program');
                } 
                else 
                {
                    $MFP->file_name = time().$_FILES["userfile"]['name'];
                    $upload->initialize($MFP->upload_config());

                    if (! $upload->do_upload()) 
                    {
                        $this->session->set_flashdata('upload', $this->upload->display_errors());
                        $this->tampil_baru();
                    }
                    else
                    {
                        $path = "./assets/images/program/".date('Y-m-d');
                        // Jika folder belum dibuat
                        if(!is_dir($path))
                        {
                            mkdir($path,0755,TRUE); // Buat folder
                        }

                        $file = $upload->data();
                        $MFP->file_name = $file['file_name'];
                        $image->initialize($MFP->resize_config());
                        $image->resize();

                        if (! $image->resize())
                        {
                            $this->session->set_flashdata('upload', $this->image_lib->display_errors());
                            $this->tampil_baru();
                        }
                        else
                        {
                            //delete real image
                            unlink('./assets/images/program/temp/'.$file['file_name']);
                            
                            $MP->add(); // save to program
                            $MFP->data( $file['file_name'], 
                                        $file['file_size'], 
                                        $file['file_type'], 
                                        './assets/images/program/'.date('Y-m-d').'/'.$file['file_name'], 
                                        $MP->id);
                            $MFP->add();
                            //set message and redirect
                            $this->session->set_flashdata('program_sukses', '<div class="alert alert-success fade-in"><a href="#" class="close" style="text-decoration: none;" data-dismiss="alert" aria-label="close">&times;</a><strong>Program berhasil di Tambahkan</strong></div>');
                            redirect('program');
                        }
                    }
                } 
            }
        } else {
           $this->tampil_baru();
		}
	}

    public function sunting($slug = null)
    {
        $MP         = $this->ModelProgram;
        $MFP        = $this->ModelFileProgram;
        $validation = $this->form_validation;
        $upload     = $this->upload;
        $image      = $this->image_lib;

        if (isset($_POST['sunting'])) 
        {    
            $rules = $MP->rules_ubah();
            $validation->set_rules($rules);

            if($validation->run() == false)
            {
                $this->tampil_sunting($slug);
            } 
            else 
            {
                if (empty($_FILES['userfile']['name']))
                {
                    //update data without image
                    $MP->update();
                    $this->session->set_flashdata('program_sukses', '<div class="alert alert-success fade-in"><a href="#" class="close" style="text-decoration: none;" data-dismiss="alert" aria-label="close">&times;</a><strong>Program berhasil di Perbarui</strong></div>');
                    redirect('program');   
                }
                else
                {
                    $MFP->file_name = time().$_FILES["userfile"]['name'];
                    $upload->initialize($MFP->upload_config());

                    if (! $upload->do_upload()) 
                    {
                        $this->session->set_flashdata('upload', $this->upload->display_errors());
                        $this->tampil_sunting($slug);
                    }
                    else
                    {
                        $path = "./assets/images/program/".date('Y-m-d');
                        // Jika folder belum dibuat
                        if(!is_dir($path))
                        {
                            mkdir($path,0755,TRUE); // Buat folder
                        }

                        $file = $upload->data();
                        $MFP->file_name = $file['file_name'];
                        $image->initialize($MFP->resize_config());
                        $image->resize();

                        if (! $image->resize())
                        {
                            $this->session->set_flashdata('upload', $this->image_lib->display_errors());
                            $this->tampil_sunting($slug);
                        }
                        else
                        {
                            //delete real image
                            unlink('./assets/images/program/temp/'.$file['file_name']);
                            unlink($this->input->post('lokasi_file_lama'));

                            $MP->update();
                            $MFP->data( $file['file_name'], 
                                        $file['file_size'], 
                                        $file['file_type'], 
                                        './assets/images/program/'.date('Y-m-d').'/'.$file['file_name'], 
                                        $MP->id);
                            $MFP->update();

                            $this->session->set_flashdata('program_sukses', '<div class="alert alert-success fade-in"><a href="#" class="close" style="text-decoration: none;" data-dismiss="alert" aria-label="close">&times;</a><strong>Program berhasil di Perbarui</strong></div>');
                            redirect('program'); 
                        }
                    }
                }
            }
        } else {
           $this->session->set_userdata('slug', $slug);
           $this->tampil_sunting($slug);
		}
	}

	public function hapus($slug = NULL, $id = NULL) {
        $MP         = $this->ModelProgram;
        $MFP        = $this->ModelFileProgram;

        $program            = $MP->read_where(['slug' => $slug])->row();
        $file_program       = $MP->read_where(['program_id' => $program->id])->row();
        $MFP->program_id    = $program->id;
        $MP->slug           = $program->slug;
        $MP->delete();
        $MFP->delete();
        unlink($file_program->file_loc);
       
		$this->session->set_flashdata('program_sukses', '<div class="alert alert-success fade-in"><a href="#" class="close" style="text-decoration: none;" data-dismiss="alert" aria-label="close">&times;</a><strong>Program berhasil di Hapus</strong></div>');
		redirect('program');
		
	}

    function judul_exist($judul){
            $this->Modeldb->setTabel('program');
            $data = $this->Modeldb->read_id('slug', strtolower(url_title($judul)))->row();
            if ($data != null) {
                return false;
            } else{
                return true;
            }
        }

    function ubah_judul_exist($judul){
            $this->Modeldb->setTabel('program');
            $data = $this->Modeldb->read_id('slug', strtolower(url_title($judul)))->row();
            if (strtolower(url_title($judul)) == $this->session->userdata('slug')) {
                return true;
            } else if ($data != null) {
                return false;
            } else {
                return true;
            }
        }
}