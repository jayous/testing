<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Artikel extends CI_Controller {

	function __construct() {
		parent:: __construct();
		$this->load->model('Modeldb');
		$this->load->library('form_validation');
        $this->load->helper(array('language'));
        $this->lang->load('auth');

        if (! $this->ion_auth->logged_in()) {
            redirect('auth');
        }
	}

	public function index() {
        $data['artikel'] = $this->Modeldb->get_artikel();
		$this->template->load_backend('template', 'backend/contents/artikel-home', $data);
	}

	public function artikel_baru() {
		// Jika tombol Terbit diklik
		if (ISSET($_POST['terbit'])) {
            $rules = array(
                [
                    'field' => 'judul',
                    'label' => 'Judul',
                    'rules' => 'trim|required|min_length[10]|callback_judul_exist',
                    'errors' => array(
                        'required' => '<span class="text-danger">Judul Tidak Boleh Kosong</span>',
                        'min_length' => '<span class="text-danger">Gunakan SPOK yang Baik Agar Mudah di Mengerti</span>',
                        'judul_exist' => '<span class="text-danger">Mohon Gunakan Judul yang Lain</span>'
                    )
                ],
                [
                    'field' => 'konten',
                    'label' => 'Isi Konten',
                    'rules' => 'trim|required',
                    'errors' => array(
                        'required' => '<span class="text-danger">Konten Tidak Boleh Kosong</span>'
                    )
                ]
            );
            $this->form_validation->set_rules($rules);
            if($this->form_validation->run() == false) {
                $this->Modeldb->setTabel('kategori_artikel');
                $data['kategori'] = $this->Modeldb->read();
                $this->template->load_backend('template', 'backend/contents/artikel-kirim', $data);
            } else {
                $slug = strtolower(url_title($this->input->post('judul')));
                $this->Modeldb->setTabel('artikel');
                $data_id_terakhir = $this->Modeldb->read_last();
                $tambah_artikel = array(
                    'id' => $data_id_terakhir->row(0)->id + 1,
                    'konten' => $this->input->post('konten'),
                    'judul' => $this->input->post('judul'),
                    'slug' => $slug,
                    'kategori_artikel_id' => $this->input->post('kategori'),
                    'tanggal_post' => time(),
                    'users_id' => $this->session->userdata('user_id'),
                );
            // Jika File Upload kosong
            if (empty($_FILES['userfile']['name'])) {
                // Simpan Artikel ke db
                $this->session->set_flashdata('artikel_berhasil', '<div class="alert alert-success fade-in"><a href="#" class="close" style="text-decoration: none;" data-dismiss="alert" aria-label="close">&times;</a><strong>Artikel berhasil di Terbitkan</strong></div>');
                $this->Modeldb->setTabel('artikel');
                $this->Modeldb->create($tambah_artikel);
                $array_file = array(
                    'file_name' => 0,
                    'file_size' => 0,
                    'file_type' => 0,
                    'file_loc' => 0,
                    'tgl_upload' => date('Y-m-d'),
                    'artikel_id' => $data_id_terakhir->row(0)->id + 1,
                );
                $this->Modeldb->setTabel('file');
                $this->Modeldb->create($array_file);
                redirect('admin/artikel');
            // Jika file upload tidak kosong
            } else {
                $first_name = $_FILES["userfile"]['name'];
                $new_name = time().$_FILES["userfile"]['name'];
                $config = [
                    'upload_path' => './assets/images/blog/temp',
                    'allowed_types' => 'jpg|png|jpeg',
                    'file_name' => $new_name,
                    'max_size' => 2048,
                    'max_width' => 5500,
                    'max_height' => 5500,
                    'overwrite' => FALSE,
                    'file_ext_tolower' => TRUE,
                    'max_filename' => 100
                ];
                $this->upload->initialize($config);
                    if (! $this->upload->do_upload()) {
                        $this->session->set_flashdata('upload', $this->upload->display_errors());
                        var_dump($this->upload->display_errors());
                        redirect('admin/artikel/artikel_baru');
                    } else {
                        $path = "./assets/images/blog/".date('Y-m-d');
                            // Jika folder belum dibuat
                            if(!is_dir($path))
                            {
                                mkdir($path,0755,TRUE); // Buat folder
                            }
                            $file = $this->upload->data();
                            $config1 = [
                                'image_library' => 'gd2',
                                'source_image' => './assets/images/blog/temp/'.$file['file_name'],
                                'create_thumb' => FALSE,
                                'maintain_ratio' => TRUE,
                                'width' => 800,
                                'quality' => '80%',
                                'maintain_ratio' => true,
                                'new_image' => $path.'/'.$file['file_name']
                            ];
                            $this->image_lib->initialize($config1);
                            $this->image_lib->resize();

                            if ( ! $this->image_lib->resize()){
                                //jika ubah ukuran error
                                $this->session->set_flashdata('upload', $this->image_lib->display_errors());
                                redirect('admin/artikel/artikel_baru');
                            } else {
                                // Hapus gambar di folder temp
                                unlink('./assets/images/blog/temp/'.$file['file_name']);

                                // Simpan artikel ke db
                                $this->Modeldb->setTabel('artikel');
                                $this->Modeldb->create($tambah_artikel);

                                // Simpan file ke db
                                $array_file = array(
                                    'file_name' => $file['file_name'],
                                    'file_size' => $file['file_size'],
                                    'file_type' => $file['file_type'],
                                    'file_loc' => './assets/images/blog/'.date('Y-m-d').'/'.$file['file_name'],
                                    'tgl_upload' => date('Y-m-d'),
                                    'artikel_id' => $data_id_terakhir->row(0)->id + 1
                                );
                                $this->Modeldb->setTabel('file');
                                $this->Modeldb->create($array_file);

                                $this->session->set_flashdata('artikel_berhasil', '<div class="alert alert-success fade-in"><a href="#" class="close" style="text-decoration: none;" data-dismiss="alert" aria-label="close">&times;</a><strong>Artikel berhasil di Terbitkan</strong></div>');
                                redirect('admin/artikel');
                            }
                    }
            }
            }
        } else {
            $this->Modeldb->setTabel('kategori_artikel');
            $data['kategori'] = $this->Modeldb->read();
            $this->template->load_backend('template', 'backend/contents/artikel-kirim', $data);
		}
	}

	public function sunting($id = null, $slug = null) {
		if (isset($_POST['perbarui'])) {
            $rules = array(
                [
                    'field' => 'judul',
                    'label' => 'Judul',
                    'rules' => 'trim|required|min_length[10]|callback_ubah_judul_exist',
                    'errors' => array(
                        'required' => '<span class="text-danger">Judul Tidak Boleh Kosong</span>',
                        'min_length' => '<span class="text-danger">Gunakan SPOK yang Baik Agar Mudah di Mengerti</span>',
                        'ubah_judul_exist' => '<span class="text-danger">Mohon Gunakan Judul yang Lain</span>'
                    )
                ],
                [
                    'field' => 'konten',
                    'label' => 'Isi Konten',
                    'rules' => 'trim|required',
                    'errors' => array(
                        'required' => '<span class="text-danger">Konten Tidak Boleh Kosong</span>'
                    )
                ]
            );
            $this->form_validation->set_rules($rules);
            if($this->form_validation->run() == false) {

                $slug = strtolower(url_title($this->input->post('judulhid')));
                $id_lama = $this->input->post('id');

                $this->Modeldb->setTabel('artikel');
                $data['artikel'] = $this->Modeldb->read_id('slug', $slug);
                $slugg = $data['artikel']->row();
                $this->session->set_userdata('slug', $slugg->slug);

                $this->Modeldb->setTabel('kategori_artikel');
                $data['kategori'] = $this->Modeldb->read();

                $this->Modeldb->setTabel('file');
                $row = $this->Modeldb->read_id('artikel_id', $id_lama)->row();

                if (!empty($row->file_loc)) {
                    $data['file_lokasi'] = $row->file_loc;
                }

                $this->template->load_backend('template', 'backend/contents/artikel-sunting', $data);
            } else {
                $slug = strtolower(url_title($this->input->post('judulhid')));
                $id_lama = $this->input->post('id');

                $sunting_artikel = array(
                    'konten' => $this->input->post('konten'),
                    'judul' => $this->input->post('judul'),
                    'slug' => $slug,
                    'kategori_artikel_id' => $this->input->post('kategori'),
                    'users_id' => $this->session->userdata('user_id'),
                );
            // Jika File Upload kosong
            if (empty($_FILES['userfile']['name'])) {
                // Update Artikel ke db
                $this->session->set_flashdata('artikel_berhasil', '<div class="alert alert-success fade-in"><a href="#" class="close" style="text-decoration: none;" data-dismiss="alert" aria-label="close">&times;</a><strong>Artikel berhasil di Perbarui</strong></div>');
                $this->Modeldb->setTabel('artikel');
                $this->Modeldb->update($sunting_artikel, 'id', $id_lama);
                redirect('admin/artikel');
            // Jika file upload tidak kosong
            } else {
                $first_name = $_FILES["userfile"]['name'];
                $new_name = time().$_FILES["userfile"]['name'];
                $config = [
                    'upload_path' => './assets/images/blog/temp',
                    'allowed_types' => 'jpg|png|jpeg',
                    'file_name' => $new_name,
                    'max_size' => 2048,
                    'max_width' => 5500,
                    'max_height' => 5500,
                    'overwrite' => FALSE,
                    'file_ext_tolower' => TRUE,
                    'max_filename' => 100
                ];
                $this->upload->initialize($config);
                    if (! $this->upload->do_upload()) {
                        $this->session->set_flashdata('upload', $this->upload->display_errors());
                        var_dump($this->upload->display_errors());
                        redirect('admin/artikel/sunting/'.$id_lama.'/'.$slug);
                    } else {
                        $path = "./assets/images/blog/".date('Y-m-d');
                            // Jika folder belum dibuat
                            if(!is_dir($path))
                            {
                                mkdir($path,0755,TRUE); // Buat folder
                            }
                            $file = $this->upload->data();
                            $config1 = [
                                'image_library' => 'gd2',
                                'source_image' => './assets/images/blog/temp/'.$file['file_name'],
                                'create_thumb' => FALSE,
                                'maintain_ratio' => TRUE,
                                'width' => 800,
                                'quality' => '80%',
                                'maintain_ratio' => true,
                                'new_image' => $path.'/'.$file['file_name']
                            ];
                            $this->image_lib->initialize($config1);
                            $this->image_lib->resize();

                            if ( ! $this->image_lib->resize()){
                                //jika ubah ukuran error
                                $this->session->set_flashdata('upload', $this->image_lib->display_errors());
                                redirect('admin/artikel/sunting/'.$id_lama.'/'.$slug);
                            } else {
                                // Hapus gambar di folder temp
                                unlink('./assets/images/blog/temp/'.$file['file_name']);

                                // Hapus gambar sebelumnya
                                $this->Modeldb->setTabel('file');
                                $row = $this->Modeldb->read_id('artikel_id', $id_lama)->row();
                                if (!empty($row->file_loc)) {
                                    unlink($row->file_loc);
                                }

                                // update artikel ke db
                                $this->Modeldb->setTabel('artikel');
                                $this->Modeldb->update($sunting_artikel, 'id', $id_lama);

                                // update file ke db
                                $array_file = array(
                                    'file_name' => $file['file_name'],
                                    'file_size' => $file['file_size'],
                                    'file_type' => $file['file_type'],
                                    'file_loc' => './assets/images/blog/'.date('Y-m-d').'/'.$file['file_name'],
                                    'tgl_upload' => date('Y-m-d'),
                                    'artikel_id' => $id_lama
                                );

                                $this->Modeldb->setTabel('file');
                                $cari_foto = $this->Modeldb->read_id('artikel_id', $id_lama);
                                if ($cari_foto->row() > 0) {
                                    $this->Modeldb->update($array_file, 'artikel_id', $id_lama);
                                } else {
                                    $this->Modeldb->create($array_file);
                                }
                                $this->session->set_flashdata('artikel_berhasil', '<div class="alert alert-success fade-in"><a href="#" class="close" style="text-decoration: none;" data-dismiss="alert" aria-label="close">&times;</a><strong>Artikel berhasil di Perbarui</strong></div>');
                                redirect('admin/artikel');
                            }
                    }
            }
            }
        } else {    
            $this->session->set_userdata('slug', $slug);

            $this->Modeldb->setTabel('artikel');
            $data['artikel'] = $this->Modeldb->read_id('slug', $slug);

            $this->Modeldb->setTabel('kategori_artikel');
            $data['kategori'] = $this->Modeldb->read();

            $this->Modeldb->setTabel('file');
            $row = $this->Modeldb->read_id('artikel_id', $id)->row();

            if (!empty($row->file_loc)) {
                $data['file_lokasi'] = $row->file_loc;
            }

			$this->template->load_backend('template', 'backend/contents/artikel-sunting', $data);
		}
	}

    public function detail($id = null, $slug = null) {
        $data['artikel'] = $this->Modeldb->get_kategori_artikel($id)->row();
        $this->template->load_backend('template', 'backend/contents/artikel-detil', $data);
    }

	public function hapus($id = null, $slug = null) {
		$this->Modeldb->setTabel('file');
		$row = $this->Modeldb->read_id('artikel_id', $id)->row();
		unlink($row->file_loc);

        $this->session->set_flashdata('artikel_berhasil', '<div class="alert alert-success fade-in"><a href="#" class="close" style="text-decoration: none;" data-dismiss="alert" aria-label="close">&times;</a><strong>Artikel berhasil di Hapus</strong></div>');
        $this->Modeldb->setTabel('artikel');
        $this->Modeldb->delete('slug', $slug);
        
        // delete comment if comments'artkel ar deleted
        $this->Modeldb->setTabel('comments');
        $this->Modeldb->delete('artikel_id', $id);
        redirect('admin/artikel');
	}

    function judul_exist($judul){
            $this->Modeldb->setTabel('artikel');
            $data = $this->Modeldb->read_id('slug', strtolower(url_title($judul)))->row();
            if ($data != null) {
                return false;
            } else{
                return true;
            }
        }

    function ubah_judul_exist($judul){
            $this->Modeldb->setTabel('artikel');
            $data = $this->Modeldb->read_id('slug', strtolower(url_title($judul)))->row();
            if (strtolower(url_title($judul)) == $this->session->userdata('slug')) {
                return true;
            } else if ($data != null) {
                return false;
            } else {
                return true;
            }
        }
}