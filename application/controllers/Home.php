<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('Modeldb');
		$this->load->model('ModelProgram');
        $this->load->model('ModelFileProgram');
		$this->load->library('pagination');
		$this->lang->load('auth');
	}
	public function index()
	{
		$data = array(
			'blog_title' => 'HIKARI - Beranda',
			'blog_heading' => 'My Blog Heading',
			'blog_entries' => $this->Modeldb->get_latest_artikel(3)->result_array(),
		);
		$this->parser->parse('frontend/header/header', $data);
		$this->parser->parse('frontend/content/beranda', $data);
		$this->parser->parse('frontend/footer/footer', $data);	
	}
	public function course(){
		$data = array(
			'blog_title' => 'HIKARI - Beranda',
			'blog_heading' => 'My Blog Heading'
		);
		$this->parser->parse('frontend/header/header', $data);
		$this->parser->parse('frontend/content/course', $data);
		$this->parser->parse('frontend/footer/footer', $data);	
	}

	public function profile(){
		$data = array(
			'blog_title' => 'HIKARI - Beranda',
			'blog_heading' => 'My Blog Heading'
		);
		$this->Modeldb->setTabel('profil');
		$da['profile'] = $this->Modeldb->read();

		$this->parser->parse('frontend/header/header', $data);
		$this->load->view('frontend/content/profile', $da);
		$this->parser->parse('frontend/footer/footer', $data);	
	}

	public function education()
	{
		
		 $total_row = $this->ModelProgram->read_where(['p.kategori_program_id' => 1])->num_rows();
 
		 //pagging setting
		 $config = array();
		 $config['base_url'] = base_url("home/education/index");
		 $config['total_rows'] = $total_row;
		 $config['per_page'] = 6;
 
		 //pagging design
		 $config['full_tag_open'] = "<div class='blog-pagination'><ul class='pagination'>";
		 $config['full_tag_close'] = '</ul></div>';
		 $config['num_tag_open'] = '<li>';
		 $config['num_tag_close'] = '</li>';
		 $config['cur_tag_open'] = '<li class="active"><a href="">';
		 $config['cur_tag_close'] = '</a></li>';
		 $config['prev_tag_open'] = '<li>';
		 $config['prev_tag_close'] = '</li>';
		 $config['first_tag_open'] = '<li>';
		 $config['first_tag_close'] = '</li>';
		 $config['last_tag_open'] = '<li>';
		 $config['last_tag_close'] = '</li>';
 
 
 
		 $config['prev_link'] = 'Previous Page';
		 $config['prev_tag_open'] = '<li>';
		 $config['prev_tag_close'] = '</li>';
 
 
		 $config['next_link'] = 'Next Page';
		 $config['next_tag_open'] = '<li>';
		 $config['next_tag_close'] = '</li>';
		 // end
 
		 $from = $this->uri->segment(4);
		 $this->pagination->initialize($config);
 
		 $data = array(
			 'blog_title' => 'Hikari - Berita',
			 'blog_heading' => 'Berita',
		 );
		 
		 $data['blog']           = $this->ModelProgram->get_page_data($config['per_page'], $from, ['p.kategori_program_id' => 1])->result_array();


		 $this->Modeldb->setTabel('kategori_program');
		 $data['kategori']       = $this->Modeldb->read()->result_array();
 
		 $this->parser->parse('frontend/header/header', $data);
		 $this->load->view('frontend/content/pendidikan', $data);
		 $this->parser->parse('frontend/footer/footer', $data);
	}

	public function internship()
	{
		$total_row = $this->ModelProgram->read_where(['p.kategori_program_id' => 2])->num_rows();
 
		//pagging setting
		$config = array();
		$config['base_url'] = base_url("home/internship/index");
		$config['total_rows'] = $total_row;
		$config['per_page'] = 6;

		//pagging design
		$config['full_tag_open'] = "<div class='blog-pagination'><ul class='pagination'>";
		$config['full_tag_close'] = '</ul></div>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="active"><a href="">';
		$config['cur_tag_close'] = '</a></li>';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';



		$config['prev_link'] = 'Previous Page';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';


		$config['next_link'] = 'Next Page';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		// end

		$from = $this->uri->segment(4);
		$this->pagination->initialize($config);

		$data = array(
			'blog_title' => 'Hikari - Berita',
			'blog_heading' => 'Berita',
		);
		
		$data['blog']           = $this->ModelProgram->get_page_data($config['per_page'], $from, ['p.kategori_program_id' => 2])->result_array();


		$this->Modeldb->setTabel('kategori_program');
		$data['kategori']       = $this->Modeldb->read()->result_array();

		$this->parser->parse('frontend/header/header', $data);
		$this->load->view('frontend/content/magang', $data);
		$this->parser->parse('frontend/footer/footer', $data);
	}

	public function general_trading()
	{
		$total_row = $this->ModelProgram->read_where(['p.kategori_program_id' => 3])->num_rows();
 
		//pagging setting
		$config = array();
		$config['base_url'] = base_url("home/general_trading/index");
		$config['total_rows'] = $total_row;
		$config['per_page'] = 6;

		//pagging design
		$config['full_tag_open'] = "<div class='blog-pagination'><ul class='pagination'>";
		$config['full_tag_close'] = '</ul></div>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="active"><a href="">';
		$config['cur_tag_close'] = '</a></li>';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';



		$config['prev_link'] = 'Previous Page';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';


		$config['next_link'] = 'Next Page';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		// end

		$from = $this->uri->segment(4);
		$this->pagination->initialize($config);

		$data = array(
			'blog_title' => 'Hikari - Berita',
			'blog_heading' => 'Berita',
		);
		
		$data['blog']           = $this->ModelProgram->get_page_data($config['per_page'], $from, ['p.kategori_program_id' => 3])->result_array();


		$this->Modeldb->setTabel('kategori_program');
		$data['kategori']       = $this->Modeldb->read()->result_array();

		$this->parser->parse('frontend/header/header', $data);
		$this->load->view('frontend/content/general-trading', $data);
		$this->parser->parse('frontend/footer/footer', $data);
	}

	public function program($slug = NULL)
	{
		if ($slug == "pendidikan-dan-pelatihan")
		{
			redirect('home/education');
		}
		else if ($slug == "program-magang")
		{
			redirect('home/internship');
		}
		else if ($slug == "general-trading")
		{
			redirect('home/general_trading');
		}
		else
		{
			redirect('home');
		}
	}
}
