<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Galeri extends CI_Controller {

	function __construct() {
		parent:: __construct();
        $this->load->model('Modeldb');
        $this->load->helper(array('language'));
        $this->lang->load('auth');
	}

	public function index() {
        $this->Modeldb->setTabel('file_galeri');
        $data['file_galeri'] = $this->Modeldb->read();
		$this->template->load_backend('template', 'backend/contents/galeri-home', $data);
	}

	public function gambar_baru() {
		if (isset($_POST['upload'])) {
			$first_name = $_FILES["userfile"]['name'];
                $new_name = time().$_FILES["userfile"]['name'];
                $config = [
                    'upload_path' => './assets/images/galeri/temp',
                    'allowed_types' => 'jpg|png|jpeg',
                    'file_name' => $new_name,
                    'max_size' => 2048,
                    'max_width' => 5500,
                    'max_height' => 5500,
                    'overwrite' => FALSE,
                    'file_ext_tolower' => TRUE,
                    'max_filename' => 100
                ];
                $this->upload->initialize($config);
                    if (! $this->upload->do_upload()) {
                        $this->session->set_flashdata('error', $this->upload->display_errors());
                        var_dump($this->upload->display_errors());
                        redirect('galeri/gambar_baru');
                    } else {
                        $path = "./assets/images/galeri/".date('Y-m-d');
                            // Jika folder belum dibuat
                            if(!is_dir($path))
                            {
                                mkdir($path,0755,TRUE); // Buat folder
                            }
                            $file = $this->upload->data();
                            $config1 = [
                                'image_library' => 'gd2',
                                'source_image' => './assets/images/galeri/temp/'.$file['file_name'],
                                'create_thumb' => FALSE,
                                'maintain_ratio' => TRUE,
                                'width' => 800,
                                'quality' => '80%',
                                'maintain_ratio' => true,
                                'new_image' => $path.'/'.$file['file_name']
                            ];
                            $this->image_lib->initialize($config1);
                            $this->image_lib->resize();

                            if ( ! $this->image_lib->resize()){
                                //jika ubah ukuran error
                                $this->session->set_flashdata('error', $this->image_lib->display_errors());
                                redirect('galeri/gambar_baru');
                            } else {
                                // Hapus gambar di folder temp
                                unlink('./assets/images/galeri/temp/'.$file['file_name']);

                                // Simpan file ke db
                                $array_file = array(
                                    'file_name' => $file['file_name'],
                                    'file_size' => $file['file_size'],
                                    'file_type' => $file['file_type'],
                                    'file_loc' => './assets/images/galeri/'.date('Y-m-d').'/'.$file['file_name'],
                                    'tgl_upload' => date('Y-m-d'),
                                    'file_ket' => $this->input->post('ket'),
                                );

                                $this->Modeldb->setTabel('file_galeri');
                                $this->Modeldb->create($array_file);
                                    $this->session->set_flashdata('galeri_berhasil', '<div class="alert alert-success fade-in"><a href="#" class="close" style="text-decoration: none;" data-dismiss="alert" aria-label="close">&times;</a><strong>Gambar Berhasil ditambahkan ke Galeri</strong></div>');
                                    redirect('galeri');
                            }
                    }
		} else {
			$this->template->load_backend('template', 'backend/contents/galeri-add');
		}
	}

    public function hapus_gambar($id) {
        $this->Modeldb->setTabel('file_galeri');
        $file_loc = $this->Modeldb->read_id('id', $id)->row();

        if ($this->Modeldb->delete('id', $id) && unlink($file_loc->file_loc)) {
            $this->session->set_flashdata('galeri_berhasil', '<div class="alert alert-success fade-in"><a href="#" class="close" style="text-decoration: none;" data-dismiss="alert" aria-label="close">&times;</a><strong>Gambar Berhasil di Hapus</strong></div>');
        }

        redirect('galeri');
    }
}