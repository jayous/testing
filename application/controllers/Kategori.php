<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kategori extends CI_Controller {

	function __construct() {
		parent:: __construct();
		$this->load->model('Modeldb');
		$this->load->library('form_validation');
        $this->load->helper(array('language'));
        $this->lang->load('auth');
	}

	public function index() {
		$this->Modeldb->setTabel('kategori_artikel');
		$data['kategori'] = $this->Modeldb->read();
		$this->template->load_backend('template', 'backend/contents/kategori-home', $data);
	}

	public function kategori_baru() {
		if (isset($_POST['tambah'])) {
			$rules = array(
                [
                    'field' => 'judul',
                    'label' => 'Judul',
                    'rules' => 'trim|required|callback_judul_exist',
                    'errors' => array(
                        'required' => '<span class="text-danger">Nama Kategori Tidak Boleh Kosong</span>',
                        'judul_exist' => '<span class="text-danger">Mohon Gunakan Nama yang Lain</span>'
                    )
                ]
            );
            $this->form_validation->set_rules($rules);
            if($this->form_validation->run() == false) {
                $this->template->load_backend('template', 'backend/contents/kategori-add');
            } else {
                $tambah_kategori = array(
                    'nama' => $this->input->post('judul'),
                    'slug' => strtolower(url_title($this->input->post('judul')))
                );
                $this->session->set_flashdata('artikel_berhasil', '<div class="alert alert-success fade-in"><a href="#" class="close" style="text-decoration: none;" data-dismiss="alert" aria-label="close">&times;</a><strong>Kategori Artikel berhasil ditambahkan</strong></div>');
                $this->Modeldb->setTabel('kategori_artikel');
                $this->Modeldb->create($tambah_kategori);
                redirect('kategori');
            }
		} else {
			$this->template->load_backend('template', 'backend/contents/kategori-add');
		}
	}

	public function sunting($slug = null) {
		if (isset($_POST['perbarui'])) {
            $rules = array(
                [
                    'field' => 'judul',
                    'label' => 'Judul',
                    'rules' => 'trim|required|callback_ubah_judul_exist',
                    'errors' => array(
                        'required' => '<span class="text-danger">Nama Tidak Boleh Kosong</span>',
                        'ubah_judul_exist' => '<span class="text-danger">Mohon Gunakan Nama yang Lain</span>'
                    )
                ]
            );
            $this->form_validation->set_rules($rules);
            if($this->form_validation->run() == false) {

                $nama = $this->input->post('namahid');
                $id_lama = $this->input->post('id');

                $this->Modeldb->setTabel('kategori_artikel');
                $data['kategori'] = $this->Modeldb->read_id('nama', $nama);
                $slugg = $data['kategori']->row();
                $this->session->set_userdata('slug', $slugg->nama);

                $this->template->load_backend('template', 'backend/contents/kategori-sunting', $data);
            } else {
            	$id_lama = $this->input->post('id');
                $sunting_artikel = array(
                    'nama' => $this->input->post('judul'),
                    'slug' => strtolower(url_title($this->input->post('judul')))
                );
                $this->session->set_flashdata('artikel_berhasil', '<div class="alert alert-success fade-in"><a href="#" class="close" style="text-decoration: none;" data-dismiss="alert" aria-label="close">&times;</a><strong>Kategori Artikel berhasil di Perbarui</strong></div>');
                $this->Modeldb->setTabel('kategori_artikel');
                $this->Modeldb->update($sunting_artikel, 'id', $id_lama);
                redirect('kategori');
            }
        } else {
			$this->Modeldb->setTabel('kategori_artikel');
            $data['kategori'] = $this->Modeldb->read_id('slug', $slug);
            $this->session->set_userdata('slug_artikel', $slug);
			$this->template->load_backend('template', 'backend/contents/kategori-sunting', $data);
        }
    }

	public function hapus($id = null) {
        $this->session->set_flashdata('artikel_berhasil', '<div class="alert alert-success fade-in"><a href="#" class="close" style="text-decoration: none;" data-dismiss="alert" aria-label="close">&times;</a><strong>Kategori Artikel berhasil di Hapus</strong></div>');
        $this->Modeldb->setTabel('kategori_artikel');
        $this->Modeldb->delete('id', $id);
        redirect('kategori');
	}

	function judul_exist($judul){
            $this->Modeldb->setTabel('kategori_artikel');
            $data = $this->Modeldb->read_id('nama', $judul)->row();
            if ($data != null) {
                return false;
            } else{
                return true;
            }
        }

    function ubah_judul_exist($judul){
        $this->Modeldb->setTabel('kategori_artikel');
        $data = $this->Modeldb->read_id('slug', strtolower(url_title($judul)))->row();
        if (strtolower(url_title($judul)) == $this->session->userdata('slug_artikel')) {
            return true;
        } else if ($data != null) {
            return false;
        } else {
            return true;
        }
    }
}