<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Comments extends CI_Controller
{
    
    function __construct()
    {
        parent::__construct();
        $this->load->model('ModelComments');
    }

    public function get_comments($id = NULL)
    {
        $comments = $this->ModelComments;
        $data = $comments->read($id)->result();
        echo json_encode($data);
    }

    public function add_comments()
    {
        //echo $this->session->userdata('s');
        $comments = $this->ModelComments;
        $comments->save();
    }
}