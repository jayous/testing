<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Persons extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model('ModelPersons');
        $this->load->model('ModelFilePersons');
		$this->load->library('form_validation');
        $this->load->helper(array('language'));
        $this->lang->load('auth');

        if (! $this->ion_auth->logged_in()) {
            redirect('auth');
        }
    }

    public function index()
    {
        $MP                 = $this->ModelPersons;
        $data['persons']    = $MP->get();
        $this->template->load_backend('template', 'backend/contents/persons-home', $data);
    }

    public function tampil_baru()
    {
        $MP         = $this->ModelPersons;

        $this->data['full_name']        = $MP->_full_name();
        $this->data['place_of_birth']   = $MP->_place_of_birth();
        $this->data['date_of_birth']    = $MP->_date_of_birth();
        $this->data['gender']           = $MP->_gender();
        $this->data['school']           = $MP->_school();
        $this->data['home_address']     = $MP->_home_address();
        $this->data['mobile_number']    = $MP->_mobile_number();
        $this->data['class']            = $MP->_class();

        $this->template->load_backend('template', 'backend/contents/persons-add', $this->data);
    }

    public function tampil_sunting($per = NULL)
    {
        $MP         = $this->ModelPersons;

        $this->data['id']               = $MP->_id($per->row(0)->id);
        $this->data['full_name']        = $MP->_full_name($per->row(0)->full_name);
        $this->data['place_of_birth']   = $MP->_place_of_birth($per->row(0)->place_of_birth);
        $this->data['date_of_birth']    = $MP->_date_of_birth($per->row(0)->date_of_birth);
        $this->data['gender']           = $MP->_gender_sunting($per->row(0)->gender);
        $this->data['school']           = $MP->_school($per->row(0)->school);
        $this->data['home_address']     = $MP->_home_address($per->row(0)->home_address);
        $this->data['mobile_number']    = $MP->_mobile_number($per->row(0)->mobile_number);
        $this->data['class']            = $MP->_class_sunting($per->row(0)->class);
        $this->data['image']            = $per->row(0)->file_loc;
        $this->data['aidi']             = $per->row(0)->id;

        $this->template->load_backend('template', 'backend/contents/persons-sunting', $this->data);
    }

    public function tampil($data = NULL)
    {
        $MP                        = $this->ModelPersons;
        $this->data['person']      = $MP->get_where(['p.id' => $data]);

        $this->template->load_backend('template', 'backend/contents/persons-detail', $this->data);

    }
    
    public function tambah()
    {
        $MP         = $this->ModelPersons;
        $MFP        = $this->ModelFilePersons;
        $validation = $this->form_validation;
        $upload     = $this->upload;
        $image      = $this->image_lib;

        if (isset($_POST['kirim']))
        {
            $rules = $MP->rules();
            $validation->set_rules($rules);

            if($validation->run() == false)
            {
               $this->tampil_baru();
            } 
            else 
            {
                if (empty($_FILES['userfile']['name']))
                {
                    $MP->add(); // save to Member
                    $MFP->data(NULL, NULL, NULL, NULL, $MP->id);
                    $MFP->add();
                    //set message and redirect
                    $this->session->set_flashdata('message', '<div class="alert alert-success fade-in"><a href="#" class="close" style="text-decoration: none;" data-dismiss="alert" aria-label="close">&times;</a><strong>Member berhasil di Tambahkan</strong></div>');
                    redirect('admin/persons');
                } 
                else 
                {
                    $MFP->file_name = time().$_FILES["userfile"]['name'];
                    $upload->initialize($MFP->upload_config());

                    if (! $upload->do_upload()) 
                    {
                        $this->session->set_flashdata('upload', $this->upload->display_errors());
                        $this->tampil_baru();
                    }
                    else
                    {
                        $path = "./assets/images/persons/".date('Y-m-d');
                        // Jika folder belum dibuat
                        if(!is_dir($path))
                        {
                            mkdir($path,0755,TRUE); // Buat folder
                        }

                        $file = $upload->data();
                        $MFP->file_name = $file['file_name'];
                        $image->initialize($MFP->resize_config());
                        $image->resize();

                        if (! $image->resize())
                        {
                            $this->session->set_flashdata('upload', $this->image_lib->display_errors());
                            $this->tampil_baru();
                        }
                        else
                        {
                            //delete real image
                            unlink('./assets/images/persons/temp/'.$file['file_name']);
                            
                            $MP->add(); // save to Member
                            $MFP->data( $file['file_name'], 
                                        $file['file_size'], 
                                        $file['file_type'], 
                                        './assets/images/persons/'.date('Y-m-d').'/'.$file['file_name'], 
                                        $MP->id);
                            $MFP->add();
                            //set message and redirect
                            $this->session->set_flashdata('message', '<div class="alert alert-success fade-in"><a href="#" class="close" style="text-decoration: none;" data-dismiss="alert" aria-label="close">&times;</a><strong>Member berhasil di Tambahkan</strong></div>');
                            redirect('admin/persons');
                        }
                    }
                } 
            }
        } 
        else 
        {
           $this->tampil_baru();
        }
    }

    public function sunting($id = NULL)
    {
        $MP         = $this->ModelPersons;
        $MFP        = $this->ModelFilePersons;
        $validation = $this->form_validation;
        $upload     = $this->upload;
        $image      = $this->image_lib;

        if (isset($_POST['kirim'])) 
        {
            $rules = $MP->rules();
            $validation->set_rules($rules);

            if($validation->run() == false)
            {
                $this->data['person']      = $MP->get_where(['p.id' => $id]);
                $this->tampil_sunting($this->data['person']);
            } 
            else 
            {
                if (empty($_FILES['userfile']['name']))
                {
                    //update data without image
                    $MP->update();
                    $this->session->set_flashdata('success', '<div class="alert alert-success fade-in"><a href="#" class="close" style="text-decoration: none;" data-dismiss="alert" aria-label="close">&times;</a><strong>Program berhasil di Perbarui</strong></div>');
                    redirect('admin/persons');   
                }
                else
                {
                    $MFP->file_name = time().$_FILES["userfile"]['name'];
                    $upload->initialize($MFP->upload_config());

                    if (! $upload->do_upload()) 
                    {
                        $this->session->set_flashdata('upload', $this->upload->display_errors());
                        $this->data['person']      = $MP->get_where(['p.id' => $id]);
                        $this->tampil_sunting($this->data['person']);
                    }
                    else
                    {
                        $path = "./assets/images/persons/".date('Y-m-d');
                        // Jika folder belum dibuat
                        if(!is_dir($path))
                        {
                            mkdir($path,0755,TRUE); // Buat folder
                        }

                        $file = $upload->data();
                        $MFP->file_name = $file['file_name'];
                        $image->initialize($MFP->resize_config());
                        $image->resize();

                        if (! $image->resize())
                        {
                            $this->session->set_flashdata('upload', $this->image_lib->display_errors());
                            $this->data['person']      = $MP->get_where(['p.id' => $id]);
                            $this->tampil_sunting($this->data['person']);
                        }
                        else
                        {
                            //delete real image
                            unlink('./assets/images/persons/temp/'.$file['file_name']);
                            unlink($this->input->post('lokasi_file_lama'));

                            $MP->update();
                            $MFP->data( $file['file_name'], 
                                        $file['file_size'], 
                                        $file['file_type'], 
                                        './assets/images/persons/'.date('Y-m-d').'/'.$file['file_name'], 
                                        $MP->id);
                            $MFP->update();

                            $this->session->set_flashdata('message', '<div class="alert alert-success fade-in"><a href="#" class="close" style="text-decoration: none;" data-dismiss="alert" aria-label="close">&times;</a><strong>Program berhasil di Perbarui</strong></div>');
                            redirect('admin/persons'); 
                        }
                    }
                }
            }
        } 
        else
        {
            $this->data['person']      = $MP->get_where(['p.id' => $id]);
            $this->tampil_sunting($this->data['person']);   
        }
    }

    public function hapus($id = NULL) {
        $MP         = $this->ModelPersons;
        $MFP        = $this->ModelFilePersons;

        $persons            = $MP->get_where(['p.id' => $id])->row();
        $file_persons       = $MFP->get_where(['person_id' => $persons->id])->row();
        $MFP->program_id    = $persons->id;
        $MP->id             = $persons->id;
        $MP->delete();
        $MFP->delete();
        unlink($file_persons->file_loc);
       
		$this->session->set_flashdata('message', '<div class="alert alert-success fade-in"><a href="#" class="close" style="text-decoration: none;" data-dismiss="alert" aria-label="close">&times;</a><strong>Program berhasil di Hapus</strong></div>');
		redirect('admin/persons');
		
	}

    function pilih($data = NULL)
    {   
        return $data == 'S' ? false : true;
    }
}