<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>HIKARI</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- css style -->

	<link rel="stylesheet" href="<?php echo base_url()?>assets/class/css/font-awesome.min.css">
	<link rel="stylesheet" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
	<link rel="stylesheet" href="<?php echo base_url()?>assets/css/util.css">
	<link rel="stylesheet" href="<?php echo base_url()?>assets/class/css/adminlte.css">
	<link rel="stylesheet" href="<?php echo base_url()?>assets/class/css/dataTables.bootstrap4.css">

	<!-- Font -->
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

	<script type="text/javascript" src="<?php echo base_url() ?>assets/ckeditor/ckeditor.js"></script>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script> 
   
	
</head>