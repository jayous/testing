    <footer class="main-footer">
		<div class="float-right d-none d-sm-block-down">
			Anything you want
		</div>
		<strong>Copyright &copy; 2018 <a href="https://adminlte.io">TI Universitas Muhammadiyah Sukabumi</a>.</strong> All rights reserved.
	</footer>				
	</div>

	<script src="<?php echo base_url()?>assets/class/js/jquery.min.js"></script>
	<script src="<?php echo base_url()?>assets/class/js/bootstrap.bundle.min.js"></script>
	<script src="<?php echo base_url()?>assets/class/js/adminlte.js"></script>
	<script src="<?php echo base_url()?>assets/class/js/jquery.dataTables.js"></script>
  <script src="<?php echo base_url()?>assets/class/js/dataTables.bootstrap4.js"></script>
	<script>
  $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "responsive":true,
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": true
		});
		$('#example3').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": true,
      "ordering": false,
      "info": false,
      "autoWidth": false
    });
  });
  </script>
  <script>
  $(function () {
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
    ClassicEditor
      .create(document.querySelector('#editor1'))
      .then(function (editor) {
        // The editor instance
      })
      .catch(function (error) {
        console.error(error)
      })

    // bootstrap WYSIHTML5 - text editor

    $('.textarea').wysihtml5({
      toolbar: { fa: true }
    })
  })
</script>
	
</body>
</html>