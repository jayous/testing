<section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Profile</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">User Profile</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
        
          <!-- /.col -->
          <div class="col-md-12">
            <div class="card">
              <div class="card-header p-2">
                <ul class="nav nav-pills">
                  <li class="nav-item"><a class="nav-link active" href="#settings" data-toggle="tab">Settings Profile</a></li>
                  <li class="nav-item"><?php if ($this->session->flashdata('ubah')) echo $this->session->flashdata('ubah');?></li>
                </ul>
              </div><!-- /.card-header -->
              <div class="card-body">
                <div class="tab-content">
                  <div class="active tab-pane" id="settings">
                    <?=form_open('e-learning/profile/ubah')?>
                        <div class="form-group">
                            <label style="font-size: 13px">Name</label>
                            <input type="text" name="nama" class="form-control" placeholder="Nama Lengkap" value="<?php if (validation_errors()) { echo set_value('nama');}else{echo $this->session->userdata('full_name');} ?>">
                            <?php if (validation_errors()){
                                echo form_error('nama');
                            }?>
                        </div>
                        <div class="form-group">
                            <label style="font-size: 13px">NIM / NIDN</label>
                            <input type="number" name="nim" id="nim" class="form-control" placeholder="NIM / NIDN" value="<?php if (validation_errors()) { echo set_value('nim');}else{echo $this->session->userdata('no');} ?>">
                            <?php if (validation_errors()){
                                echo form_error('nim');
                            }?>
                        </div>
                        <div class="form-group">
                            <label style="font-size: 13px">No Tlp</label>
                            <input type="text" name="notlp" id="notlp" class="form-control" placeholder="+62.." value="<?php if (validation_errors()) { echo set_value('notlp');}else{echo $this->session->userdata('phone');} ?>">
                            <?php if (validation_errors()){
                                echo form_error('notlp');
                            }?>
                        </div>
                        <div class="form-group">
                            <label style="font-size: 13px">Email</label>
                            <input type="email" name="email" id="email" class="form-control" placeholder="Email" value="<?php if (validation_errors()) { echo set_value('email');}else{echo $this->session->userdata('email');} ?>">
                            <?php if (validation_errors()){
                                 echo form_error('email');
                            }?>
                            </div>
                        <div class="form-group">
                            <label style="font-size: 13px">New Password (Optional)</label>
                            <input type="password" name="pass" id="pass" class="form-control" placeholder="Password min 8 karakter" value="<?php echo set_value('pass'); ?>">
                            <?php if (validation_errors()){
                                echo form_error('pass');
                            }?>
                        </div> 
                        <input type="password" name="cpass" id="cpass" class="form-control" placeholder="Confirm" value="<?php echo set_value('cpass'); ?>">
                        <?php if (validation_errors()){
                            echo form_error('cpass');
                        }?>                        
                        <hr>
                        <div class="form-group">
                            <label style="font-size: 13px">Password Sekarang</label>
                            <input type="password" name="crpass" id="crpass" class="form-control" placeholder="Password" value="<?php echo set_value('crpass'); ?>">
                            <?php if (validation_errors()){
                                echo form_error('crpass');
                            }?>
                        </div> 
                        <div class="form-group">
                            <input name="save" type="submit"  class="btn btn-block btn-outline-info btn-flat" value="Simpan Perubahan">                                                
                        </div>                        
                    </form>	
                  </div>
                  <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
              </div><!-- /.card-body -->
            </div>
            <!-- /.nav-tabs-custom -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->