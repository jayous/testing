<aside class="main-sidebar sidebar-dark-primary elevation-4">
					
	<a href="" class="brand-link">
	    <span class="brand-text font-weight-light">E-Complain</span>
    </a>
				
	<div class="sidebar">
		<div class="user-panel mt-3 pb-3 mb-3 d-flex">
			
			<div class="info">
				<a href="#" class="d-block"><?php echo strtoupper($this->session->userdata('first_name')." ".$this->session->userdata('last_name'))?></a>
			</div>
			
		</div>
				
		<nav class="mt-2">
			<ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
				<li class="nav-item">
					<a href="<?php echo site_url('e-complain')?>" class="nav-link">
						<i class="nav-icon fa fa-th-large"></i>
                        <p>
                        Beranda
                        </p>
					</a>
				</li>
					 
				<li class="nav-header">SETTING</li>
				
				<li class="nav-item">
					<a href="<?php echo site_url('admin/artikel')?>" class="nav-link">
						<i class="nav-icon fa fa-newspaper-o"></i>
                        <p>
                        Artikel
                        </p>
					</a>
				</li>
				<li class="nav-item">
					<a href="<?php echo site_url('profil')?>" class="nav-link">
						<i class="nav-icon fa fa-user"></i>
                        <p>
                        Profile
                        </p>
					</a>
				</li>
				<li class="nav-item">
					<a href="<?php echo site_url('kontak')?>" class="nav-link">
						<i class="nav-icon fa fa-phone"></i>
                        <p>
                        Contact
                        </p>
					</a>
				</li>
				<li class="nav-item">
					<a href="<?php echo site_url('galeri')?>" class="nav-link">
						<i class="nav-icon fa fa-camera"></i>
                        <p>
                        Galeri
                        </p>
					</a>
				</li>
				<li class="nav-item">
					<a href="<?php echo site_url('kursus')?>" class="nav-link">
						<i class="nav-icon fa fa-book"></i>
                        <p>
                        Kursus
                        </p>
					</a>
				</li>
				<li class="nav-item">
					<a href="<?php echo site_url('program')?>" class="nav-link">
						<i class="nav-icon fa fa-book"></i>
                        <p>
                        Program
                        </p>
					</a>
				</li>
				<li class="nav-item">
					<a href="<?php echo site_url('admin/persons')?>" class="nav-link">
						<i class="nav-icon fa fa-book"></i>
                        <p>
                        Member
                        </p>
					</a>
				</li>
				<li class="nav-header">DATA MASTER</li>
				<li class="nav-item">
					<a href="<?php echo site_url('kategori')?>" class="nav-link">
						<i class="nav-icon fa fa-newspaper-o"></i>
                        <p>
                        Kategori Artikel
                        </p>
					</a>
				</li>
				<li class="nav-header">PENGATURAN</li>
				<li class="nav-item">
					<a href="<?php echo site_url('admin/pengguna')?>" class="nav-link">
						<i class="nav-icon fa fa-users"></i>
                        <p>
                        Pengguna
                        </p>
					</a>
				</li>
				<li class="nav-item">
					<a href="<?php echo site_url('admin/pengguna/buat_pengguna')?>" class="nav-link">
						<i class="nav-icon fa fa-plus"></i>
                        <p>
                         <?php echo lang('index_create_user_link')?>
                        </p>
					</a>
				</li>
				<li id="design" class="nav-header">HELP</li>
				<!-- <li class="nav-item">
					<a href="<?php echo base_url()?>e-learning/class/info" class="nav-link">
						<i class="nav-icon fa fa-info"></i>
                        <p>
                        	Info
                        </p>
					</a>
				</li> -->
				<br>
				<li class="nav-item menu-open">
					<a href="<?php echo base_url()?>cms/keluar" class="nav-link">
						<i class="nav-icon fa fa-times-circle"></i>
                        <p>
                        	Keluar
                        </p>
					</a>
				</li>
				
			</ul>
		</nav>
	</div>
</aside>