
<!--  -->
<body class='hold-transition sidebar-mini'>
	<div class="wrapper">
	 <!-- Navbar -->
		<nav style="background-color: #3b5998 !important;" class="main-header navbar navbar-expand bg-white navbar-light border-bottom">
		<!-- Left navbar links -->
		    <ul class="navbar-nav">
				<li class='nav-item'>
								<a style=' color: white !important;' class='nav-link' data-widget='pushmenu' href='#'><i class='fa fa-bars'></i></a>
				</li>
				
				<li class="nav-item d-none d-sm-inline-block">
					<a style=" color: white !important;" href="<?=base_url()?>" class="nav-link">HOME</a>
				</li>
				
			</ul>
				
					<!-- Right navbar links -->
		    <ul class="navbar-nav ml-auto">
				<li class="nav-item dropdown">
					<a style=" color: white !important;" class="nav-link" data-toggle="dropdown" href="#">
					<b><?php echo strtoupper($this->session->userdata('first_name'))?> </b><i class="fa fa-user"></i>
					</a>
				    <div class="dropdown-menu dropdown-menu-sm dropdown-menu-right">
						<a href="<?php echo base_url()?>auth/logout" class="dropdown-item">
						    <i class="nav-icon fa fa-times mr-1"></i> logout
						</a>
					</div>
				</li>
				<li>
					
				</li>
			</ul>
		</nav>
		