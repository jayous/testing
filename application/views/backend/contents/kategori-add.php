<div class="content-header">
	<div class="container-fluid" >
		<div class="row mb-2">
			<div class="col-sm-6">
				<h1 class="m-0 text-dark"><?php echo lang('cat_article_home_heading') ?></h1>
			</div>
			<div class="col-sm-6">
				<ol class="breadcrumb float-sm-right">
					<li class="breadcrumb-item"><a href="#"><?php echo lang('cat_article_add_identifier_1') ?></a></li>
					<li class="breadcrumb-item active"><?php echo lang('cat_article_add_identifier_2') ?></li>
				</ol>
			</div>
		</div>
	</div>
</div>

<div class="content">
	<div class="container-fluid" style="margin: 0 auto !important;">
		<div class="row">
			<div class="col-lg-8">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">
                            <i class="fa fa-edit"></i>
                            <?php echo lang('cat_article_add_form_title') ?>
                        </h3>
                    </div>
                    <div class="card-body">
                    <?=form_open_multipart('kategori/kategori_baru');?>
                        <div class="form-group">
                            <label style="font-size: 13px" name="Judul"><?php echo lang('cat_article_add_title_lbl') ?></label>
                            <input type="text" name="judul" class="form-control" value="<?php if (validation_errors()){echo set_value('judul');}?>" placeholder="<?php echo lang('cat_article_add_title_placeholder') ?>" required>
                                <?php if (validation_errors()){
                                    echo form_error('judul');
                                }?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">
                            <i class="fa fa-info-circle"></i>
                            <?php echo lang('cat_article_add_information_title') ?>
                        </h3>
                    </div>
                    <div class="card-footer">
                        <input type="submit" name="tambah" value="<?php echo lang('cat_article_add_post_cat_article_btn') ?>" class="btn btn-info">
                    </div>
                </div>
                    </form>
            </div>
		</div>
	</div>
</div>