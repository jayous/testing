
<div onload="" class="content-header">
	<div class="container-fluid">
		<div class="row mb-2" style="border-left: 2px solid #3b5998;">
			<div class="col-sm-12">
            <?php
                if ($this->session->flashdata('set_sukses') != null) {
                    echo $this->session->flashdata('set_sukses');
                } else if ($this->session->flashdata('set_fail') != null) {
                    echo $this->session->flashdata('set_gagal');
                }
            ?>
			</div>
			<div class="col-sm-6">
				<h1 class="m-0 text-secondary text-uppercase"><b><?php echo lang('contact_heading') ?></b></h1>
                <span class="text-secondary text-uppercase"></span>
			</div>
			<div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#"><?php echo lang('contact_identifier_1') ?></a></li>
                    <li class="breadcrumb-item active"><?php echo lang('contact_identifier_2') ?></li>
                </ol>
			</div>
		</div>
	</div>
</div>

<div class="content">
    <div class="container-fluid" style="margin: 0 auto !important;">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">
                            <i class="fa fa-edit"></i>
                            <?php echo lang('contact_form_title') ?>
                        </h3>
                    </div>
                    <div class="card-body">
                    <?=form_open_multipart('kontak');?>
                        <div class="row">
                            <div class="col-lg-6 col-md-12 col-sm-12">
                                <div class="form-group">
                                    <label style="font-size: 15px"><?php echo lang('contact_edit_email_lbl') ?></label>
                                    <input type="email" name="email" class="form-control" value="<?php if (!empty($kontak)) { echo $kontak->email; }?>">
                                </div>
                                <div class="form-group">
                                    <label style="font-size: 15px"><?php echo lang('contact_edit_phone_lbl') ?></label>
                                    <input type="text" name="telepon" class="form-control" value="<?php if (!empty($kontak)) { echo $kontak->telepon; }?>">
                                </div><div class="form-group">
                                    <label style="font-size: 15px"><?php echo lang('contact_edit_fax_lbl') ?></label>
                                    <input type="text" name="fax" class="form-control" value="<?php if (!empty($kontak)) { echo $kontak->fax; }?>">
                                </div><div class="form-group">
                                    <label style="font-size: 15px"><?php echo lang('contact_edit_address_lbl') ?></label>
                                    <input type="text" name="alamat" class="form-control" value="<?php if (!empty($kontak)) { echo $kontak->alamat; }?>">
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-12 col-sm-12">
                                <div class="form-group">
                                    <label style="font-size: 15px"><?php echo lang('contact_edit_city_lbl') ?></label>
                                    <input type="text" name="kota" class="form-control" value="<?php if (!empty($kontak)) { echo $kontak->kota; }?>">
                                </div>
                                <div class="form-group">
                                    <label style="font-size: 15px"><?php echo lang('contact_edit_postal_code_lbl') ?></label>
                                    <input type="text" name="kodepos" class="form-control" value="<?php if (!empty($kontak)) { echo $kontak->kodepos; }?>">
                                </div><div class="form-group">
                                    <label style="font-size: 15px"><?php echo lang('contact_edit_province_lbl') ?></label>
                                    <input type="text" name="provinsi" class="form-control" value="<?php if (!empty($kontak)) { echo $kontak->provinsi; }?>">
                                </div><div class="form-group">
                                    <label style="font-size: 15px"><?php echo lang('contact_edit_country_lbl') ?></label>
                                    <input type="text" name="negara" class="form-control" value="<?php if (!empty($kontak)) { echo $kontak->negara; }?>">
                                </div>
                            </div>
                    </div>
                    <div class="card-footer">
                        <input type="submit" name="set_kontak" value="<?php echo lang('contact_edit_btn') ?>" style="float: right;" class="btn btn-info">
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>