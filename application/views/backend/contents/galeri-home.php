<div onload="" class="content-header">
	<div class="container-fluid">
		<div class="row mb-2" style="border-left: 2px solid #3b5998;">
			<div class="col-sm-12">
            <?php if ($this->session->flashdata('galeri_berhasil') != null) {
                echo $this->session->flashdata('galeri_berhasil');
            }
            ?>
			</div>
			<div class="col-sm-6">
				<h1 class="m-0 text-secondary text-uppercase"><b><?php echo lang('gallery_home_heading') ?></b></h1>
				<span class="text-secondary text-uppercase"><?php echo lang('gallery_home_subheading');?></span> 
				
			</div>
			<div class="col-sm-6">
				<ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#"><?php echo lang('gallery_home_identifier_1') ?></a></li>
					<li class="breadcrumb-item active"><?php echo lang('gallery_home_identifier_2') ?></li>
				</ol>
			</div>
		</div>
	</div>
</div>

<div class="content">
	<div class="container-fluid" style="margin: 0 auto !important;">
    <a href="<?=site_url('galeri/gambar_baru')?>"><button class="btn btn-sm btn-success"><?php echo lang('gallery_home_new_gallery_btn') ?></button></a>
    <hr>
        <div class="row">
        
      <?php
      foreach ($file_galeri->result_array() as $key) {
      ?>
			<div class="col-lg-4 col-md-6">
        <div class="card">
            <div class="card-body">
                <div class="form-group">
                    <img width="100%" src="<?php echo base_url().$key['file_loc']?>">
                </div>
            </div>
            <div class="card-footer">
                <a onclick='javascript:return confirm("<?php echo lang('gallery_home_delete_alert') ?>");' href="<?=site_url('galeri/hapus_gambar/'.$key['id']);?>"><button class="btn btn-danger" style="width: 100%;"><?php echo lang('gallery_home_delete_galley_btn') ?></button></a>
            </div>
        </div>
      </div>
      <?php
      }
      ?>
		</div>
	</div>
</div>