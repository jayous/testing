<div class="content-header">
	<div class="container-fluid" >
		<div class="row mb-2">
			<div class="col-sm-6">
				<h1 class="m-0 text-dark"><?php echo lang('gallery_home_heading') ?></h1>
			</div>
			<div class="col-sm-6">
				<ol class="breadcrumb float-sm-right">
					<li class="breadcrumb-item"><a href="#"><?php echo lang('gallery_add_identifier_1') ?></a></li>
					<li class="breadcrumb-item active"><?php echo lang('gallery_add_identifier_2') ?></li>
				</ol>
			</div>
		</div>
	</div>
</div>

<div class="content">
	<div class="container-fluid" style="margin: 0 auto !important;">
		<div class="row">
			<div class="col-lg-8">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">
                            <i class="fa fa-edit"></i>
                            <?php echo lang('gallery_add_form_title') ?>
                        </h3>
                    </div>
                    <div class="card-body">
                    <?=form_open_multipart('galeri/gambar_baru');?>
                        <div class="form-group">
                            <label style="font-size: 13px"><?php echo lang('gallery_add_title_lbl') ?></label>
                            <input type="file" name="userfile" class="form-control" required>
                            <?php if ($this->session->flashdata('error') != null) {
                            echo '<span style="color : red;">'.$this->session->flashdata('error').'</span>';
                            }
                            ?>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="ket" placeholder="tittle">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">
                            <i class="fa fa-info-circle"></i>
                            <?php echo lang('gallery_add_information_title') ?>
                        </h3>
                    </div>
                    <div class="card-footer">
                        <input style="width: 100%;" type="submit" name="upload" value="<?php echo lang('gallery_add_post_gallery_btn') ?>" class="btn btn-info">
                    </div>
                </div>
                    </form>
            </div>
		</div>
	</div>
</div>