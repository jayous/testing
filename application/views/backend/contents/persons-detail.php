<div onload="" class="content-header">
	<div class="container-fluid">
		<div class="row mb-2" style="border-left: 2px solid #3b5998;">
			<div class="col-sm-12">
				<?php if ($this->session->flashdata('message') != null) {
					echo $this->session->flashdata('message');
				}
				?>
			</div>
			<div class="col-sm-6">
				<h1 class="m-0 text-secondary text-uppercase"><b>ANGGOTA</b></h1>
				<span class="text-secondary text-uppercase">di bawah ini adalah daftar anggota</span> 
				
			</div>
		</div>
	</div>
</div>

<div class="content">
	<div class="container-fluid" style="margin: 0 auto !important;">
		<div class="row">
			<div class="col-lg-12">
				<div class="card" style="padding: 9px;">
				<a href="<?=site_url('admin/persons')?>"><button class="btn btn-sm btn-success">KEMBALI</button></a>
					<hr>	
					<div class="card-body table-responsive">
                        <center><img style="max-width: 150px;"  src="<?=base_url().$person->row(0)->file_loc?>" class="img-thumbnail"></center>
                        <table class="table">
                        <hr>
                            <tr>
                                <td>Nama</td>
                                <td><?=$person->row(0)->full_name;?></td>
                            </tr>
                            <tr>
                                <td>Tempat, tanggal lahir</td>
                                <td><?=$person->row(0)->place_of_birth;?>, <?=$person->row(0)->date_of_birth;?> </td>
                            </tr>
                            <tr>
                                <td>Jenis Kelamin</td>
                                <td><?=$person->row(0)->gender;?></td>
                            </tr>
                            <tr>
                                <td>Sekolah</td>
                                <td><?=$person->row(0)->school;?></td>
                            </tr>
                            <tr>
                                <td>Alamat Rumah</td>
                                <td><?=$person->row(0)->home_address;?></td>
                            </tr>
                            <tr>
                                <td>No Tlp</td>
                                <td><?=$person->row(0)->mobile_number;?></td>
                            </tr>
                            <tr>
                                <td>Kelas</td>
                                <td><?=$person->row(0)->class;?></td>
                            </tr>
                        </table>
					
					</div>	
				</div>
			</div>
		</div>
	</div>
</div>