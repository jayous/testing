
<div onload="" class="content-header">
	<div class="container-fluid">
		<div class="row mb-2" style="border-left: 2px solid #3b5998;">
			<div class="col-sm-12">
            <?php
                if ($this->session->flashdata('set_sukses') != null) {
                    echo $this->session->flashdata('set_sukses');
                } else if ($this->session->flashdata('set_fail') != null) {
                    echo $this->session->flashdata('set_gagal');
                }
            ?>
			</div>
			<div class="col-sm-6">
				<h1 class="m-0 text-secondary text-uppercase"><b><?php echo lang('profile_heading') ?></b></h1>
                <span class="text-secondary text-uppercase"></span>
			</div>
			<div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#"><?php echo lang('profile_identifier_1') ?></a></li>
                    <li class="breadcrumb-item active"><?php echo lang('profile_identifier_2') ?></li>
                </ol>
			</div>
		</div>
	</div>
</div>

<div class="content">
    <div class="container-fluid" style="margin: 0 auto !important;">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">
                            <i class="fa fa-edit"></i>
                            <?php echo lang('profile_form_title') ?>
                        </h3>
                    </div>
                    <div class="card-body">
                    <?=form_open_multipart('profil');?>
                        <div class="form-group">
                            <label style="font-size: 15px"><?php echo lang('profile_vision_lbl') ?></label>
                            <!-- <textarea name="visi" rows="5" class="form-control"><?=$profil->visi?></textarea> -->
                            <textarea id="ckeditor" name="visi" class="form-control ckeditor"><?php if (!empty($profil)) { echo $profil->visi; }?></textarea>
                        </div>
                        <div class="form-group">
                            <label style="font-size: 15px"><?php echo lang('profile_mission_lbl') ?></label>
                            <textarea id="ckeditor" name="misi" class="form-control ckeditor"><?php if (!empty($profil)) { echo $profil->misi; }?></textarea>
                        </div>
                        <div class="form-group">
                            <label style="font-size: 15px"><?php echo lang('profile_history_lbl') ?></label>
                            <textarea id="ckeditor" name="sejarah" class="form-control ckeditor"><?php if (!empty($profil)) { echo $profil->sejarah; }?></textarea>
                        </div>
                    </div>
                    <div class="card-footer">
                        <input type="submit" name="set_profil" value="<?php echo lang('profile_edit_btn') ?>" style="float: right;" class="btn btn-info">
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>