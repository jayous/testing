    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1><?php echo lang('article_detail_heading') ?></h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#"><?php echo lang('article_detail_identifier_1') ?></a></li>
              <li class="breadcrumb-item active"><?php echo lang('article_detail_identifier_2') ?></li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
     <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <!-- Main content -->
            <div class="invoice p-3 mb-3">
              <!-- title row -->
              <div class="row">
                <div class="col-12">
                  <h4>
                    <i class="fa fa-check-square-o"></i> <?php echo lang('article_detail_form_title') ?>
                    <small class="float-right"><?php echo lang('article_detail_date_post') ?> <?=$artikel->tanggal_post?></small>
                  </h4>
                </div>
                <!-- /.col -->
              </div>
              <!-- info row -->
              <div class="row invoice-info">
                <div class="col-sm-12 invoice-col">
                  <address>
                    <table class="table table-condensed">
                        <tr>
                            <td> <?php echo lang('article_detail_title_lbl') ?> </td>
                            <td>:</td>
                            <td><?=$artikel->judul?> </td>
                        </tr>
                        <tr>
                            <td> <?php echo lang('article_detail_last_update_lbl') ?></td>
                            <td>:</td>
                            <td><?=$artikel->diubah?></td>
                        </tr>
                        <tr>
                            <td> <?php echo lang('article_detail_category_lbl') ?></td>
                            <td>:</td>
                            <td><?=$artikel->nama?></td>
                        </tr>
                    </table>
                  </address>  
                </div>

              </div>
              <!-- /.row -->

              <!-- Table row -->

              <div class="row">
                <div class="col-12 table-responsive">
                <p style="background-color: silver; padding: 8px; border-radius: 4px;" class="lead" style="margin: 0;"><?php echo lang('article_detail_content_lbl') ?></p>
                  <address>
                  <p style="text-align: justify;"><?=$artikel->konten?></p>
                  </address>
                </div>
              </div>
            </div>
            <!-- /.invoice -->
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
