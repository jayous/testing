<div onload="" class="content-header">
	<div class="container-fluid">
		<div class="row mb-2" style="border-left: 2px solid #3b5998;">
			<div class="col-sm-12">
				<?php if ($this->session->flashdata('artikel_berhasil') != null) {
					echo $this->session->flashdata('artikel_berhasil');
				}
				?>
			</div>
			<div class="col-sm-6">
				<h1 class="m-0 text-secondary text-uppercase"><b><?=lang('article_home_heading')?></b></h1>
				<span class="text-secondary text-uppercase"><?php echo lang('article_home_subheading');?></span> 
				
			</div>
			<div class="col-sm-6">
				<ol class="breadcrumb float-sm-right">
					<li class="breadcrumb-item"><a href="#"><?=lang('article_home_identifier_1')?></a></li>
					<li class="breadcrumb-item active"><?=lang('article_home_identifier_2')?></li>
				</ol>
			</div>
		</div>
	</div>
</div>

<div class="content">
	<div class="container-fluid" style="margin: 0 auto !important;">
		<div class="row">
			<div class="col-lg-12">
				<div class="card" style="padding: 9px;">
				<a href="<?=site_url('admin/artikel/artikel_baru')?>"><button class="btn btn-sm btn-success"><?=lang('article_home_new_article_btn')?></button></a>
					<hr>	
					<div class="card-body table-responsive">
					
					<table id="example3" class="table table-hover">
							<thead>
								<tr>
									<th>#</th>
									<th><?=lang('article_home_title_lbl')?></th>
									<th><?php echo lang('article_home_last_update_lbl') ?></th>
									<th><?php echo lang('article_home_category_lbl') ?></th>
                                    <th><?php echo lang('article_home_actions_lbl') ?></th>
								</tr>
							</thead>
							<tbody>
								<?php
								$n=1;
								foreach ($artikel->result_array() as $artikel) {
								?>
								<tr>
                                	<td><?=$n;?></td>
                                	<td><?=$artikel['judul'];?></td>
                                	<td><?=date('d F Y', strtotime($artikel['diubah']));?></td>
                                	<td><?=$artikel['nama'];?></td>
                                	<td><div class="btn-group">
                    					<button type="button" class="btn btn-success"><?php echo lang('article_home_actions_lbl') ?></button>
                    					<button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown">
                    						<span class="caret"></span>
                    						<span class="sr-only">Toggle Dropdown</span>
                    					</button>
                    					<div class="dropdown-menu">
                    						<a href="<?=site_url('admin/artikel/sunting/'.$artikel['id'].'/'.$artikel['slug'])?>" class="dropdown-item"><i class="fa fa-edit fa-fw"></i> <?php echo lang('article_home_edit_article_btn') ?></a>
                    						<a onclick='javascript:return confirm("<?php echo lang('article_home_delete_alert') ?>");' href="<?=site_url('artikel/hapus/'.$artikel['id'].'/'.$artikel['slug'])?>" class="dropdown-item fa-fw"><i class="fa fa-trash fa-fw"></i> <?php echo lang('article_home_delete_article_btn') ?></a>
                    						<a href="<?=site_url('admin/artikel/detail/'.$artikel['id'].'/'.$artikel['slug'])?>" class="dropdown-item"><i class="fa fa-eye fa-fw"></i> <?php echo lang('article_home_detail_article_btn') ?></a>
                    					</div>
                  					</div></td>
                                </tr>
								<?php
								$n++;
								}
								unset($n);
								?>
							</tbody>
						</table>
					</div>	
				</div>
			</div>
		</div>
	</div>
</div>