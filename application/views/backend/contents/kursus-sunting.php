<div class="content-header">
	<div class="container-fluid" >
		<div class="row mb-2">
			<div class="col-sm-12">
				
			</div>
			<div class="col-sm-6">
				<h1 class="m-0 text-dark"><?php echo lang('courses_home_heading') ?></h1>
			</div>
			<div class="col-sm-6">
				<ol class="breadcrumb float-sm-right">
					<li class="breadcrumb-item"><a href="#"><?php echo lang('courses_edit_identifier_1') ?></a></li>
					<li class="breadcrumb-item active"><?php echo lang('courses_edit_identifier_2') ?></li>
				</ol>
			</div>
		</div>
	</div>
</div>

<div class="content">
	<div class="container-fluid" style="margin: 0 auto !important;">
		<div class="row">
			<div class="col-lg-8">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">
                            <i class="fa fa-edit"></i>
                            <?php echo lang('courses_edit_form_title') ?>
                        </h3>
                    </div>
                    <div class="card-body">
                    <?=form_open('kursus/sunting/'.$program->slug);?>
                        <div class="form-group">
                            <label style="font-size: 13px" name="nama"><?php echo lang('courses_edit_title_lbl') ?></label>
                            <input type="text" name="nama" class="form-control" value="<?php if (validation_errors()){echo set_value('nama');} else {echo $program->nama; }?>" placeholder="<?php echo lang('courses_edit_title_placeholder') ?>" required>
                                <?php if (validation_errors()){
                                    echo form_error('nama');
                                }?>
                            <input type="hidden" name="idlama" value="<?=$program->id?>">
                        </div>
                        <div class="form-group">
                            <label style="font-size: 13px" name="deskripsi"><?php echo lang('courses_edit_desc_lbl') ?></label>
                            <textarea class="form-control" name="deskripsi" rows="4"><?php if (validation_errors()){echo set_value('deskripsi');} else {echo $program->deskripsi; }?></textarea>
                                <?php if (validation_errors()){
                                    echo form_error('deskripsi');
                                }?>
                        </div>
                        <div class="form-group">
                            <label style="font-size: 13px" name="harga"><?php echo lang('courses_edit_price_lbl') ?></label>
                            <input type="number" name="harga" class="form-control" value="<?php if (validation_errors()){echo set_value('harga');} else {echo $program->harga; }?>" placeholder="<?php echo lang('courses_edit_price_placeholder') ?>" required>
                                <?php if (validation_errors()){
                                    echo form_error('harga');
                                }?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">
                            <i class="fa fa-info-circle"></i>
                            <?php echo lang('courses_edit_information_title') ?>
                        </h3>
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <input type="submit" style="width: 100%;" name="sunting" value="<?php echo lang('courses_edit_post_courses_btn') ?>" class="btn btn-info">
                        </div>
                    </div>
                </div>
            </div>
                </form>
		</div>
	</div>
</div>