<div onload="" class="content-header">
	<div class="container-fluid">
		<div class="row mb-2" style="border-left: 2px solid #3b5998;">
			<div class="col-sm-12">
				<?php if ($this->session->flashdata('message') != null) {
					echo $this->session->flashdata('message');
				}
				?>
			</div>
			<div class="col-sm-6">
				<h1 class="m-0 text-secondary text-uppercase"><b>ANGGOTA</b></h1>
				<span class="text-secondary text-uppercase">di bawah ini adalah daftar anggota</span> 
				
			</div>
		</div>
	</div>
</div>

<div class="content">
	<div class="container-fluid" style="margin: 0 auto !important;">
		<div class="row">
			<div class="col-lg-12">
				<div class="card" style="padding: 9px;">
				<a href="<?=site_url('persons/tambah')?>"><button class="btn btn-sm btn-success">TAMBAH</button></a>
					<hr>	
					<div class="card-body table-responsive">
					
					<table id="example3" class="table table-hover">
							<thead>
								<tr>
									<th>#</th>
									<th>Nama</th>
									<th>No Tlp</th>
									<th>Kelas</th>
                                    <th>Alamat</th>
								</tr>
							</thead>
							<tbody>
								<?php
								$n=1;
								foreach ($persons->result_array() as $p) {
								?>
								<tr>
                                	<td><?=$n;?></td>
                                	<td><?=$p['full_name'];?></td>
                                	<td><?=$p['mobile_number']?></td>
                                	<td><?=$p['class'];?></td>
                                	<td><div class="btn-group">
                    					<button type="button" class="btn btn-success">Aksi</button>
                    					<button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown">
                    						<span class="caret"></span>
                    						<span class="sr-only">Toggle Dropdown</span>
                    					</button>
                    					<div class="dropdown-menu">
                    						<a href="<?=site_url('admin/persons/sunting/'.$p['id'])?>" class="dropdown-item"><i class="fa fa-edit fa-fw"></i>Edit</a>
                    						<a onclick='javascript:return confirm("Aapakah anda serius");' href="<?=site_url('persons/hapus/'.$p['id'])?>" class="dropdown-item fa-fw"><i class="fa fa-trash fa-fw"></i>Hapus</a>
                    						<a href="<?=site_url('admin/persons/tampil/'.$p['id'])?>" class="dropdown-item"><i class="fa fa-eye fa-fw"></i>Lihat</a>
                    					</div>
                  					</div></td>
                                </tr>
								<?php
								$n++;
								}
								unset($n);
								?>
							</tbody>
						</table>
					</div>	
				</div>
			</div>
		</div>
	</div>
</div>