<div class="content-header">
	<div class="container-fluid" >
		<div class="row mb-2">
			<div class="col-sm-6">
				<h1 class="m-0 text-dark"><?php echo lang('article_home_heading') ?></h1>
			</div>
			<div class="col-sm-6">
				<ol class="breadcrumb float-sm-right">
					<li class="breadcrumb-item"><a href="#"><?php echo lang('article_edit_identifier_1') ?></a></li>
					<li class="breadcrumb-item active"><?php echo lang('article_edit_identifier_2') ?></li>
				</ol>
			</div>
		</div>
	</div>
</div>

<div class="content">
	<div class="container-fluid" style="margin: 0 auto !important;">
		<div class="row">
			<div class="col-lg-8">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">
                            <i class="fa fa-edit"></i>
                            <?php echo lang('article_edit_form_title') ?>
                        </h3>
                    </div>
                    <div class="card-body">
                    <?=form_open_multipart('admin/artikel/sunting/'.$artikel->row()->id.'/'.$artikel->row()->slug);?>
                        <div class="form-group">
                            <label style="font-size: 13px"><?php echo lang('article_edit_title_lbl') ?></label>
                            <input type="hidden" name="judulhid" class="form-control" value="<?php echo $artikel->row()->judul ?>">
                            <input type="text" name="judul" class="form-control" value="<?php echo $artikel->row()->judul ?>" placeholder="<?php echo lang('article_edit_title_placeholder') ?>" required>
                                <?php if (validation_errors()){
                                    echo form_error('judul');
                                }?>
                            <input type="hidden" name="id" class="form-control" value="<?php echo $artikel->row()->id ?>">
                        </div>
                        <div class="form-group">
                            <label style="font-size: 13px"><?php echo lang('article_edit_content_lbl') ?></label>
                            <textarea id="ckeditor" name="konten" class="form-control ckeditor" required "><?php echo $artikel->row()->konten; ?></textarea>
                            <?php if (validation_errors()){
                                    echo form_error('konten');
                                }?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">
                            <i class="fa fa-info-circle"></i>
                            <?php echo lang('article_edit_information_title') ?>
                        </h3>
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <label for="sel1"><?php echo lang('article_edit_select_category') ?></label>
                            <select class="form-control" id="sel1" name="kategori">
                                <?php
                                foreach ($kategori->result_array() as $kat) {
                                ?>
                                    <option <?php if ($artikel->row()->kategori_artikel_id == $kat['id']) { echo 'selected'; } ?> value="<?=$kat['id']?>" ><?=$kat['nama']?></option>
                                <?php
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="sel1"><?php echo lang('article_edit_upload_lbl') ?></label>
                            <?php
                                if (!empty($file_lokasi)) {
                                ?>
                                    <img width="100%" src="<?=base_url().$file_lokasi?>"><hr>
                                <?php
                                }
                            ?>
                            <input type="file" name="userfile" class="form-control">
                            <?php if ($this->session->flashdata('upload') != null) {
                            echo '<span class="text-danger">'.$this->session->flashdata('upload').'</span>';
                            }
                            ?>
                        </div>
                    </div>
                    <div class="card-footer">
                        <input type="submit" name="perbarui" value="<?php echo lang('article_edit_post_article_btn') ?>" class="btn btn-info" style="float: right;">
                    </div>
                </div>
                    </form>
            </div>
		</div>
	</div>
</div>