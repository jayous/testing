
    <div onload="" class="content-header">
        <div class="container-fluid">
            <div class="row mb-2" style="border-left: 2px solid #3b5998;">
                <div class="col-sm-12">
                    <?php if ($this->session->flashdata('message') != null) {
                        echo $this->session->flashdata('message');
                    }
                    ?>
                            
                </div>
                <div class="col-sm-12">
                    <h1 class="m-0 text-secondary text-uppercase"><b>ANGGOTA</b></h1> 
                    <span class="text-secondary text-uppercase">silahkan masukan informasi pengguna di bawah ini</span>
                </div>
            </div>
        </div>
    </div>

    <div class="content">
        <div class="container-fluid">
            <div class="row">
            <div class="col-md-8">
				<div class="card">
                    <div class="card-header">
                        <h3 class="card-title">
                            <i class="fa fa-edit"></i>
                                isi dengan baik dan benar
                        </h3>
                    </div>
					<div class="card-body">
                    <?=form_open_multipart('persons/sunting/'.$aidi);?>
                    <?php echo form_input($id);?>  
                    <div class="form-group">
                        <span class="label">Nama Lengkap <?php if (validation_errors()){
                                            echo form_error('full_name');}?></span> 
                                            <?php echo form_input($full_name);?>               
                        <span class="focus-input100"></span>
                    </div>
                              
                    <div class="form-group">
						<span class="label">Tempat Lahir <?php if (validation_errors()){
                            echo form_error('place_of_birth');}?></span>                
                            <?php echo form_input($place_of_birth);?>
						<span class="focus-input100"></span>
                    </div>
                             

                    <div class="form-group">
					    <span class="label">Tanggal Lahir <?php if (validation_errors()){
                            echo form_error('date_of_birth');}?></span>                
                            <?php echo form_input($date_of_birth);?>
						<span class="focus-input100"></span>
					</div>

                              
					<div class="form-group">
						<span class="label">Jenis Kelamin <?php if (validation_errors()){
                                    echo form_error('gender');}?></span>              
                                    <?php echo form_dropdown('gender', $gender, 'S', ['class' => 'form-control']);?>
						<span class="focus-input100"></span>
					</div>

					<div class="form-group">
						<span class="label">Sekolah<?php if (validation_errors()){
                                    echo form_error('school');}?></span>
                                    <?php echo form_input($school);?>
						<span class="focus-input100"></span>
                              </div>
                              
                    <div class="form-group">
						<span class="label">Alamat Rumah<?php if (validation_errors()){
                                    echo form_error('home_address');}?></span>
                                    <?php echo form_input($home_address);?>
						<span class="focus-input100"></span>
					</div>

                     <div class="form-group">
						<span class="label">No Tlp<?php if (validation_errors()){
                                    echo form_error('mobile_number');}?></span>
                                    <?php echo form_input($mobile_number);?>
						<span class="focus-input100"></span>
					</div>    

                    <div class="form-group">
						<span class="label">Kelas <?php if (validation_errors()){
                                    echo form_error('class');}?></span>
                                    <?php echo form_dropdown('class', $class, 'S', ['class' => 'form-control']);?>
						<span class="focus-input100"></span>
					</div>    
					</div>	
				</div>
			</div>

            <div class="col-lg-4">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">
                            <i class="fa fa-info-circle"></i>
                            <?php echo lang('article_add_information_title') ?>
                        </h3>
                    </div>
                    <div class="card-body">
                    <img src="<?=base_url().$image?>" class="img-thumbnail">
                        <div class="form-group">
                            <label style="font-size: 13px"><?php echo lang('article_add_upload_lbl') ?></label>
                            <input type="file" name="userfile" class="form-control">
                        </div>
                        <?php if ($this->session->flashdata('upload') != null) {
                            echo '<span style="color : red;">'.$this->session->flashdata('upload').'</span>';
                        }
                        ?>
                          <div class="form-group">
							<input type="submit" name="kirim" style="width: 100%;" class="btn btn-success" value="Simpan">        
					</div>
                    </div>
                </div>
                    </form>
            </div>

            </div>
	    </div>
    </div>



