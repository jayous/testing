<div class="content-wrapper">
<div onload="" class="content-header">
	<div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-12">
				<!-- <?php if ($this->session->flashdata('artikel_berhasil') != null) {
					echo $this->session->flashdata('artikel_berhasil');
				}
				?> -->
			</div>
			<div class="col-sm-6">
				<h1 class="m-0 text-dark"><?php echo lang('index_heading');?> </h1> 
			</div>
			<div class="col-sm-6">
				<ol class="breadcrumb float-sm-right">
					<li class="breadcrumb-item"><a href="#">Home</a></li>
					<li class="breadcrumb-item active">Daftar Artikel</li>
				</ol>
			</div>
		</div>
	</div>
</div>

<div class="content">
      <div class="container-fluid" style="margin: 0 auto !important;">
		<div class="row">
			<div class="col-lg-8">
				<div class="card">
                              <div class="card-header">
                                    <h3 class="card-title">
                                    <i class="fa fa-edit"></i>
                                          <?php echo lang('create_user_subheading')?>
                                    </h3>
                              </div>
					<div class="card-body">
					
                              <form action="<?php echo site_url('admin/pengguna/buat_pengguna')?>" class="login100-form validate-form" method="POST">
                              <?php echo $this->session->flashdata('message')?>
                              
                              <div class="wrap-input100 validate-input m-b-26">
						<span class="label-input100"><?php echo lang('create_user_fname_label', 'first_name');?> <?php if (validation_errors()){
                                    echo form_error('first_name');}?></span> 
                                    <?php echo form_input($first_name);?>               
						<span class="focus-input100"></span>
                              </div>
                              
                              <div class="wrap-input100 validate-input m-b-26">
						<span class="label-input100"><?php echo lang('create_user_lname_label', 'last_name');?> <?php if (validation_errors()){
                                    echo form_error('last_name');}?></span>                
                                    <?php echo form_input($last_name);?>
						<span class="focus-input100"></span>
                              </div>
                              <?php if($identity_column!=='email') {?>
                              <div class="wrap-input100 validate-input m-b-26">
						<span class="label-input100"><?php echo lang('create_user_identity_label', 'identity');?> <?php if (validation_errors()){
                                    echo form_error('identity');}?></span>                
                                    <?php echo form_input($identity);?>
						<span class="focus-input100"></span>
                              </div>
                              <?php } ?>

                              <div class="wrap-input100 validate-input m-b-26">
						<span class="label-input100"><?php echo lang('create_user_company_label', 'company');?> <?php if (validation_errors()){
                                    echo form_error('company');}?></span>                
                                    <?php echo form_input($company);?>
						<span class="focus-input100"></span>
					</div>

                              <div class="wrap-input100 validate-input m-b-26">
						<span class="label-input100"><?php echo lang('create_user_email_label', 'email');?> <?php if (validation_errors()){
                                    echo form_error('email');}?></span>                
                                    <?php echo form_input($email);?>
						<span class="focus-input100"></span>
                              </div>
                              
					<div class="wrap-input100 validate-input m-b-26">
						<span class="label-input100"><?php echo lang('create_user_phone_label', 'phone');?> <?php if (validation_errors()){
                                    echo form_error('phone');}?></span>                
                                    <?php echo form_input($phone);?>
						<span class="focus-input100"></span>
					</div>

					<div class="wrap-input100 validate-input m-b-18">
						<span class="label-input100"><?php echo lang('create_user_password_label', 'password');?><?php if (validation_errors()){
                                    echo form_error('password');}?></span>
                                    <?php echo form_input($password);?>
						<span class="focus-input100"></span>
                              </div>
                              
                              <div class="wrap-input100 validate-input m-b-18">
						<span class="label-input100"><?php echo lang('create_user_password_confirm_label', 'password_confirm');?><?php if (validation_errors()){
                                    echo form_error('password_confirm');}?></span>
                                    <?php echo form_input($password_confirm);?>
						<span class="focus-input100"></span>
					</div>

					
					<div class="container-login100-form-btn">
							<input type="submit" style="width: 100%;" class="btn btn-success" value="<?php echo lang('create_user_submit_btn')?>">
                            
					</div>
				</form>
	
					</div>	
				</div>
			</div>
		</div>
	</div>
</div>
</div>


