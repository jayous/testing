<div class="content-wrapper">
<div onload="" class="content-header">
	<div class="container-fluid">
		<div class="row mb-2" style="border-left: 2px solid #3b5998;">
			<div class="col-sm-12">
				<?php if ($this->session->flashdata('artikel_berhasil') != null) {
					echo $this->session->flashdata('artikel_berhasil');
				}
				?>
			</div>
			<div class="col-sm-12">
				<h1 class="m-0 text-secondary text-uppercase"><b><?php echo lang('index_heading')?></b></h1> 
				<span class="text-secondary text-uppercase"><?php echo lang('index_subheading')?></span>
			</div>
		</div>
	</div>
</div>

<div class="content">
	<div class="container-fluid" style="margin: 0 auto !important;">
		<div class="row">
			<div class="col-lg-12">
				<div class="card">
							<div class="card-header">
									<?php echo anchor('admin/pengguna/buat_pengguna', lang('index_create_user_link'), array('class' => 'btn btn-sm btn-success'))?> | <?php echo anchor('admin/pengguna/buat_grup', lang('index_create_group_link'), array('class' => 'btn btn-sm btn-success'))?>
                              </div>
					<div class="card-body">
						<table id="" class="table table-hover">
							<thead>
								<tr>
									<th><?php echo lang('index_fname_th');?></th>
									<th><?php echo lang('index_lname_th');?></th>
									<th><?php echo lang('index_email_th');?></th>
									<th><?php echo lang('index_groups_th');?></th>
									<th><?php echo lang('index_status_th');?></th>
									<th><?php echo lang('index_action_th');?></th>
								</tr>
							</thead>
							<tbody>
								<?php
									$n=1;
							 		foreach ($users as $user):?>
									<tr>
										<td><?php echo htmlspecialchars($user->first_name,ENT_QUOTES,'UTF-8');?></td>
										<td><?php echo htmlspecialchars($user->last_name,ENT_QUOTES,'UTF-8');?></td>
										<td><?php echo htmlspecialchars($user->email,ENT_QUOTES,'UTF-8');?></td>
										<td>
											<?php foreach ($user->groups as $group):?>
												<?php echo anchor("pengguna/edit_group/".$group->id, htmlspecialchars($group->name,ENT_QUOTES,'UTF-8'), array('class'=>'text-secondary text-sm')) ;?><br />
											<?php endforeach?>
										</td>
										<td><?php echo ($user->active) ? anchor("pengguna/deactivate/".$user->id, lang('index_active_link'), array('class' => 'text-success text-sm')) : anchor("pengguna/activate/". $user->id, lang('index_inactive_link'), array('class' => 'text-danger text-sm'));?></td>
										<td>
										<a href="<?=site_url('admin/pengguna/edit_pengguna/'.$user->id);?>" class="btn btn-sm btn-secondary"><i class="fa fa-edit"></i> Edit</a>
										</td>
									</tr>
								<?php endforeach;?>
							</tbody>
						</table>
						
					</div>	
				</div>
			</div>
		</div>
	</div>
</div>
</div>

