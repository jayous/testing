<div class="content-wrapper">
<div onload="" class="content-header">
	<div class="container-fluid">
		<div class="row mb-2" style="border-left: 2px solid #3b5998;">
			<div class="col-sm-12">
				<?php if ($this->session->flashdata('artikel_berhasil') != null) {
					echo $this->session->flashdata('artikel_berhasil');
				}
				?>
                        <?php echo $message;?>
			</div>
			<div class="col-sm-12">
				<h1 class="m-0 text-secondary text-uppercase"><b><?php echo lang('edit_group_heading');?></b></h1> 
				<span class="text-secondary text-uppercase"><?php echo lang('edit_group_subheading')?></span>
			</div>
		</div>
	</div>
</div>
<div class="content">
      <div class="container-fluid" style="margin: 0 auto !important;">
		<div class="row">
			<div class="col-lg-8">
				<div class="card">
                              <div class="card-header">
                                    <h3 class="card-title">
                                    <i class="fa fa-edit"></i>
                                          <?php echo lang('edit_group_subheading')?>
                                    </h3>
                              </div>
					<div class="card-body">
					
                              <form action="<?php echo site_url(uri_string())?>" class="" method="POST">
                              <?php echo $this->session->flashdata('message')?>
                              
                              <div class="form-group">
						<span class="label"><?php echo lang('edit_group_name_label', 'group_name');?> <?php if (validation_errors()){
                                    echo form_error('group_name');}?></span> 
                                    <?php echo form_input($group_name);?>              
						<span class="focus-input100"></span>
                              </div>
                              
                              <div class="form-group">
						<span class="label"><?php echo lang('edit_group_desc_label', 'description');?> <?php if (validation_errors()){
                                    echo form_error('description');}?></span>                
                                    <?php echo form_input($group_description);?>
						<span class="focus-input100"></span>
                              </div>	
					
					<div class="form-group">
							<input type="submit" style="width: 100%;" class="btn btn-success" value="<?php echo lang('edit_group_submit_btn');?>">
					</div>
				</form>
	
					</div>	
				</div>
			</div>
		</div>
	</div>
</div>
</div>

