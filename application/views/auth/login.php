<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content=""> 
    <title>Login</title>
   
    <link href="<?php echo base_url('assets')?>/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url('assets')?>/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?php echo base_url('assets')?>/css/animate.min.css" rel="stylesheet"> 
    <link href="<?php echo base_url('assets')?>/css/lightbox.css" rel="stylesheet"> 
	<link href="<?php echo base_url('assets')?>/css/responsive.css" rel="stylesheet">
    <link href="<?php echo base_url('assets')?>/css/util.css" rel="stylesheet">
    <link href="<?php echo base_url('assets')?>/css/main.css" rel="stylesheet">
        
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
</head>
<body>      
    <div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<div class="login100-form-title" style="background-image: url(images/bg-01.jpg);">
					<span class="login100-form-title-1">
						<?php echo lang('login_heading')?>
                       
					</span>
                    
				</div>
				<form action="<?php echo site_url('auth/login')?>" class="login100-form validate-form" method="POST">
                    <?php echo $this->session->flashdata('message')?>
					<div class="wrap-input100 validate-input m-b-26" data-validate="Username is required">
						<span class="label-input100"><?php echo lang('login_identity_label', 'identity');?> <?php if (validation_errors()){
                                    echo form_error('identity');
                                }?></span>
                
                
                        <?php echo form_input($identity);?>
						<span class="focus-input100"></span>
					</div>

					<div class="wrap-input100 validate-input m-b-18" data-validate = "Password is required">
						<span class="label-input100"><?php echo lang('login_password_label', 'password');?><?php if (validation_errors()){
                                    echo form_error('password');
                                }?></span>
            
                        <?php echo form_input($password);?>
						<span class="focus-input100"></span>
					</div>

					<div class="flex-sb-m w-full p-b-30">

						<div>
							<a href="<?php echo site_url('pengguna/lupa_sandi')?>" class="txt1">
                                <?php echo lang('login_forgot_password');?>
							</a>
						</div>
					</div>

					<div class="container-login100-form-btn">
							<input type="submit" class="login100-form-btn" value="<?php echo lang('login_submit_btn')?>">
                            
					</div>

				</form>
			</div>
		</div>  
    </section>   
</body>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url('assets')?>/js/lightbox.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url('assets')?>/js/wow.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url('assets')?>/js/main.js"></script>
    
    
</body>
</html>