<div class="content-wrapper">
<div onload="" class="content-header">
	<div class="container-fluid">
		<div class="row mb-2" style="border-left: 2px solid #3b5998;">
			<div class="col-sm-12">
				<?php if ($this->session->flashdata('artikel_berhasil') != null) {
					echo $this->session->flashdata('artikel_berhasil');
				}
				?>
                      
			</div>
			<div class="col-sm-12">
				<h1 class="m-0 text-secondary text-uppercase"><b><?php echo lang('deactivate_heading');?></b></h1> 
				<span class="text-secondary text-uppercase"></span>
			</div>
		</div>
	</div>
</div>

<div class="content">
  <div class="container-fluid" style="margin: 0 auto !important;">
		<div class="row">
			<div class="col-lg-12">
				<div class="card">
          <div class="card-header">
            <h3 class="card-title">
              <i class="fa fa-edit"></i>
                <?php echo lang('edit_group_subheading')?>
            </h3>
          </div>
					<div class="card-body">
          <?php echo form_open("pengguna/deactivate/".$user->id);?>
          <p><?php echo sprintf(lang('deactivate_subheading'), $user->username);?></p>
          <p>
            <?php echo lang('deactivate_confirm_y_label', 'confirm');?>
            <input type="radio" name="confirm" value="yes" checked="checked" />
            <?php echo lang('deactivate_confirm_n_label', 'confirm');?>
            <input type="radio" name="confirm" value="no" />
          </p>

          <?php echo form_hidden($csrf); ?>
          <?php echo form_hidden(array('id'=>$user->id)); ?>

          <p><?php echo form_submit('submit', lang('deactivate_submit_btn'), ['class' => 'btn btn-success', 'value' => 'OK']);?></p>

          <?php echo form_close();?>
                             
	
					</div>	
				</div>
			</div>
		</div>
	</div>
</div>
</div>


