<div class="content-wrapper">
<div onload="" class="content-header">
	<div class="container-fluid">
		<div class="row mb-2" style="border-left: 2px solid #3b5998;">
			<div class="col-sm-12">
				<?php if ($this->session->flashdata('artikel_berhasil') != null) {
					echo $this->session->flashdata('artikel_berhasil');
				}
				?>
                        <?php echo $message;?>
			</div>
			<div class="col-sm-12">
				<h1 class="m-0 text-secondary text-uppercase"><b><?php echo lang('edit_user_heading')?></b></h1> 
				<span class="text-secondary text-uppercase"><?php echo lang('edit_user_subheading')?></span>
			</div>
		</div>
	</div>
</div>

<div class="content">
      <div class="container-fluid" style="margin: 0 auto !important;">
		<div class="row">
			<div class="col-lg-12">
				<div class="card">
                              <div class="card-header">
                                    <h3 class="card-title">
                                    <i class="fa fa-edit"></i>
                                          <?php echo lang('edit_user_subheading')?>
                                    </h3>
                              </div>
					<div class="card-body">
					
                              <form action="<?php echo site_url(uri_string());?>" class="" method="POST">
                              <?php echo $this->session->flashdata('message')?>
                              
                              <div class="form-group">
						<span class="label"><?php echo lang('edit_user_fname_label', 'first_name');?> <?php if (validation_errors()){
                                    echo form_error('first_name');}?></span> 
                                    <?php echo form_input($first_name);?>               
						<span class="focus-input100"></span>
                              </div>
                              
                              <div class="form-group">
						<span class="label"><?php echo lang('edit_user_lname_label', 'last_name');?> <?php if (validation_errors()){
                                    echo form_error('last_name');}?></span>                
                                    <?php echo form_input($last_name);?>
						<span class="focus-input100"></span>
                              </div>
                             

                              <div class="form-group">
						<span class="label"><?php echo lang('edit_user_company_label', 'company');?> <?php if (validation_errors()){
                                    echo form_error('company');}?></span>                
                                    <?php echo form_input($company);?>
						<span class="focus-input100"></span>
					</div>

                              
					<div class="form-group">
						<span class="label"><?php echo lang('edit_user_phone_label', 'phone');?> <?php if (validation_errors()){
                                    echo form_error('phone');}?></span>                
                                    <?php echo form_input($phone);?>
						<span class="focus-input100"></span>
					</div>

					<div class="form-group">
						<span class="label"><?php echo lang('edit_user_password_label', 'password');?><?php if (validation_errors()){
                                    echo form_error('password');}?></span>
                                    <?php echo form_input($password);?>
						<span class="focus-input100"></span>
                              </div>
                              
                              <div class="form-group">
						<span class="label"><?php echo lang('edit_user_password_confirm_label', 'password_confirm');?><?php if (validation_errors()){
                                    echo form_error('password_confirm');}?></span>
                                    <?php echo form_input($password_confirm);?>
						<span class="focus-input100"></span>
					</div>
                              <?php if ($this->ion_auth->is_admin()): ?>
                              <div class="form-group">
                              <span class="label"><b><?php echo lang('edit_user_groups_heading');?></b><span class="label">
                                    <?php foreach ($groups as $group):?>
                                          <label class="checkbox">
                                                <?php
                                                      $gID=$group['id'];
                                                      $checked = null;
                                                      $item = null;
                                                      foreach($currentGroups as $grp) {
                                                            if ($gID == $grp->id) {
                                                            $checked= ' checked="checked"';
                                                            break;
                                                            }
                                                      }
                                                ?>
                                                <input type="checkbox" name="groups[]" value="<?php echo $group['id'];?>"<?php echo $checked;?>>
                                                <?php echo htmlspecialchars($group['name'],ENT_QUOTES,'UTF-8');?>
                                          </label>
                                    <?php endforeach?>

                              <?php endif ?>
                              </div>
                              <?php echo form_hidden('id', $user->id);?>
                              <?php echo form_hidden($csrf); ?>
					
					<div class="container-login100-form-btn">
							<input type="submit" style="width: 100%;" class="btn btn-success" value="<?php echo lang('edit_user_submit_btn')?>">
                            
					</div>
				</form>
	
					</div>	
				</div>
			</div>
		</div>
	</div>
</div>
</div>

