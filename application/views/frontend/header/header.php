<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content=""> 
    <title>{blog_title}</title>
    <!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous"> -->
    
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link href="<?php echo base_url('assets')?>/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url('assets')?>/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?php echo base_url('assets')?>/css/animate.min.css" rel="stylesheet"> 
    <link href="<?php echo base_url('assets')?>/css/lightbox.css" rel="stylesheet"> 
	<link href="<?php echo base_url('assets')?>/css/main.css" rel="stylesheet">
	<link href="<?php echo base_url('assets')?>/css/responsive.css" rel="stylesheet">
    
   
    <!--[if lt IE 9]>
	    <script src="js/html5shiv.js"></script>
	    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
    
</head><!--/head-->

<body>
	<header id="header">      
        <div class="container">
            <div class="row">
                <div class="col-sm-12 overflow">
                   <div class="social-icons pull-right">
                        <ul class="nav nav-pills">
                            <li><a href=""><i class="fa fa-facebook"></i></a></li>
                            <li><a href=""><i class="fa fa-twitter"></i></a></li>
                            <li><a href=""><i class="fa fa-google-plus"></i></a></li>
                            <li><a href=""><i class="fa fa-dribbble"></i></a></li>
                            <li><a href=""><i class="fa fa-linkedin"></i></a></li>
                            <li>
                            <div class="form-group">
                                <select class="form-control" onchange="javascript:window.location.href='<?php echo base_url(); ?>LanguageSwitcher/switchLang/'+this.value;">
                                    <option value="english" <?php if($this->session->userdata('site_lang') == 'english') echo 'selected="selected"'; ?>>English</option>
                                    <option value="indonesian" <?php if($this->session->userdata('site_lang') == 'indonesian') echo 'selected="selected"'; ?>>Indonesia</option>
                                    <option value="japanese" <?php if($this->session->userdata('site_lang') == 'japanese') echo 'selected="selected"'; ?>>Japanese</option>   
                                </select>
                            </div>
                            </li>               
                        </ul>
                    </div> 
                </div>
             </div>
        </div>
        <div class="navbar navbar-inverse" role="banner">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <a class="navbar-brand" href="index.html">
                    	<h1><i class="material-icons">
                            language
                        </i> HIKARI </h1>
                    </a>
                    
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li class="active"><a href="<?php echo site_url()?>">Beranda</a></li>
                        <li class=""><a href="<?php echo site_url('news')?>">Berita</a></li>
                        <!-- <li class="dropdown"><a href="#">Berita <i class="fa fa-angle-down"></i></a>
                            <ul role="menu" class="sub-menu">
                                <li><a href="aboutus.html">About</a></li>
                                <li><a href="aboutus2.html">About 2</a></li>
                                <li><a href="service.html">Services</a></li>
                                <li><a href="pricing.html">Pricing</a></li>
                                <li><a href="contact.html">Contact us</a></li>
                                <li><a href="contact2.html">Contact us 2</a></li>
                                <li><a href="404.html">404 error</a></li>
                                <li><a href="coming-soon.html">Coming Soon</a></li>
                            </ul>
                        </li>                     -->
                        <li class="dropdown"><a href="blog.html">Program <i class="fa fa-angle-down"></i></a>
                            <ul role="menu" class="sub-menu">
                                <li><a href="<?php echo site_url('home/course')?>">Kursus</a></li>
                                <li><a href="<?php echo site_url('home/education')?>">Pendidikan dan Pelatikan</a></li>
                                <li><a href="<?php echo site_url('home/internship')?>">Program Magang</a></li>
                                <li><a href="<?php echo site_url('home/general_trading');?>">General Trading</a></li>
                            </ul>
                        </li>                    
                        <li class=""><a href="<?php echo site_url('profile')?>">Profile</a></li>
                        <li class=""><a href="<?php echo site_url('galery')?>">Galeri</a></li>
                    </ul>
                </div>
            </div>
        </div>
</div>
    </header>
    <!--/#header-->