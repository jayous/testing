
    <footer id="footer">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 text-center bottom-separator">
                    <img src="images/home/under.png" class="img-responsive inline" alt="">
                </div>
                <div class="col-md-4 col-sm-6">
                    <div class="testimonial bottom">
                        <h2 style="color: white;">Testimonial</h2>
                        <div class="media">
                            <div class="pull-left">
                                <a href="#"><img src="images/home/profile1.png" alt=""></a>
                            </div>
                            <div class="media-body">
                                <blockquote>Nisi commodo bresaola, leberkas venison eiusmod bacon occaecat labore tail.</blockquote>
                                <h3><a href="#">- Jhon Kalis</a></h3>
                            </div>
                         </div>
                        <div class="media">
                            <div class="pull-left">
                                <a href="#"><img src="images/home/profile2.png" alt=""></a>
                            </div>
                            <div class="media-body">
                                <blockquote>Capicola nisi flank sed minim sunt aliqua rump pancetta leberkas venison eiusmod.</blockquote>
                                <h3><a href="">- Abraham Josef</a></h3>
                            </div>
                        </div>   
                    </div> 
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="contact-info bottom">
                    <h2 style="color: white;">Contacts</h2>
                        <address>
                        E-mail: <a href="mailto:someone@example.com">email@email.com</a> <br> 
                        Phone: +1 (123) 456 7890 <br> 
                        Fax: +1 (123) 456 7891 <br> 
                        </address>

                        <h2 style="color: white;">Address</h2>
                        <address>
                        Unit C2, St.Vincent's Trading Est., <br> 
                        Feeder Road, <br> 
                        Bristol, BS2 0UY <br> 
                        United Kingdom <br> 
                        </address>
                    </div>
                </div>
                <div class="col-md-4 col-sm-12">
                    <div class="contact-form bottom">
                    <h2 style="color: white;">Send a message</h2>
                        <form id="main-contact-form" name="contact-form" method="post" action="sendemail.php">
                            <div class="form-group">
                                <input type="text" name="name" class="form-control" required="required" placeholder="Name">
                            </div>
                            <div class="form-group">
                                <input type="email" name="email" class="form-control" required="required" placeholder="Email Id">
                            </div>
                            <div class="form-group">
                                <textarea name="message" id="message" required="required" class="form-control" rows="8" placeholder="Your text here"></textarea>
                            </div>                        
                            <div class="form-group">
                                <input type="submit" name="submit" class="btn btn-submit" value="Submit">
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-sm-12" style="">
                    <div class="copyright-text text-center">
                        <p>&copy; Your Company 2014. All Rights Reserved.</p>
                        <p>Designed by <a target="_blank" href="http://www.themeum.com">Themeum</a></p>
                    </div>
                </div>
            </div>
        </div>
        
    </footer>
    <!--/#footer-->

    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url('assets')?>/js/bootstrap.min.js"></script>
 
    <script type="text/javascript" src="<?php echo base_url('assets')?>/js/wow.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url('assets')?>/js/main.js"></script>
    
    <script>
        $(function(){
            var id = $( "input[name='artikel_id']" ).val();

            $(".content-limit").each(function(i){
                len=$(this).text().length;
                if(len>100)
                {
                    $(this).text($(this).text().substr(0,200)+'...');
                }
            });
            $(".program-content-limit").each(function(i){
                len=$(this).text().length;
                if(len>100)
                {
                    $(this).text($(this).text().substr(0,400)+'...');
                }
            });
            $(".post-title").each(function(i){
                len=$(this).text().length;
                if(len>20)
                {
                    $(this).text($(this).text().substr(0,40)+'...');
                }
            });
            
            function show() {
                $.get( "<?php echo site_url()?>/comments/get_comments/"+id, function( data ) {
                    var html = '';
                    var i;
                    for (i = 0; i<data.length; i++) {
                        html += '<li class="media">'+
                                '<div class="post-comment">'+
                                '<div class="media-body">'+
                                '<span><i class="fa fa-user"></i>Posted by - <a href="#">'+data[i].users_name+'</a></span>'+
                                '<p>'+data[i].comment_content+'</p>'+
                                '<ul class="nav navbar-nav post-nav">'+
                                '<li><a href="#"><i class="fa fa-clock-o"></i>'+data[i].date_time+'</a></li>'+
                                '</ul>'+
                                '</div>'+
                                '</div>'+
                                '</li>';
                    }
                    $('#list-comments').html(html).hide().fadeIn(1000);;                
                }, "json");
            }

            show();

            $('#comments-form').submit(function( event ){
                //stop event submit
                event.preventDefault();
                
                //get val
                var $form = $( this ),
                    _name        = $form.find( "input[name='name']" ).val(),
                    _email       = $form.find( "input[name='email']" ).val(),
                    _message     = $form.find( "textarea[name='message']" ).val(),
                    _artikel_id  = $form.find( "input[name='artikel_id']" ).val(),
                    url          = $form.attr( "action" );
                
                var posting = $.post( 
                    url, 
                    {
                        name : _name,
                        email : _email,
                        message : _message,
                        artikel_id : _artikel_id
                    }
                );

                posting.done(function( data ) {
                    $( "input[name='name']" ).removeAttr('value');
                    show();
                });

                this.reset();
            });

            // $("#btn-save").click(function() {
            //     var name = $("#name").val();
            //     var email = $("#email").val();
            //     var message = $("#message").val();
            //     var artikel_id = $("#artikel_id").val();
            //     $.ajax({
            //         type: "POST",
            //         url: "<?php echo site_url()?>/comments/add_comments",
            //         dataType: "JSON",
            //         data: {
            //             users_name: name,
            //             users_email: email,
            //             comment_content: message,
            //             artikel_id: artikel_id,
            //         },
            //     });
            //     alert(name);
            // });
            
        });
   
    </script>
</body>
</html>
