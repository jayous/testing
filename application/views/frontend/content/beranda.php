
  
    <section id="masonery_area">
        <div class="">
            <div class="row">

                <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <div class="container-image">
                                <div style="background-color: rgb(0, 0, 0);">
                                    <img src="<?php echo base_url('assets')?>/images/home/slider/crgc.jpg" class="image-fluid" alt="Responsive image">
                                </div>
                            <!-- <img class="d-block w-100" src="" alt="First slide"> -->
                                <div class="middle">
                                    <h2 class="title-image">HIKARI</h2>
                                    <p class="text-image">Lembaga Kursus Bahasa Jepang</p>
                                </div>
                                <div class="carousel-caption">
                                    <h2>Apa INI</h2>
                                    <p>INI apa</p>
                                </div>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="container-image">
                                <div style="background-color: rgb(0, 0, 0);">
                                    <img src="<?php echo base_url('assets')?>/images/home/slider/crgc.jpg" class="image-fluid" alt="Responsive image">
                                </div>
                                <!-- <img class="d-block w-100" src="" alt="First slide"> -->
                                <div class="middle">
                                    <h2 class="title-image">HIKARI</h2>
                                    <p class="text-image">Lembaga Kursus Bahasa Jepang</p>
                                </div>
                                <div class="carousel-caption">
                                    <h2>Apa INI</h2>
                                    <p>INI apa</p>
                                </div>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="container-image">
                            <div style="background-color: rgb(0, 0, 0);">
                                <img src="<?php echo base_url('assets')?>/images/home/slider/crgc.jpg" class="image-fluid" alt="Responsive image">
                            </div>
                            <!-- <img class="d-block w-100" src="" alt="First slide"> -->
                            <div class="middle">
                                <h2 class="title-image">HIKARI</h2>
                                <p class="text-image">Lembaga Kursus Bahasa Jepang</p>
                            </div>
                            <div class="carousel-caption">
                                    <h2>Apa INI</h2>
                                    <p>INI apa</p>
                                </div>
                            </div>
                        </div>
                       
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>

            </div>
        </div>
        <!-- <div class="preloader"><i class="fa fa-sun-o fa-spin"></i></div> -->
    </section>
    <!--/#home-slider-->

    <section id="services" style="background-color: rgb(232,224,240);">
        <div class="container">
            <div class="row">
                <div class="col-sm-4 text-center padding wow fadeIn" data-wow-duration="1000ms" data-wow-delay="300ms">
                    <div class="single-service">
                        <i class="material-icons">
                        done_outline
                        </i>
                        <h2 style="">Incredibly Responsive</h2>
                        <p>Ground round tenderloin flank shank ribeye. Hamkevin meatball swine. Cow shankle beef sirloin chicken ground round.</p>
                    </div>
                </div>
                <div class="col-sm-4 text-center padding wow fadeIn" data-wow-duration="1000ms" data-wow-delay="600ms">
                    <div class="single-service">
                        <i class="material-icons">
                        featured_play_list
                        </i>
                        <h2 style="">Superior Typography</h2>
                        <p>Hamburger ribeye drumstick turkey, strip steak sausage ground round shank pastrami beef brisket pancetta venison.</p>
                    </div>
                </div>
                <div class="col-sm-4 text-center padding wow fadeIn" data-wow-duration="1000ms" data-wow-delay="900ms">
                    <div class="single-service">
                        <i class="material-icons">
                        select_all
                        </i>
                        <h2 style="">Swift Page Builder</h2>
                        <p>Venison tongue, salami corned beef ball tip meatloaf bacon. Fatback pork belly bresaola tenderloin bone pork kevin shankle.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--/#services-->

    <section id="action" class="responsive">
        <div class="vertical-center" style="background-color: rgb(225, 225, 225, 0.4);">
             <div class="container">
                <div class="row">
                    <div class="action take-tour">
                        <div class="col-sm-7 wow fadeInLeft" data-wow-duration="500ms" data-wow-delay="300ms">
                            <h1 class="title">Hikari Fitur</h1>
                            <p>A responsive, retina-ready &amp; wide multipurpose template.</p>
                        </div>
                       
                    </div>
                </div>
            </div>
        </div>
   </section>
    <!--/#action-->

    <section id="features">
        <div class="container">
            <div class="row">
                <div class="single-features">
                    <div class="col-sm-5 wow fadeInLeft" data-wow-duration="500ms" data-wow-delay="300ms">
                        <img src="<?php echo base_url('assets')?>/images/home/image1.png" class="img-responsive" alt="">
                    </div>
                    <div class="col-sm-6 wow fadeInRight" data-wow-duration="500ms" data-wow-delay="300ms">
                        <h2>Experienced and Enthusiastic</h2>
                        <P>Pork belly leberkas cow short ribs capicola pork loin. Doner fatback frankfurter jerky meatball pastrami bacon tail sausage. Turkey fatback ball tip, tri-tip tenderloin drumstick salami strip steak.</P>
                    </div>
                </div>
                <div class="single-features">
                    <div class="col-sm-6 col-sm-offset-1 align-right wow fadeInLeft" data-wow-duration="500ms" data-wow-delay="300ms">
                        <h2>Built for the Responsive Web</h2>
                        <P>Mollit eiusmod id chuck turducken laboris meatloaf pork loin tenderloin swine. Pancetta excepteur fugiat strip steak tri-tip. Swine salami eiusmod sint, ex id venison non. Fugiat ea jowl cillum meatloaf.</P>
                    </div>
                    <div class="col-sm-5 wow fadeInRight" data-wow-duration="500ms" data-wow-delay="300ms">
                        <img src="<?php echo base_url('assets')?>/images/home/image2.png" class="img-responsive" alt="">
                    </div>
                </div>
                <div class="single-features">
                    <div class="col-sm-5 wow fadeInLeft" data-wow-duration="500ms" data-wow-delay="300ms">
                        <img src="<?php echo base_url('assets')?>/images/home/image3.png" class="img-responsive" alt="">
                    </div>
                    <div class="col-sm-6 wow fadeInRight" data-wow-duration="500ms" data-wow-delay="300ms">
                        <h2>Experienced and Enthusiastic</h2>
                        <P>Ut officia cupidatat anim excepteur fugiat cillum ea occaecat rump pork chop tempor. Ut tenderloin veniam commodo. Shankle aliquip short ribs, chicken eiusmod exercitation shank landjaeger spare ribs corned beef.</P>
                    </div>
                </div>
            </div>
        </div>
    </section>
     <!--/#features-->
     
     <section id="action" class="responsive">
        <div class="vertical-center" style="background-color: rgb(225, 225, 225, 0.4);">
             <div class="container">
                <div class="row">
                    <div class="action take-tour">
                        <div class="col-sm-7 wow fadeInLeft" data-wow-duration="500ms" data-wow-delay="300ms">
                            <h1 class="title"> <i class="material-icons">
                            perm_media
                            </i> Artikel Terbaru</h1>
                            <p>A responsive, retina-ready &amp; wide multipurpose template.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
   </section>
    <!-- blog -->
    <section  id="blog" class="padding-top" >
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-7">
                    <div class="row">
                        {blog_entries}
                         <div class="col-md-4 col-sm-12 blog-padding-right">
                            <div class="single-blog two-column">
                                <div class="post-thumb">
                                    <a href="blogdetails.html"><img src="<?php echo base_url()?>{file_loc}" class="img-responsive" alt=""></a>
                                    <div class="post-overlay">
                                        <span class="uppercase"><a href="#">14 <br><small>Feb</small></a></span>
                                    </div>
                                </div>
                                <div class="post-content overflow">
                                    <h2 class="post-title bold"><a href="blogdetails.html">{judul}</a></h2>
                                    <h3 class="post-author"><a href="#">Posted by micron News</a></h3>
                                    <div class="content-limit"  style="color: black; text-align: justify; padding: 10px;">{konten}</div>
                                    <a href="<?php echo site_url('news/page/')?>{slug}" class="read-more">View More</a>
                                    <div class="post-bottom overflow">
                                        <ul class="nav nav-justified post-nav">
                                            <li><a href="#"><i class="fa fa-tag"></i>Creative</a></li>
                                            <li><a href="#"><i class="fa fa-heart"></i>32 Love</a></li>
                                            <li><a href="#"><i class="fa fa-comments"></i>3 Comments</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {/blog_entries}
                    </div>
                 </div>
                </div>
            </div>
        </div>
    </section>
    <!--/#blog-->

