    <section id="page-breadcrumb">
        <div class="vertical-center sun">    
             <div class="container">
                <div class="row">
                    <div class="action">
                        <div class="col-sm-12">
                            <h1 class="title">Blog</h1>
                            <p>Blog with right sidebar</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
   </section>
    <!--/#action-->

    <section id="blog-details" class="padding-top">
        <div class="container">
            <div class="row">
                <div class="col-md-9 col-sm-7">
                    <div class="row">
                         <div class="col-md-12 col-sm-12">
                            
                            <div class="single-blog blog-details two-column">
                            <?php
                                if ($blog_detail != NULL) {
                             ?> 
                               <hr>
                                <h2 class="post-titlee bold"><?php echo $blog_detail[0]['judul']?></h2>
                                <hr>
                                <div class="post-thumb">
                                    <a href="#"><img style="width: 100%;" src="<?php echo base_url().$blog_detail[0]['file_loc']?>" class="img-responsive" alt=""></a>
                                    <div class="post-overlay">
                                        <span class="uppercase"><a href="#"><?php echo date('d', $blog_detail[0]['tanggal_post']);?><br><small><?php echo substr(date('F', $blog_detail[0]['tanggal_post']), 0, 3);?></small></a></span>
                                    </div>
                                </div>
                                <div class="post-content overflow">
                                    <h2 class="post-titlee blod"><?php echo timespan($blog_detail[0]['tanggal_post'], time(), 2);?> / <?php echo date('d F Y', $blog_detail[0]['tanggal_post']);?></h2>
                                    <h3 class="post-author"><a href="#">Posted by <?php echo $blog_detail[0]['first_name'].' '.$blog_detail[0]['last_name']?></a></h3>
                                    <p style="color: black;"><?php echo $blog_detail[0]['konten']?></p>
                                    
                                    <div class="post-bottom overflow">
                                        <ul class="nav navbar-nav post-nav">
                                            <li><a href="#"><i class="fa fa-tag"></i>Creative</a></li>
                                            <li><a href="#"><i class="fa fa-heart"></i>32 Love</a></li>
                                            <li><a href="#"><i class="fa fa-comments"></i>3 Comments</a></li>
                                        </ul>
                                    </div>
                                   
                                    <div class="response-area">
                                    <hr>
                                    <h2 class="bold">Comments</h2>
                                    <div class="contact-form bottom">
                                    <h2 style="color: white;">Send a message</h2>
                                        <form id="comments-form" name="contact-form" method="post" action="<?php echo site_url()?>/comments/add_comments">
                                            <input name="artikel_id" type="hidden" value="<?php echo $blog_detail[0]['id']?>">
                                            <div class="form-group">
                                                <input type="text" name="name" class="form-control" required="required" placeholder="Name">
                                            </div>
                                            <div class="form-group">
                                                <input type="email" name="email" class="form-control" required="required" placeholder="Email Id">
                                            </div>
                                            <div class="form-group">
                                                <textarea id="message"  name="message" required="required" class="form-control" rows="8" placeholder="Your text here"></textarea>
                                            </div>                        
                                            <div class="form-group">
                                                <input type="submit" name="submit" class="btn btn-comment" value="Submit">
                                            </div>
                                        </form>
                                    </div>
                                    <ul id="list-comments" class="media-list">
                                       <!-- JS CONTENT -->
                                    </ul>             
                                </div><!--/Response-area-->
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                 </div>
                <div class="col-md-3 col-sm-5">
                    <div class="sidebar blog-sidebar">
                    <div class="sidebar-item  recent">
                            <h3>Latest News</h3>
                            <?php
                                if ($blog_latest != NULL) {
                                    foreach ($blog_latest as $data) {
                            ?>
                            <div class="media">
                                <div class="pull-left" style="text-align: center; padding : 4px; background-color: silver;">
                                    <span class="uppercase"><?php echo date('d', $data['tanggal_post']);?><br> <?php echo substr(date('F', $data['tanggal_post']), 0, 3);?></span>
                                </div>
                                <div class="media-body" style="padding-left: 5px;">
                                    <h4><a href="<?php echo site_url('news/page/'.$data['slug'])?>"><?php echo word_limiter($data['judul'], 5);?></a></h4>
                                    <p>posted on   <?php echo timespan($data['tanggal_post'], time(), 2);?></p>
                                </div>
                            </div>

                                    <?php }  }?>
                            <div class="media">
                            </div>
                        </div>
                        <div class="sidebar-item categories">
                        <h3>Categories</h3>
                            
                                <ul class="nav navbar-stacked">
                                    <?php if($kategori != NULL) {
                                        foreach($kategori as $kategori) {
                                    ?>
                                    <div class="media">
                                    <li><a href="<?php echo site_url('news/categories/'.$kategori['slug'])?>"><?php echo $kategori['nama'];?><span class="pull-right">(1)</span></a></li>
                                    </div>
                                        <?php } }?>
                                        <div class="media">
                                    <li><a href="#"><span class="pull-right"></span></a></li>
                                    </div>
                                </ul>
                        </div>
                      
                        <div class="sidebar-item popular">
                            <h3>Latest Photos</h3>
                            <ul class="gallery">
                                <li><a href="#"><img src="<?php echo base_url('assets')?>/images/portfolio/popular1.jpg" alt=""></a></li>
                                <li><a href="#"><img src="<?php echo base_url('assets')?>/images/portfolio/popular2.jpg" alt=""></a></li>
                                <li><a href="#"><img src="<?php echo base_url('assets')?>/images/portfolio/popular3.jpg" alt=""></a></li>
                                <li><a href="#"><img src="<?php echo base_url('assets')?>/images/portfolio/popular4.jpg" alt=""></a></li>
                                <li><a href="#"><img src="<?php echo base_url('assets')?>/images/portfolio/popular5.jpg" alt=""></a></li>
                                <li><a href="#"><img src="<?php echo base_url('assets')?>/images/portfolio/popular1.jpg" alt=""></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--/#blog-->
