<section id="page-breadcrumb">
        <div class="vertical-center sun">    
             <div class="container">
                <div class="row">
                    <div class="action">
                        <div class="col-sm-12">
                            <h1 class="title">Galery</h1>
                            <p>Blog with right sidebar</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
   </section>
    <!--/#action-->

<section id="blog" class="padding-top padding-bottom">
        <div class="container">
            <div class="row">
                <div class="masonery_area">
                    <?php 
                        if ($galery != NULL) {
                            foreach($galery as $galery) {
                    ?>
                    <div class="col-md-3 col-sm-4">
                        <div class="single-blog two-column">
                            <div class="portfolio-wrapper">
                                <div class="portfolio-single">
                                    <div class="portfolio-thumb">
                                        <img src="<?php echo $galery['file_loc']?>" class="img-responsive" alt="">
                                    </div>
                                    <div class="portfolio-view">
                                        <ul class="nav nav-pills">
                                            <li><a href="portfolio-details.html"><i class="fa fa-link"></i></a></li>
                                            <li><a href="<?php echo $galery['file_loc']?>"  data-lightbox="example-set"><i class="fa fa-eye"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="post-content overflow">
                                <h2 class="post-titlee"><?php echo $galery['file_ket']?></h2>
                            </div>
                            <hr>
                        </div>
                    </div>
                            <?php } }?>
                   
                </div>
            </div>
        </div>
    </section>