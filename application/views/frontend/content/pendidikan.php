<section id="page-breadcrumb">
        <div class="vertical-center sun">
             <div class="container">
                <div class="row">
                    <div class="action">
                        <div class="col-sm-12">
                            <h1 class="title">Blog</h1>
                            <p>Blog with right sidebar</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
   </section>

   <section id="blog" class="padding-top">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-5">
                    <div class="sidebar blog-sidebar">
                        <div class="sidebar-item categories">
                            <h3>Categories</h3>
                            <ul class="nav navbar-stacked">
                                    <?php if($kategori != NULL) {
                                        foreach($kategori as $kategori) {
                                    ?>
                                    <div class="media">
                                    <li><a href="<?php echo site_url('home/program/'.$kategori['slug'])?>"><?php echo $kategori['nama'];?><span class="pull-right">(1)</span></a></li>
                                    </div>
                                        <?php } }?>
                                        <div class="media">
                                    <li><a href="#"><span class="pull-right"></span></a></li>
                                    </div>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-9 col-sm-7">
                    <div class="row">
                        <?php
                            if ($blog != NULL){
                                $kom;
                                foreach($blog as $blog){
                        ?>
                         <div class="col-md-12 col-sm-12">
                            <div class="single-blog two-column">
                                <div class="post-thumb">
                                    <a href="blogdetails.html"><img src="<?php echo base_url().$blog['file_loc']?>" class="img-responsive" alt=""></a>
                                    <div class="post-overlay">
                                    <span class="uppercase"><a href="#"><?php echo date('d', $blog['tanggal']);?><br><small><?php echo substr(date('F', $blog['tanggal']), 0, 3);?></small></a></span>
                                    </div>
                                </div>
                                <div class="post-content overflow">
                                    <h2 class="post-title bold"><?php echo $blog['judul']?></h2>
                                    <h3 class="post-author"><a href="#">Posted On <?php echo timespan($blog['tanggal'], time(), 2);?></a></h3>
                                    <hr>
                                    <div class="program-content-limit"><?php echo $blog['konten']?></div>
                                    <a href="#" class="read-more">Liha Selengkapnya</a>
                                    <div class="post-bottom overflow">
                                        <ul class="nav navbar-nav post-nav">
                                            <li><a href="#"><i class="fa fa-tag"></i>Creative</a></li>
                                            <li><a href="#"><i class="fa fa-heart"></i>32 Love</a></li>
                                            <li><a href="#"><i class="fa fa-comments"></i>3 Comments</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                                <?php } }?>
                       
                      
                    </div>
                    <?php 
	                    echo $this->pagination->create_links();
	                ?>
                 </div>
            </div>
        </div>
    </section>