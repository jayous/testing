    <section id="page-breadcrumb">
        <div class="vertical-center sun">    
             <div class="container">
                <div class="row">
                    <div class="action">
                        <div class="col-sm-12">
                            <h1 class="title">Blog 
                                <?php 
                                    if ($this->uri->segment(2) == "categories") {
                                        echo "/ ".$detail_kategori;
                                    }
                                ?>
                            </h1>
                            <p style="  color: #4b0375;">Blog with right sidebar</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
   </section>
    <!--/#action-->
   
    <section id="blog" class="padding-top">
        <div class="container">
            <div class="row">
                <div class="col-md-9 col-sm-7">
                    <div class="row">
                        <?php
                            if ($blog != NULL){
                                $kom;
                                foreach($blog as $blog){
                        ?>
                        <div style="max-height: 600px; min-height: 600px;" class="col-md-6 col-sm-12 blog-padding-right">
                            <div class="single-blog two-column">
                                <div class="post-thumb">
                                    <center><a href="<?php echo site_url('news/page/'.$blog['slug'])?>"><img style="max-height: 230px; min-height: 230px;" src="<?php echo base_url().$blog['file_loc']?>" class="img-responsive" alt=""></a></center>
                                    <div class="post-overlay">
                                        <span class="uppercase"><a href="#"><?php echo date('d', $blog['tanggal_post']);?><br><small><?php echo substr(date('F', $blog['tanggal_post']), 0, 3);?></small></a></span>
                                    </div>
                                </div>
                                <div class="post-content overflow">
                                    <h2 class="post-title bold"><a href="blogdetails.html"><?php echo $blog['judul']?></a></h2>
                                    <hr>
                                    <h3 class="post-author"><a href="#">Posted by <?php echo $blog['first_name'].' '.$blog['last_name']?> - <?php echo timespan($blog['tanggal_post'], time(), 2);?></a></h3>
                                    <div class="content-limit"  style="color: black; text-align: justify; padding: 10px;"><?php echo $blog['konten']?></div>
                                    <a href="<?php echo site_url('news/page/'.$blog['slug'])?>" class="read-more">Lihat Selengkapnya</a>
                                    <div class="post-bottom overflow">
                                       
                                    </div>
                                </div>
                            </div>
                        </div>
                                <?php } } else { ?>
                        <div class="col-md-12 col-sm-12 blog-padding-right">
                            <div class="single-blog two-column">
                                <div class="post-content overflow">
                                    <h2 class="post-title bold">Data tidak ada</h2>
                                    <hr>
                                </div>
                            </div>
                        </div>
                                <?php }?>
                    </div>
                    <?php 
	                    echo $this->pagination->create_links();
	                ?>
                   
                 </div>
                <div class="col-md-3 col-sm-5">
                    <div class="sidebar blog-sidebar">
                        <div class="sidebar-item  recent">
                            <h3>Latest News</h3>
                            <?php
                                if ($blog_latest != NULL) {
                                    foreach ($blog_latest as $data) {
                            ?>
                            <div class="media">
                                <div class="pull-left" style="text-align: center; padding : 4px; background-color: silver;">
                                    <span class="uppercase"><?php echo date('d', $data['tanggal_post']);?><br> <?php echo substr(date('F', $data['tanggal_post']), 0, 3);?></span>
                                </div>
                                <div class="media-body" style="padding-left: 5px;">
                                    <h4><a href="<?php echo site_url('news/page/'.$data['slug'])?>"><?php echo word_limiter($data['judul'], 5);?></a></h4>
                                    <p>posted on   <?php echo timespan($data['tanggal_post'], time(), 2);?></p>
                                </div>
                            </div>

                                    <?php }  }?>
                                <div class="media">
                                </div>
                                    
    
                        </div>
                        <div class="sidebar-item categories">
                        <h3>Categories</h3>
                            
                                <ul class="nav navbar-stacked">
                                    <?php if($kategori != NULL) {
                                        foreach($kategori as $kategori) {
                                    ?>
                                    <div class="media">
                                    <li><a href="<?php echo site_url('news/categories/'.$kategori['slug'])?>"><?php echo $kategori['nama'];?><span class="pull-right">(1)</span></a></li>
                                    </div>
                                        <?php } }?>
                                        <div class="media">
                                    <li><a href="#"><span class="pull-right"></span></a></li>
                                    </div>
                                </ul>
                        </div>
                      
                        <div class="sidebar-item popular">
                            <h3>Latest Photos</h3>
                            <ul class="gallery">
                                <li><a href="#"><img src="<?php echo base_url('assets')?>/images/portfolio/popular1.jpg" alt=""></a></li>
                                <li><a href="#"><img src="<?php echo base_url('assets')?>/images/portfolio/popular2.jpg" alt=""></a></li>
                                <li><a href="#"><img src="<?php echo base_url('assets')?>/images/portfolio/popular3.jpg" alt=""></a></li>
                                <li><a href="#"><img src="<?php echo base_url('assets')?>/images/portfolio/popular4.jpg" alt=""></a></li>
                                <li><a href="#"><img src="<?php echo base_url('assets')?>/images/portfolio/popular5.jpg" alt=""></a></li>
                                <li><a href="#"><img src="<?php echo base_url('assets')?>/images/portfolio/popular1.jpg" alt=""></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--/#blog-->