-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 16, 2019 at 08:46 AM
-- Server version: 10.1.32-MariaDB
-- PHP Version: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hikari`
--

-- --------------------------------------------------------

--
-- Table structure for table `artikel`
--

CREATE TABLE `artikel` (
  `id` int(20) NOT NULL,
  `tanggal_post` varchar(10) DEFAULT NULL,
  `konten` longtext,
  `judul` text,
  `diubah` datetime DEFAULT CURRENT_TIMESTAMP,
  `comment` int(20) DEFAULT NULL,
  `slug` text,
  `kategori_artikel_id` int(10) UNSIGNED NOT NULL,
  `users_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `artikel`
--

INSERT INTO `artikel` (`id`, `tanggal_post`, `konten`, `judul`, `diubah`, `comment`, `slug`, `kategori_artikel_id`, `users_id`) VALUES
(1, '1545810829', '<h2>Creating&nbsp;foreign keys for tables</h2>\r\n\r\n<h3>MySQL creating foreign key syntax</h3>\r\n\r\n<p>The following syntax illustrates how to define a foreign key in a child table in <a href=\"http://www.mysqltutorial.org/mysql-create-table/\" title=\"MySQL CREATE TABLE\">CREATE TABLE</a> statement.</p>\r\n\r\n<div class=\"crayon-font-liberation-mono crayon-os-pc crayon-syntax crayon-theme-coy-copied crayon-wrapped notranslate print-yes\" id=\"crayon-5c2323a8ad868903136161\" style=\"font-size:13px !important; height:auto; line-height:18px !important; margin-bottom:12px; margin-top:12px\">\r\n<div class=\"crayon-plain-wrap\">&nbsp;</div>\r\n\r\n<div class=\"crayon-main\" style=\"overflow:hidden; position:relative; z-index:1\">\r\n<table class=\"crayon-table\" style=\"height:127px; width:305px\">\r\n	<tbody>\r\n		<tr>\r\n			<td>\r\n			<div class=\"crayon-nums-content\" style=\"font-size:13px !important; line-height:18px !important\">\r\n			<div class=\"crayon-num\" style=\"height:18px\">1</div>\r\n\r\n			<div class=\"crayon-num\" style=\"height:18px\">2</div>\r\n\r\n			<div class=\"crayon-num\" style=\"height:18px\">3</div>\r\n\r\n			<div class=\"crayon-num\" style=\"height:18px\">4</div>\r\n\r\n			<div class=\"crayon-num\" style=\"height:18px\">5</div>\r\n			</div>\r\n			</td>\r\n			<td>\r\n			<div class=\"crayon-pre\" style=\"-moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; font-size:13px !important; line-height:18px !important; tab-size:4\">\r\n			<div class=\"crayon-line\" id=\"crayon-5c2323a8ad868903136161-1\">CONSTRAINT constraint_name\r\n			<table border=\"1\" cellpadding=\"1\" cellspacing=\"1\" style=\"width:500px\">\r\n				<tbody>\r\n					<tr>\r\n						<td>asd</td>\r\n						<td>asd</td>\r\n					</tr>\r\n					<tr>\r\n						<td>as</td>\r\n						<td>asd</td>\r\n					</tr>\r\n					<tr>\r\n						<td>asd</td>\r\n						<td>asd</td>\r\n					</tr>\r\n				</tbody>\r\n			</table>\r\n\r\n			<p>&nbsp;</p>\r\n			</div>\r\n\r\n			<div class=\"crayon-line\" id=\"crayon-5c2323a8ad868903136161-2\">FOREIGN KEY foreign_key_name (columns)</div>\r\n\r\n			<div class=\"crayon-line\" id=\"crayon-5c2323a8ad868903136161-3\">REFERENCES parent_table(columns)</div>\r\n\r\n			<div class=\"crayon-line\" id=\"crayon-5c2323a8ad868903136161-4\">ON DELETE action</div>\r\n\r\n			<div class=\"crayon-line\" id=\"crayon-5c2323a8ad868903136161-5\">ON UPDATE action</div>\r\n			</div>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n</div>\r\n</div>\r\n\r\n<p>Let&rsquo;s examine the syntax in greater detail:</p>\r\n\r\n<ul>\r\n	<li>The <code>CONSTRAINT</code> clause allows you to define constraint name for the foreign key constraint. If you omit it, MySQL will generate a name automatically.</li>\r\n	<li>The <code>FOREIGN KEY</code> clause specifies the columns in the child table that refers to primary key columns in the parent table. You can put a foreign key name after <code>FOREIGN KEY</code> clause or leave it to let MySQL create a name for you. Notice that MySQL automatically creates an index with the <code>foreign_key_name</code> name.</li>\r\n	<li>The <code>REFERENCES</code> clause specifies the parent table and its columns to which the columns in the child table refer. The number of columns in the child table and parent table specified in the <code>FOREIGN KEY</code> and <code>REFERENCES</code> must be the same.</li>\r\n	<li>The <code>ON DELETE</code> clause allows you to define what happens to the records in the child table when the records in the parent table are deleted. If you omit the <code>ON DELETE</code> clause and delete a record in the parent table that has records in the child table refer to, MySQL will reject the deletion. In addition, MySQL also provides you with actions so that you can have other options such as <a href=\"http://www.mysqltutorial.org/mysql-on-delete-cascade/\" title=\"MySQL ON DELETE CASCADE\">ON DELETE CASCADE</a>&nbsp;that ask MySQL to delete records in the child table that refers to a record in the parent table when the record in the parent table is deleted. If you don&rsquo;t want the related records in the child table to be deleted, you use the <code>ON DELETE SET NULL</code> action instead. MySQL will set the foreign key column values in the child table to <code>NULL</code> when the record in the parent table is deleted, with a condition that the foreign key column in the child table must accept <code>NULL</code> values. Notice that if you use <code>ON DELETE NO ACTION</code> or <code>ON DELETE RESTRICT</code> action, MySQL will reject the deletion.</li>\r\n	<li>The <code>ON UPDATE</code> clause enables you to specify what happens to the rows in the child table when rows in the parent table are updated. You can omit the <code>ON UPDATE</code> clause to let MySQL reject any updates to the rows in the child table when the rows in the parent table are updated. The <code>ON UPDATE CASCADE</code> action allows you to perform a cross-table update, and the <code>ON UPDATE SET NULL</code> action resets the values in the rows in the child table to <code>NULL</code> values when the rows in the parent table are updated. The <code>ON UPDATE NO ACTION</code> or <code>UPDATE RESTRICT</code> actions reject any updates.</li>\r\n</ul>\r\n\r\n<h3>MySQL creating table foreign key example</h3>\r\n\r\n<p>The following example creates a <code>dbdemo</code> database and two tables: <code>categories</code> and&nbsp;<code>products.</code> Each category has one or more products and each product belongs to only one category. The <code>cat_id</code> field in the <code>products</code> table is defined as a foreign key with <code>UPDATE ON CASCADE</code> and <code>DELETE ON RESTRICT</code> actions.</p>\r\n\r\n<div class=\"crayon-font-liberation-mono crayon-os-pc crayon-syntax crayon-theme-coy-copied crayon-wrapped notranslate print-yes\" id=\"crayon-5c2323a8ad888918415923\" style=\"font-size:13px !important; height:auto; line-height:18px !important; margin-bottom:12px; margin-top:12px\">\r\n<div class=\"crayon-plain-wrap\">&nbsp;</div>\r\n\r\n<div class=\"crayon-main\" style=\"overflow:hidden; position:relative; z-index:1\">\r\n<table class=\"crayon-table\">\r\n	<tbody>\r\n		<tr>\r\n			<td>\r\n			<div class=\"crayon-nums-content\" style=\"font-size:13px !important; line-height:18px !important\">\r\n			<div class=\"crayon-num\" style=\"height:18px\">1</div>\r\n\r\n			<div class=\"crayon-num\" style=\"height:18px\">2</div>\r\n\r\n			<div class=\"crayon-num\" style=\"height:18px\">3</div>\r\n\r\n			<div class=\"crayon-num\" style=\"height:18px\">4</div>\r\n\r\n			<div class=\"crayon-num\" style=\"height:18px\">5</div>\r\n\r\n			<div class=\"crayon-num\" style=\"height:18px\">6</div>\r\n\r\n			<div class=\"crayon-num\" style=\"height:18px\">7</div>\r\n\r\n			<div class=\"crayon-num\" style=\"height:18px\">8</div>\r\n\r\n			<div class=\"crayon-num\" style=\"height:18px\">9</div>\r\n\r\n			<div class=\"crayon-num\" style=\"height:18px\">10</div>\r\n\r\n			<div class=\"crayon-num\" style=\"height:18px\">11</div>\r\n\r\n			<div class=\"crayon-num\" style=\"height:18px\">12</div>\r\n\r\n			<div class=\"crayon-num\" style=\"height:18px\">13</div>\r\n\r\n			<div class=\"crayon-num\" style=\"height:18px\">14</div>\r\n\r\n			<div class=\"crayon-num\" style=\"height:18px\">15</div>\r\n\r\n			<div class=\"crayon-num\" style=\"height:18px\">16</div>\r\n\r\n			<div class=\"crayon-num\" style=\"height:18px\">17</div>\r\n\r\n			<div class=\"crayon-num\" style=\"height:18px\">18</div>\r\n\r\n			<div class=\"crayon-num\" style=\"height:18px\">19</div>\r\n\r\n			<div class=\"crayon-num\" style=\"height:18px\">20</div>\r\n			</div>\r\n			</td>\r\n			<td>\r\n			<div class=\"crayon-pre\" style=\"-moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; font-size:13px !important; line-height:18px !important; tab-size:4\">\r\n			<div class=\"crayon-line\" id=\"crayon-5c2323a8ad888918415923-1\">CREATE DATABASE IF NOT EXISTS dbdemo;</div>\r\n\r\n			<div class=\"crayon-line\" id=\"crayon-5c2323a8ad888918415923-2\">&nbsp;</div>\r\n\r\n			<div class=\"crayon-line\" id=\"crayon-5c2323a8ad888918415923-3\">USE dbdemo;</div>\r\n\r\n			<div class=\"crayon-line\" id=\"crayon-5c2323a8ad888918415923-4\">&nbsp;</div>\r\n\r\n			<div class=\"crayon-line\" id=\"crayon-5c2323a8ad888918415923-5\">CREATE TABLE categories(</div>\r\n\r\n			<div class=\"crayon-line\" id=\"crayon-5c2323a8ad888918415923-6\">&nbsp;&nbsp; cat_id int not null auto_increment primary key,</div>\r\n\r\n			<div class=\"crayon-line\" id=\"crayon-5c2323a8ad888918415923-7\">&nbsp;&nbsp; cat_name varchar(255) not null,</div>\r\n\r\n			<div class=\"crayon-line\" id=\"crayon-5c2323a8ad888918415923-8\">&nbsp;&nbsp; cat_description text</div>\r\n\r\n			<div class=\"crayon-line\" id=\"crayon-5c2323a8ad888918415923-9\">) ENGINE=InnoDB;</div>\r\n\r\n			<div class=\"crayon-line\" id=\"crayon-5c2323a8ad888918415923-10\">&nbsp;</div>\r\n\r\n			<div class=\"crayon-line\" id=\"crayon-5c2323a8ad888918415923-11\">CREATE TABLE products(</div>\r\n\r\n			<div class=\"crayon-line\" id=\"crayon-5c2323a8ad888918415923-12\">&nbsp;&nbsp; prd_id int not null auto_increment primary key,</div>\r\n\r\n			<div class=\"crayon-line\" id=\"crayon-5c2323a8ad888918415923-13\">&nbsp;&nbsp; prd_name varchar(355) not null,</div>\r\n\r\n			<div class=\"crayon-line\" id=\"crayon-5c2323a8ad888918415923-14\">&nbsp;&nbsp; prd_price decimal,</div>\r\n\r\n			<div class=\"crayon-line\" id=\"crayon-5c2323a8ad888918415923-15\">&nbsp;&nbsp; cat_id int not null,</div>\r\n\r\n			<div class=\"crayon-line\" id=\"crayon-5c2323a8ad888918415923-16\">&nbsp;&nbsp; FOREIGN KEY fk_cat(cat_id)</div>\r\n\r\n			<div class=\"crayon-line\" id=\"crayon-5c2323a8ad888918415923-17\">&nbsp;&nbsp; REFERENCES categories(cat_id)</div>\r\n\r\n			<div class=\"crayon-line\" id=\"crayon-5c2323a8ad888918415923-18\">&nbsp;&nbsp; ON UPDATE CASCADE</div>\r\n\r\n			<div class=\"crayon-line\" id=\"crayon-5c2323a8ad888918415923-19\">&nbsp;&nbsp; ON DELETE RESTRICT</div>\r\n\r\n			<div class=\"crayon-line\" id=\"crayon-5c2323a8ad888918415923-20\">)ENGINE=InnoDB;</div>\r\n			</div>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n</div>\r\n</div>', 'mysql Foreign key constraint is incorrectly formed erro', '2018-12-26 14:53:50', NULL, 'mysql-foreign-key-constraint-is-incorrectly-formed-erro', 4, 2);

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` int(11) NOT NULL,
  `comment_content` varchar(255) DEFAULT NULL,
  `users_ip` varchar(255) DEFAULT NULL,
  `users_name` varchar(45) DEFAULT NULL,
  `artikel_id` int(11) DEFAULT NULL,
  `users_email` varchar(255) DEFAULT NULL,
  `date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `comment_content`, `users_ip`, `users_name`, `artikel_id`, `users_email`, `date_time`) VALUES
(25, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec iaculis massa vitae lorem imperdiet, ac gravida nisi euismod. Pellentesque vel porta mi. In neque lorem, vehicula vitae nulla vel, aliquam rhoncus lacus. Integer et dignissim purus. Donec ferm', '127.0.0.1', 'M Rizqi Maulana', 1, 'rizqimaulana69@gmail.com', '2018-12-26 14:47:56'),
(26, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec iaculis massa vitae lorem imperdiet, ac gravida nisi euismod. Pellentesque vel porta mi. In neque lorem, vehicula vitae nulla vel, aliquam rhoncus lacus. Integer et dignissim purus. Donec ferm', '127.0.0.1', 'Ades Tira Rahayu', 1, 'admin@gmail.com', '2018-12-26 14:49:49'),
(27, 'asd', '127.0.0.1', 'hehe', 1, 'asd@gmail.com', '2018-12-26 16:46:05');

-- --------------------------------------------------------

--
-- Table structure for table `file`
--

CREATE TABLE `file` (
  `id` int(10) NOT NULL,
  `file_name` varchar(255) NOT NULL,
  `file_size` int(10) NOT NULL,
  `file_type` varchar(10) NOT NULL,
  `file_loc` text NOT NULL,
  `tgl_upload` date NOT NULL,
  `artikel_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `file`
--

INSERT INTO `file` (`id`, `file_name`, `file_size`, `file_type`, `file_loc`, `tgl_upload`, `artikel_id`) VALUES
(18, '15458108293.jpg', 416, 'image/jpeg', './assets/images/blog/2018-12-26/15458108293.jpg', '2018-12-26', 1);

-- --------------------------------------------------------

--
-- Table structure for table `file_galeri`
--

CREATE TABLE `file_galeri` (
  `id` int(10) NOT NULL,
  `file_name` varchar(255) NOT NULL,
  `file_size` int(10) NOT NULL,
  `file_type` varchar(10) NOT NULL,
  `file_loc` text NOT NULL,
  `tgl_upload` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `description`) VALUES
(1, 'admin', 'Administrator'),
(2, 'members', 'General User'),
(3, 'guru', 'lorem ipsum'),
(4, 'biasa', 'adalah ');

-- --------------------------------------------------------

--
-- Table structure for table `kategori_artikel`
--

CREATE TABLE `kategori_artikel` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `kategori_artikel`
--

INSERT INTO `kategori_artikel` (`id`, `nama`, `slug`) VALUES
(1, 'Tak Berkategori', ''),
(2, 'Kesehatan', ''),
(3, 'Olahraga', ''),
(4, 'Pendidikan', '');

-- --------------------------------------------------------

--
-- Table structure for table `kategori_program`
--

CREATE TABLE `kategori_program` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `kategori_program`
--

INSERT INTO `kategori_program` (`id`, `nama`) VALUES
(1, 'Pendidikan dan Pelatihan'),
(2, 'Program Magang'),
(3, 'General Trading');

-- --------------------------------------------------------

--
-- Table structure for table `kontak`
--

CREATE TABLE `kontak` (
  `id` int(10) NOT NULL DEFAULT '1',
  `email` varchar(100) NOT NULL,
  `telepon` varchar(20) NOT NULL,
  `fax` varchar(100) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `kota` varchar(100) NOT NULL,
  `kodepos` int(10) NOT NULL,
  `provinsi` varchar(100) NOT NULL,
  `negara` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kontak`
--

INSERT INTO `kontak` (`id`, `email`, `telepon`, `fax`, `alamat`, `kota`, `kodepos`, `provinsi`, `negara`) VALUES
(1, 'hikari@gmail.com', '0857-1234-5678', '+1 (123) 456 7891 ', 'Jl. Bayangkara No.10 Blok.5', 'Sukabumii', 43154, 'Jawa Barat', 'Indonesia');

-- --------------------------------------------------------

--
-- Table structure for table `kursus`
--

CREATE TABLE `kursus` (
  `id` int(10) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `deskripsi` text NOT NULL,
  `harga` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `kursus`
--

INSERT INTO `kursus` (`id`, `nama`, `slug`, `deskripsi`, `harga`) VALUES
(4, 'Paket A', 'paket-a', 'Harga Kursus + Buku', 120000),
(5, 'Paket B', 'paket-b', 'Harga Kursus + Buku', 130000);

-- --------------------------------------------------------

--
-- Table structure for table `login_attempts`
--

CREATE TABLE `login_attempts` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `login_attempts`
--

INSERT INTO `login_attempts` (`id`, `ip_address`, `login`, `time`) VALUES
(14, '::1', 'administrator', 1547615337),
(15, '::1', '	 administrator', 1547615346),
(16, '::1', 'administrator', 1547615356),
(17, '::1', 'administrator', 1547615526);

-- --------------------------------------------------------

--
-- Table structure for table `profil`
--

CREATE TABLE `profil` (
  `id` int(1) NOT NULL,
  `visi` text NOT NULL,
  `misi` text NOT NULL,
  `sejarah` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `profil`
--

INSERT INTO `profil` (`id`, `visi`, `misi`, `sejarah`) VALUES
(1, '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n', '<ol>\r\n	<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.&nbsp;</li>\r\n	<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.&nbsp;</li>\r\n	<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.&nbsp;</li>\r\n	<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.&nbsp;</li>\r\n</ol>\r\n', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `program`
--

CREATE TABLE `program` (
  `id` int(10) NOT NULL,
  `konten` text NOT NULL,
  `judul` text NOT NULL,
  `slug` text NOT NULL,
  `kategori_program_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `program`
--

INSERT INTO `program` (`id`, `konten`, `judul`, `slug`, `kategori_program_id`) VALUES
(6, '<p>qqq</p>', 'qqqqqqqqqqqqqqqq', 'qqqqqqqqqqqqqqqq', 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `email` varchar(254) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) UNSIGNED DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) UNSIGNED NOT NULL,
  `last_login` int(11) UNSIGNED DEFAULT NULL,
  `active` tinyint(1) UNSIGNED DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`) VALUES
(1, '127.0.0.1', 'administrator', '$2a$07$SeBknntpZror9uyftVopmu61qg0ms8Qv1yV6FG.kQOSM.9QhmTo36', '', 'rizqimaulana69@gmail.com', NULL, 'GZtM76P.IQ61sXuf1l3H.uc212d6784ba28987a5', 1545632842, NULL, 1268889823, 1268889823, 1, 'admin', 'istrator', 'ADMIN', '081312345674'),
(2, '127.0.0.1', 'rizqimaulana69@gmail.com', '$2y$08$0AOS3OBj2Aa1mlI/Gg2W/uj265.Iwa6LXcstS70xUF.IQ4iEKH2o2', NULL, 'rizqimaulana69@gmail.com', NULL, 'GZtM76P.IQ61sXuf1l3H.uc212d6784ba28987a5', 1545632842, NULL, 1542789131, 1545810884, 1, 'rizqi', 'maulana', 'hikari', '0223123'),
(3, '127.0.0.1', 'ades@gmail.com', '$2y$08$mmkbFtkVEMMZ4EobGhbSe.7tqFdEsbw3EtWb/jNPVViu/9jQv7VgW', NULL, 'ades@gmail.com', NULL, NULL, NULL, NULL, 1542817761, NULL, 1, 'ades', 'tira', 'cika', '0223123');

-- --------------------------------------------------------

--
-- Table structure for table `users_groups`
--

CREATE TABLE `users_groups` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `group_id` mediumint(8) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users_groups`
--

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES
(27, 1, 1),
(28, 1, 2),
(15, 2, 1),
(16, 3, 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `artikel`
--
ALTER TABLE `artikel`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_artikle_kategori_artikel_idx` (`kategori_artikel_id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `file`
--
ALTER TABLE `file`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_file_artikel` (`artikel_id`);

--
-- Indexes for table `file_galeri`
--
ALTER TABLE `file_galeri`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kategori_artikel`
--
ALTER TABLE `kategori_artikel`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kategori_program`
--
ALTER TABLE `kategori_program`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kontak`
--
ALTER TABLE `kontak`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kursus`
--
ALTER TABLE `kursus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `login_attempts`
--
ALTER TABLE `login_attempts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profil`
--
ALTER TABLE `profil`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `program`
--
ALTER TABLE `program`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_program_kategori_program` (`kategori_program_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  ADD KEY `fk_users_groups_users1_idx` (`user_id`),
  ADD KEY `fk_users_groups_groups1_idx` (`group_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `artikel`
--
ALTER TABLE `artikel`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `file`
--
ALTER TABLE `file`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `file_galeri`
--
ALTER TABLE `file_galeri`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `kategori_artikel`
--
ALTER TABLE `kategori_artikel`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `kategori_program`
--
ALTER TABLE `kategori_program`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `kursus`
--
ALTER TABLE `kursus`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `login_attempts`
--
ALTER TABLE `login_attempts`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `program`
--
ALTER TABLE `program`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users_groups`
--
ALTER TABLE `users_groups`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `artikel`
--
ALTER TABLE `artikel`
  ADD CONSTRAINT `fk_artikle_kategori_artikel` FOREIGN KEY (`kategori_artikel_id`) REFERENCES `kategori_artikel` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `file`
--
ALTER TABLE `file`
  ADD CONSTRAINT `fk_file_artikel` FOREIGN KEY (`artikel_id`) REFERENCES `artikel` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `program`
--
ALTER TABLE `program`
  ADD CONSTRAINT `fk_program_kategori_program` FOREIGN KEY (`kategori_program_id`) REFERENCES `kategori_program` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD CONSTRAINT `fk_users_groups_groups1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_users_groups_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
